jQuery(document).ready(function () {
    
    // ================================== AJAX Setup ============================================
    $.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

    // ================================== Summernote Plugin Settings =================================================
    $('.summernote').summernote({
        height: 200,
        toolbar: [
            // [groupName, [list of button]]
            ['style',
                ['bold', 'italic', 'underline', 'clear']
            ],
            ['font',
                ['strikethrough', 'superscript', 'subscript']
            ],
            ['fontsize',
                ['fontsize']
            ],
            ['color',
                ['color']
            ],
            ['para',
                ['ul', 'ol', 'paragraph']
            ],
            ['height',
                ['height']
            ],
            ['insert',
                ['link','table']
            ],
            ['misc',
                ['fullscreen','codeview','undo','redo']
            ]
        ]
    });
    
    // ================================== Search Supplier Functions ============================================
    function searchSupplier()
    {
    	var s = $('input[name="s"]').val();
    	
    	$('#searchResult').html('');
    	$('#loader').removeClass('hidden');
    	
    	$.ajax({
    		type: "GET",
    		url: $('form#searchSupplierForm').attr('action'),
    		data: { s: s }
    	}).done(function(t){
    		$('#loader').addClass('hidden');
    		$('#searchResult').html(t);
    	})
    	
    }
    
    $('input[name="s"]').keyup(function(e){
    	searchSupplier();
    	e.preventDefault();
    });
    
    $('form#searchSupplierForm').submit(function(e){
    	if($('input[name="s"]').val().length > 0){
    		searchSupplier();
    	} else {
    		alert("Please, insert name, store name or phone of supplier before you submit..!!");
    		return false;
    	}
    	e.preventDefault();
    });
    
    // ================================== jQuery Circliful ============================================
    $('.circliful').circliful();
    
    // ================================= jQuery Slick Slider ==========================================
	function createSlick(){
    		$('.carousel').not('.slick-initialized').slick({
				slidesToShow: 1
			});
	}
	createSlick();
	
	// =================================== jQuery jScroll ============================================
	$('.new-products ul.pagination').hide();
    $('.infinite-scroll').jscroll({
    	autoTrigger: true,
    	padding: 0,
    	loadingHtml: '',
    	nextSelector: '.pagination li.active + li a',
    	contentSelector: 'div.infinite-scroll',
    	callback: function() {
    		createSlick(); // call slick to re-init
    		$('ul.pagination').remove();
    	}
    });
    
    //============================= Slide Toggle Search Form ==================================
	$( ".search-btn" ).click(function() {
		$( ".resisten-search-form" ).slideToggle( "slow" );
	})
    

}); 