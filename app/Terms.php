<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

class Terms extends Model
{
	
	use Sluggable;
	use SluggableScopeHelpers;
	
	/**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' 	=> 'name',
                'onUpdate' 	=> true,
            ]
        ];
    }
	
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'terms';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'type', 'count', 'status',
    ];
	
	/**
     * Get the term's user.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
	
	/**
     * Get the term's products.
     */
    public function products()
    {
        $relations = new TermsRelations();
        return $this->belongsToMany('App\Products', $relations->getTable(), 'term_id', 'product_id');
    }
	
	/**
     * Get the term's relations.
     */
    public function relations()
    {
        return $this->hasMany('App\TermsRelations', 'term_id');
    }
    
    public static function boot()
    {
    	parent::boot();
    	
    	static::deleted(function($data)
    	{
    		$data->relations()->delete();
		});
	}
}
