<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 'seller_id', 'buyer_id', 'count', 'total_weight', 'total_price', 'total_commisions', 'description', 
        'shipping_province_id', 'shipping_province_name', 'shipping_city_id', 'shipping_city_name', 
        'shipping_subdistrict_id', 'shipping_subdistrict_name', 'shipping_cost', 'shipping_name', 
        'shipping_phone', 'shipping_address', 'shipping_date', 'shipping_agents_id', 'shipping_service_code', 
        'shipping_code', 'shipping_status', 'grand_total', 'payment_image', 'status',
    ];
	
	/**
     * Get the supplier of the order
     */
    public function seller()
    {
        return $this->belongsTo('App\User', 'seller_id');
    }
	
	/**
     * Get the reseller of the order
     */
    public function buyer()
    {
        return $this->belongsTo('App\User', 'buyer_id');
    }
	
	/**
     * Get the product of the order
     */
    public function product()
    {
        return $this->belongsTo('App\Products', 'product_id');
    }
	
	/**
     * Get the shipping agent of the order
     */
    public function courier()
    {
        return $this->belongsTo('App\ShippingAgents', 'shipping_agents_id');
    }
}
