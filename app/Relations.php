<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Relations extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_relations';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent', 'child', 'requestant', 'request_to', 'status',
    ];
	
	/**
     * Get the requested user.
     */
    public function requested_user()
    {
        return $this->belongsTo('App\User', 'request_to');
    }
	
	/**
     * Get the requestant user.
     */
    public function requestant_user()
    {
        return $this->belongsTo('App\User', 'requestant');
    }
}
