<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;

use App\Http\Controllers\NotificationsController;

use Auth;

class NotificationsComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
    	if(Auth::user()){
    		$view->with('notifications', NotificationsController::getNotifications());
			
			$complete_profile = true;
			if(Auth::user()->hasRole('supplier')){
				if(is_null(Auth::user()->subdistrict_id) || (count(Auth::user()->banks) <= 0))
					$complete_profile = false;
			}
			$view->with('complete_profile', $complete_profile);
			
    	} else {
    		$view->with('notifications', function (){
    			 return 0; 
			});
    	}
    }
}