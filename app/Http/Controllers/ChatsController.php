<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth, DB, Session, Log;

use App\User;
use App\ChatRooms;
use App\ChatMessages;

class ChatsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {

			$this->authorize('seeIndex', new ChatRooms());

			$user = Auth::user();

			$chatRooms	= ChatRooms::where('sender', '=', $user->id)->where('status', '!=', '2')
									->orWhere(function ($query) use ($user) {
										$query->where('receiver', '=', $user->id)->where('status', '!=', '3');
									})->orderBy('updated_at', 'desc')->get();

        	return view('chats.index')->with('chats', $chatRooms);

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {

			$this->authorize('create', new ChatRooms());

			$user 		= User::findOrFail(Auth::id());
			$contacts	= $user->child()->where('user_relations.status', '=', 2)->paginate(30);

        	return view('chats.create')->with('contacts', $contacts);

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

			$this->authorize('create', new ChatRooms());

			$this->validate($request, [
				'receiver' 	=> 'required|exists:users,id',
			]);

			$user 		= Auth::user();
			$receiver	= User::findOrFail($request->receiver);

			$chat_info	= json_encode(['users'=>['sender'=>$user->toArray(), 'receiver'=>$receiver->toArray()]]);

			// Check the chat room existence between user and receiver
			$chatRoom	= ChatRooms::where('sender', '=', $user->id)->where('receiver', '=', $request->receiver)
										->orWhere(function ($query) use ($user, $request) {
											$query->where('sender', '=', $request->receiver)
												  ->where('receiver', '=', $user->id);
										})->first();

			DB::beginTransaction();

			// If chat room not found, just create a new one
			if(is_null($chatRoom)){
				$chatRoom 			= new ChatRooms();
				$chatRoom->sender	= $user->id;
				$chatRoom->receiver	= $request->receiver;
				$chatRoom->info		= $chat_info;
				$chatRoom->save();
			}

			$chatRoom->status = 1;
			$chatRoom->save();

			DB::commit();

        	return redirect()->action('ChatsController@show', [$chatRoom->id]);

		} catch (AuthorizationException $e) {
			DB:: rollback();
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
			DB:: rollback();
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {

			$this->authorize('view', ChatRooms::findOrFail($id));

			$chat = ChatRooms::findOrFail($id);

        	return view('chats.chat_box')->with('chat', $chat);

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

			$this->authorize('delete', ChatRooms::findOrFail($id));

			$user = Auth::user();

			DB::beginTransaction();

			$chat = ChatRooms::findOrFail($id);
			if($chat->status == 1){
				if($user->id == $chat->sender){
					$chat->status = 2;
				} elseif($user->id == $chat->receiver) {
					$chat->status = 3;
				}
				$chat->save();
			} else{
				if(($user->id == $chat->sender && $chat->status == 3) || ($user->id == $chat->receiver && $chat->status == 2)){
					$chat->delete();
				}
			}

			DB::commit();

        	return redirect()->action('ChatsController@index');

		} catch (AuthorizationException $e) {
			DB:: rollback();
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
			DB:: rollback();
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

/*=====================================================================================================================================
 ************************************************** CHAT API FOR WEB ******************************************************************
 ====================================================================================================================================*/

 	/**
     * Get the chat room content
     *
     * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function getChatRoom(Request $request, $id)
	{
		try {

			$this->authorize('view', ChatRooms::findOrFail($id));

			$chat = ChatMessages::where('chat_id', '=', $id)->with('user')->get();
			if($request->has('last')){
				$last = ChatMessages::findOrFail($request->last);
				$chat = ChatMessages::where('chat_id', '=', $id)->where('created_at', '>', $last->created_at)->with('user')->get();
			}

        	return response()->json(array('status' => true, 'message' => $chat->toArray()));

		} catch (AuthorizationException $e) {
            Log::error($e);
            return response()->json(array('status' => false, 'message' => trans('resisten.This Action is Unauthorized')));
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(array('status' => false, 'message' => $e->getMessage()));
        }
	}

	/**
     * Store new chat room message
     *
     * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
     * @return \Illuminate\Http\Response
     */
	public function storeChat(Request $request, $id)
	{
		try {

			$this->authorize('update', ChatRooms::findOrFail($id));

			$this->validate($request, [
				'chat_id' => 'required|exists:chat_rooms,id',
				'user_id' => 'required|exists:users,id',
				'message' => 'required',
			]);

			DB::beginTransaction();

			ChatMessages::create($request->all());

			DB::commit();

        	return response()->json(array('status' => true, 'message' => $chat->toArray()));

		} catch (AuthorizationException $e) {
			DB::rollback();
            Log::error($e);
            return response()->json(array('status' => false, 'message' => trans('resisten.This Action is Unauthorized')));
        } catch (\Exception $e) {
			DB::rollback();
            Log::error($e);
            return response()->json(array('status' => false, 'message' => $e->getMessage()));
        }
	}

}
