<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use Auth, DB, Exception, Log, Validator;

use App\Terms;
use App\TermsRelations;

class TermsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexShowcaseCategories()
    {
        try {
        	
			$user	= Auth::user();
			$terms 	= $user->categories()->paginate(10); 
			
        	return view('categories.index_showcase_categories')->with('terms', $terms);
			
		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addShowcaseCategories()
    {
        try {
        	
			$this->authorize('create', new Terms());
			
        	return view('categories.add_showcase_categories');
			
		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeShowcaseCategories(Request $request)
    {
        try {
        	
			$this->authorize('create', new Terms());
			
			$this->validate($request, [
				'name'		=> 'required',
			]);
			
			DB::beginTransaction();
	        
			// Run insert data process
	        Terms::create([
	        				'user_id'	=> Auth::id(),
	        				'name'		=> $request->name,
	        				'type'		=> 'showcase_category',
	        			]);
	        
	        DB::commit();
			
			if($request->ajax()){
				return 'success';
			}
			
        	return redirect()->action('ProductsController@index')->with('success', trans('resisten.Categories created successfully.'));
			
		} catch (AuthorizationException $e) {
			DB::rollback();
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
			DB::rollback();
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editShowcaseCategories($id)
    {
        try {
        	
			$this->authorize('update', Terms::findOrFail($id));
			
			$term = Terms::findOrFail($id);
			
        	return view('categories.edit_showcase_categories')->with('term', $term);
			
		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateShowcaseCategories(Request $request, $id)
    {
        try {
        	
			$this->authorize('update', Terms::findOrFail($id));
			
			$this->validate($request, [
				'name'		=> 'required',
			]);
			
			DB::beginTransaction();
	        
			$terms 			= Terms::findOrFail($id);
			$terms->name	= $request->name;
			$terms->save();
	        
	        DB::commit();
			
			if($request->ajax()){
				return 'success';
			}
			
        	return redirect()->action('ProductsController@index')->with('success', trans('resisten.Categories updated successfully.'));
			
		} catch (AuthorizationException $e) {
			DB::rollback();
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
			DB::rollback();
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyShowcaseCategories(Request $request, $id)
    {
        try {
        	
			$this->authorize('delete', Terms::findOrFail($id));
			
			DB::beginTransaction();
	        
			$terms = Terms::findOrFail($id);
			$terms->delete();
	        
	        DB::commit();
			
			if($request->ajax()){
				return 'success';
			}
			
        	return redirect()->action('ProductsController@index')->with('success', trans('resisten.Categories deleted successfully.'));
			
		} catch (AuthorizationException $e) {
			DB::rollback();
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
			DB::rollback();
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }
}
