<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth, DB, Exception, Log, Validator;

use App\User;
use App\Relations;
use App\Terms;

class RelationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() 
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() 
	{
		try {
			
			$this->authorize('create', new Relations());
			
			return view('relations.create');
			
		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) 
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id, $slug = null) 
	{
		try {
			
			$this->authorize('view', new Relations());
			
			$viewer		= Auth::user();
			$user		= User::findOrFail($id);
			$products	= $user->products()->where(function($query) use ($viewer){
												$query->where('show_to', '=', '3')
													  ->orWhere('show_to', '=', $viewer->type);
										   })->orderBy('created_at', 'desc')
										 	 ->paginate(8);
			if(!is_null($slug)){
				$terms			= Terms::findBySlugOrFail($slug);
				$products		= $terms->products()->where(function($query) use ($viewer){
															$query->where('show_to', '=', '3')
																  ->orWhere('show_to', '=', $viewer->type);
													   })->orderBy('created_at', 'desc')
													 	 ->paginate(8);
			}
			$showcase_categories= $user->categories()->where('type', '=', 'showcase_category')->orderBy('name')->get();
			
			$orders 	= $user->orders_out()->with('seller', 'buyer', 'product', 'courier')
											 ->orderBy('created_at', 'desc')
											 ->paginate(10);
			
			$relations 	= Relations::where('parent', '=', $viewer->id)->where('child', '=', $id)->first();
        	
			return view('relations.user_details')->with('user', $user)
														  ->with('products', $products)
														  ->with('showcase_categories', $showcase_categories)
														  ->with('orders', $orders)
														  ->with('relations', $relations);
			
        } catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
	}

	/**
	 * Display the list of supplier that related with user.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function supplierList(Request $request) 
	{
		try {
			
			$user		= Auth::user();
			
			$suppliers	= User::whereExists(function ($query) use ($user) {
									$query->select(DB::raw(1))
					                      ->from('user_relations')
					                      ->whereRaw('user_relations.parent = '.$user->id.' AND user_relations.child = users.id');
								})
								->where('type', '=', 2)
								->join('user_relations', function($join) use ($user) {
									$join->on('user_relations.child', '=', 'users.id')
										 ->where('user_relations.parent', '=', $user->id);
								})
								->select('users.id', 
										 'users.name', 
										 'users.store_name', 
										 'users.email', 
										 'users.phone', 
										 'users.profile_picture', 
										 'user_relations.status as relation_status')
								->paginate(10);
        	
			return view('relations.suppliers_list')->with('suppliers', $suppliers);
			
        } catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
	}

	/**
	 * Display the list of reseller that related with user.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function resellerList(Request $request) 
	{
		try {
			
			$this->authorize('viewResellers', new Relations());
			
			$user		= Auth::user();
			
			$resellers	= User::whereExists(function ($query) use ($user) {
									$query->select(DB::raw(1))
					                      ->from('user_relations')
					                      ->whereRaw('user_relations.parent = '.$user->id.' AND user_relations.child = users.id');
								})
								->where('type', '=', 1)
								->join('user_relations', function($join) use ($user) {
									$join->on('user_relations.child', '=', 'users.id')
										 ->where('user_relations.parent', '=', $user->id);
								})
								->select('users.id', 
										 'users.name', 
										 'users.store_name', 
										 'users.email', 
										 'users.phone', 
										 'users.profile_picture', 
										 'user_relations.status as relation_status')
								->paginate(10);
        	
			return view('relations.resellers_list')->with('resellers', $resellers);
			
        } catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) 
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) 
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) 
	{
		//
	}

	/**
	 * Store user relations between supplier and reseller.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function add(Request $request) 
	{
		
		try {
			
			$this->authorize('create', new Relations());
			
			// Make validation rules
			$this->validate($request, [
	            'supplier'		=> 'required|numeric|exists:users,id'
	        ]);
			
			DB::beginTransaction();
        	
			$user = Auth::user();
	        
			// Run insert data process
	        if(Relations::where('parent', '=', $user->id)->where('child', '=', $request->supplier)->count() == 0){
	        	
				Relations::create([	'parent'	=>$user->id, 
									'child'		=>$request->supplier, 
									'requestant'=>$user->id, 
									'request_to'=>$request->supplier
								  ]);
								  
				Relations::create([	'parent'	=>$request->supplier, 
									'child'		=>$user->id, 
									'requestant'=>$user->id, 
									'request_to'=>$request->supplier
								  ]);
				
	        }
			
			DB::commit();
			
			return redirect()->action('RelationsController@supplierList')->with('success', trans('resisten.Supplier Added Successfully'));
			
		} catch (AuthorizationException $e) {
            Log::error($e);
			DB::rollback();
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
			DB::rollback();
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
		
	}

	/**
	 * Accept request of user relation.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function accept(Request $request) 
	{
		
		try {
			
			// Make validation rules
			$this->validate($request, [
	            'requestant' => 'required|numeric|exists:users,id'
	        ]);
			
			DB::beginTransaction();
        	
			$user 		= Auth::user();
			$requestant	= User::findOrFail($request->requestant);
			
			$this->authorize('requestApproval', Relations::where('parent', '=', $user->id)->where('child', '=', $request->requestant)->first());
	        
			// Run update data process
	        Relations::where('parent', '=', $user->id)->where('child', '=', $request->requestant)->update(['status'=> '2']);
	        Relations::where('parent', '=', $request->requestant)->where('child', '=', $user->id)->update(['status'=> '2']);
			
			DB::commit();
			
			if($requestant->type == 1){
				return redirect()->action('RelationsController@resellerList')->with('success', trans('resisten.Request accepted'));
			} else {
				return redirect()->action('RelationsController@supplierList')->with('success', trans('resisten.Request accepted'));
			}
			
		} catch (AuthorizationException $e) {
            Log::error($e);
			DB::rollback();
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
			DB::rollback();
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
		
	}

	/**
	 * Reject request of user relation.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function reject(Request $request) 
	{
		
		try {
			
			// Make validation rules
			$this->validate($request, [
	            'requestant' => 'required|numeric|exists:users,id'
	        ]);
			
			DB::beginTransaction();
        	
			$user 		= Auth::user();
			$requestant	= User::findOrFail($request->requestant);
			
			$this->authorize('requestApproval', Relations::where('parent', '=', $user->id)->where('child', '=', $request->requestant)->first());
	        
			// Run update data process
	        Relations::where('parent', '=', $user->id)->where('child', '=', $request->requestant)
	        		 ->where('status', '=', '1')->delete();
	        Relations::where('parent', '=', $request->requestant)->where('child', '=', $user->id)
	        		 ->where('status', '=', '1')->delete();
			
			DB::commit();
			
			return redirect()->action('NotificationsController@index')->with('success', trans('resisten.Request rejected'));
			
		} catch (AuthorizationException $e) {
            Log::error($e);
			DB::rollback();
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
			DB::rollback();
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
		
	}

	/**
	 * Get the suppliers list from search param.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function search(Request $request) 
	{
		
		try {
			
			$this->authorize('create', new Relations());
			
			if(!$request->has('s')) throw new Exception('Please insert name, store name, category or phone!');
			$user		= Auth::user();
			
			$categories	= Terms::whereNotExists(function ($query) use ($user, $request) {
									$query->select(DB::raw(1))
					                      ->from('user_relations')
					                      ->whereRaw('user_relations.parent = '.$user->id.' AND user_relations.child = terms.user_id');
								})
								->whereRaw(self::getCategoriesSearchParam($request->s, $user->id))
								->get();
			
			$suppliers	= User::whereNotExists(function ($query) use ($user, $request) {
									$query->select(DB::raw(1))
					                      ->from('user_relations')
					                      ->whereRaw('user_relations.parent = '.$user->id.' AND user_relations.child = users.id');
								})
								->where('type', '=', 2)
								->whereRaw(self::getSupplierSearchParam($request->s, $user->id))
								->select('id', 'name', 'store_name', 'profile_picture')
								->get();
			
			$result = '';
			
			if(count($categories) > 0 || count($suppliers) > 0){
			
				if(count($categories) > 0){
					$result = '<h3 style="margin-top: 0;">'.trans('resisten.Categories List').'</h3><div class="row">';
					foreach ($categories as $category) {
						$propict = !is_null($category->user->profile_picture) ? url('show_image/'.$category->user->profile_picture) : 'http://placehold.it/160x160';
						$store	 = !is_null($category->user->store_name) ? $category->user->store_name : '-';
						$result .= '<div class="supplier-list clearfix">
										<a href="'.action('RelationsController@show', [$category->user->id]).'">
											<div class="col-xs-10">'.
												'<div class="user-block">
													<img class="img-circle" src="'.$propict.'" alt="User Image">
													<span class="username">'.
														$category->name.' '.trans('resisten.on').' '.$category->user->name.'
													</span>
													<span class="description">'.$store.'</span>
												</div>
											</div>
											<div class="col-xs-2">
												<form action="'.action('RelationsController@add').'" method="POST">
													'.csrf_field().'
													<input type="hidden" name="supplier" value="'.$category->user->id.'" />
													<button class="btn btn-danger pull-right" type="submit">'.strtoupper(trans('resisten.Add')).'</button>
												</form>
											</div>
										</a>
									</div>';
					}
					$result .= '</div><hr />';
				}
				
				if(count($suppliers) > 0){
					$result .= '<h3 style="margin-top: 0;">'.trans('resisten.Suppliers List').'</h3><div class="row">';
					foreach ($suppliers as $supplier) {
						$propict = !is_null($supplier->profile_picture) ? url('show_image/'.$supplier->profile_picture) : 'http://placehold.it/160x160';
						$store	 = !is_null($supplier->store_name) ? $supplier->store_name : '-';
						$result .= '<div class="supplier-list clearfix">
										<a href="'.action('RelationsController@show', [$supplier->id]).'">
											<div class="col-xs-10">
													<div class="user-block">
														<img class="img-circle" src="'.$propict.'" alt="User Image">
														<span class="username">'.$supplier->name.'</span>
														<span class="description">'.$store.'</span>
													</div>
											</div>
											<div class="col-xs-2">
												<form action="'.action('RelationsController@add').'" method="POST">
													'.csrf_field().'
													<input type="hidden" name="supplier" value="'.$supplier->id.'" />
													<button class="btn btn-danger pull-right" type="submit">'.strtoupper(trans('resisten.Add')).'</button>
												</form>
											</div>
										</a>
									</div>';
					}
					$result .= '</div><hr />';
				}

			} else {
				$result .= trans('resisten.User Not Found');
			}
			
			echo $result;
			
        } catch (AuthorizationException $e) {
            Log::error($e);
            return trans('resisten.This Action is Unauthorized');
        } catch (\Exception $e) {
            Log::error($e);
            return $e->getMessage();
        }
		
	}

	/**
	 * Get the valid search parameter.
	 *
	 * @param  String  $str
	 * @return String $result
	 */
	public static function getSupplierSearchParam($str, $userId) {

		$all = explode(" ", $str);
		$result = 'id !="'.$userId.'" AND (';

		for ($i = 0; $i < count($all); $i++) {

			$result .= "name LIKE '%" . $all[$i] . "%' OR store_name LIKE '%" . $all[$i] . "%' OR phone LIKE '%" . $all[$i] . "%' OR email LIKE '%" . $all[$i] . "%' ";
			if ($i + 1 < count($all)) {
				$result .= "OR ";
			}

		}

		return $result.')';

	}

	/**
	 * Get the valid search parameter.
	 *
	 * @param  String  $str
	 * @return String $result
	 */
	public static function getCategoriesSearchParam($str, $userId) {

		$all = explode(" ", $str);
		$result = 'user_id !="'.$userId.'" AND (';

		for ($i = 0; $i < count($all); $i++) {

			$result .= "name LIKE '%" . $all[$i] . "%' ";
			if ($i + 1 < count($all)) {
				$result .= "OR ";
			}

		}

		return $result.')';

	}
	
}
