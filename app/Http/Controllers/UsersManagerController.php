<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth, DB, Exception, Image, Log, Storage, Validator;

use App\Http\Controllers\Api\ApiProductsController;
use App\Http\Controllers\Api\ApiOrdersController;

use App\User;
use App\Orders;

class UsersManagerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
			
			$this->authorize('seeIndex', new User());
			
			$users = User::where('id', '!=', Auth::id())->orderBy('name')->paginate(10);
			if($request->has('s')){
				$users = User::whereRaw(RelationsController::getSupplierSearchParam($request->s, Auth::id()))->orderBy('name')->paginate(10);
			}
			
			$top_supplier = Orders::select(DB::raw('seller_id, count(*) as total_orders'))->groupBy('seller_id')
									->orderBy('total_orders', 'desc')->with('seller')->first();
			
			$top_reseller = Orders::select(DB::raw('buyer_id, count(*) as total_orders'))->where('buyer_type', '=', '1')->groupBy('buyer_id')
									->orderBy('total_orders', 'desc')->with('buyer')->first();
			
			return view('users_manager.index')->with('users', $users)->with('top_supplier', $top_supplier)
											  ->with('top_reseller', $top_reseller);
			
		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
			
			$this->authorize('view', User::findOrFail($id));
			
			$user = User::findOrFail($id);
			
			return view('users_manager.show')->with('user', $user);
			
		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
			
			$this->authorize('update', User::findOrFail($id));
			
			$user = User::findOrFail($id);
			
			// Get provinces
			$getProvinces	= ApiOrdersController::simple_get_curl('http://pro.rajaongkir.com/api/province', ['key'=>'b61074ec862c34d910e3e11c0b38c29b']);
			$provinces		= json_decode($getProvinces);
			
			$cities			= null;
			if($user->province_id){
				// Get cities
				$getCities		= ApiOrdersController::simple_get_curl('http://pro.rajaongkir.com/api/city', [
																												'key'=>'b61074ec862c34d910e3e11c0b38c29b',
																												'province'=> old('province_id', $user->province_id),
																											 ]);
				$rCities		= json_decode($getCities);
				if(isset($rCities->rajaongkir->results))
					$cities = $rCities->rajaongkir->results;
			}
			
			$subdistricts	= null;
			if($user->city_id){
				// Get subdistricts
				$getSubdistricts= ApiOrdersController::simple_get_curl('http://pro.rajaongkir.com/api/subdistrict', [
																												'key'=>'b61074ec862c34d910e3e11c0b38c29b',
																												'city'=> old('city_id', $user->city_id),
																											 ]);
				$rSubdistricts	= json_decode($getSubdistricts);
				if(isset($rSubdistricts->rajaongkir->results))
					$subdistricts = $rSubdistricts->rajaongkir->results;
			}
			
			return view('users_manager.edit')->with('user', $user)
											  ->with('provinces', $provinces->rajaongkir->results)
											  ->with('cities', $cities)
											  ->with('subdistricts', $subdistricts);
			
		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
			
			// Make validation rules
			$this->validate($request, [
	            'email' 			=> 'required|email|max:191',
	            'name' 				=> 'required|max:191',
	            'phone' 			=> 'required|max:191',
	            'province_id'		=> 'required|numeric|min:1',
	            'province_name'		=> 'required',
	            'city_id'			=> 'required|numeric|min:1',
	            'city_name'			=> 'required',
	            'subdistrict_id'	=> 'required|numeric|min:1',
	            'subdistrict_name'	=> 'required',
	        ]);
			
			DB::beginTransaction();
			
			$user 					= User::findOrFail($id);
			
			$user->email			= $request->email;
			$user->name				= $request->name;
			$user->phone			= $request->phone;
			$user->province_id		= $request->province_id;
			$user->province_name	= $request->province_name;
			$user->city_id			= $request->city_id;
			$user->city_name		= $request->city_name;
			$user->subdistrict_id	= $request->subdistrict_id;
			$user->subdistrict_name	= $request->subdistrict_name;
			
			if($request->has('store_name'))
				$user->store_name = $request->store_name;
			
			if($request->has('address'))
				$user->address = $request->address;
			
			// Check the request has file uploaded
			if ($request->hasFile('profile_picture') && $request->file('profile_picture')->isValid()) {
			    	
			    $filename 		= sha1(md5(microtime())).'_'.$user->id.'_'.time();
				$path 			= $request->profile_picture->path();
				$extension 		= $request->profile_picture->extension();
				
				$img 			= Image::make($path)
			    					->fit(400, 400);
				
				Storage::disk('google')->put($filename.'.'.$extension, $img->encode($extension));
				
				$listContents 	= Storage::disk('google')->listContents();
				$id 			= ApiProductsController::getId($listContents, 'filename', $filename);
				$request->session()->put('uploadedFile', $id['path']);
				
			}
			
			if($request->session()->has('uploadedFile')){
				
				Storage::disk('google')->delete($user->profile_picture);
				$user->profile_picture = $request->session()->pull('uploadedFile');;
				
			}
			
			$user->save(); // Save changes
			
			DB::commit();
			
			return redirect()->action('UsersManagerController@index')->with('success', trans('resisten.User\'s profile updated successfully'));
			
		} catch (AuthorizationException $e) {
			if ($request->session()->has('uploadedFile')) {
				Storage::disk('google')->delete($request->session()->pull('uploadedFile'));
			}
			DB::rollback();
            Log::error($e);
            return redirect()->action('UsersManagerController@index')->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
			if ($request->session()->has('uploadedFile')) {
				Storage::disk('google')->delete($request->session()->pull('uploadedFile'));
			}
			DB::rollback();
            Log::error($e);
            return redirect()->action('UsersManagerController@index')->with('error', $e->getMessage())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // try {
// 			
			// $this->authorize('delete', User::findOrFail($id));
// 			
			// DB::beginTransaction();
// 			
			// $user = User::findOrFail($id);
			// $user->delete();
// 			
			// DB::commit();
// 			
			// return redirect()->action('UsersManagerController@index')->with('success', trans('resisten.User deleted successfully!'));
// 			
		// } catch (AuthorizationException $e) {
			// DB::rollback();
            // Log::error($e);
            // return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        // } catch (\Exception $e) {
			// DB::rollback();
            // Log::error($e);
            // //Avoid Redirect Loop
            // if (url()->previous() == url()->current())
                // return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());
// 
            // return redirect()->back()->with('error', $e->getMessage());
        // }
    }

	/**
     * Change user status.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changeStatus($id)
    {
    	try {
			
			$this->authorize('changeStatus', User::findOrFail($id));
			
			$user = User::findOrFail($id);
			
			DB::beginTransaction();
			
			$status			= $user->status == 1 ? 'disabled' : 'enabled';
			$user->status 	= $user->status == 1 ? '0' : '1';
			$user->save();
			
			DB::commit();
			
			return redirect()->action('UsersManagerController@index')->with('success', trans('resisten.User '.$status));
			
		} catch (AuthorizationException $e) {
			DB::rollback();
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
			DB::rollback();
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
	}
     
}
