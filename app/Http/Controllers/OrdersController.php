<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use Auth, DB, Exception, finfo, Image, Log, Storage, Validator;

use App\Http\Controllers\Api\ApiProductsController;
use App\Http\Controllers\Api\ApiOrdersController;

use App\User;
use App\Orders;
use App\Products;
use App\Relations;
use App\ShippingAgents;

class OrdersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {

			$user = User::findOrFail(Auth::id());

			if(Auth::user()->hasRole('supplier')){
				$orders 	= $user->orders_in()->with('seller', 'buyer', 'product', 'courier')
												->orderBy('created_at', 'desc')
												->paginate(10);
				if($request->data == 'out')	{
					$orders	= $user->orders_out()->with('seller', 'buyer', 'product', 'courier')
											 	 ->orderBy('created_at', 'desc')
											 	 ->paginate(10);
				}
			} elseif(Auth::user()->hasRole('reseller')) {
				$orders	= $user->orders_out()->with('seller', 'buyer', 'product', 'courier')
											 ->orderBy('created_at', 'desc')
											 ->paginate(10);
			}

			return view('orders.index')->with('orders', $orders)->with('data', $request->data);

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
	 *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function createOrder($id)
    {
        try {

			$this->authorize('create', new Orders());
			$this->authorize('order', Products::findOrFail($id));

			$user        = Auth::user();

			$related	= User::whereExists(function ($query) use ($user) {
    						        $query->select(DB::raw(1))
    					        		  ->from('user_relations')
    					        		  ->whereRaw('user_relations.parent = '.$user->id.' AND user_relations.child = users.id');
					            })->count();

			if($related <= 0)
				throw new Exception(trans('resisten.This Action is Unauthorized'));

			$product     = Products::findOrFail($id);

			// Get provinces
			$getProvinces= ApiOrdersController::simple_get_curl('http://pro.rajaongkir.com/api/province', ['key'=>'b61074ec862c34d910e3e11c0b38c29b']);
			$provinces   = json_decode($getProvinces);

			// Get Shipping Agents
			$couriers    = ShippingAgents::all();

			return view('orders.create')->with('product', $product)
                                        ->with('provinces', $provinces->rajaongkir->results)
										->with('couriers', $couriers);

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

			$this->authorize('create', new Orders());

			// Make validation rules
			$this->validate($request, [
	            'product_id'				=> 'required|exists:products,id',
	            'count' 					=> 'required|numeric|min:1',
	            'total_weight' 				=> 'required|numeric|min:1',
	            'total_price' 				=> 'required|numeric|min:1',
	            'shipping_province_id' 		=> 'required|numeric|min:1',
	            'shipping_city_id' 			=> 'required|numeric|min:1',
	            'shipping_subdistrict_id' 	=> 'required|numeric|min:1',
	            'shipping_cost' 			=> 'required|numeric|min:1',
	            'shipping_name' 			=> 'required',
	            'shipping_phone' 			=> 'required',
	            'shipping_address' 			=> 'required',
	            'shipping_agents_id' 		=> 'required|exists:shipping_agents,id',
	            'shipping_service_code' 	=> 'required|numeric|min:1',
	        ]);

			DB::beginTransaction();

			$user = Auth::user();

			// Run input process
			$product = Products::findOrFail($request->product_id);
			if($product->stock < $request->count) throw new Exception(trans('resisten.Sorry, this product stock is not enough!'));

			$relations = Relations::where('parent', '=', $user->id)->where('child', '=', $product->user_id)
									->where('status', '=', 2)->first();

			if($user->type == 1){

				if($product->show_to == 2 || !$relations) throw new Exception(trans('resisten.You are not allowed to order this product!'));

			} elseif($user->type == 2){

				if($product->show_to == 1 || !$relations) throw new Exception(trans('resisten.You are not allowed to order this product!'));

			}

			$serviceCode = $request->session()->get('shipping_service');

			$order = Orders::create($request->all());
			$order->shipping_service_code	= $serviceCode[$request->shipping_service_code]['service'];
			$order->seller_id 				= $product->supplier->id;
			$order->buyer_id 				= $user->id;
			$order->buyer_type 				= $user->type;
			$order->total_commisions		= $product->commision * $request->count;
			$order->save();
			$product->stock 				-= $request->count;
			$product->save();

			DB::commit();

			$request->session()->forget('shipping_service');

			return view('orders.success_page')->with('product', $product)
													   ->with('order', $order)
													   ->with('supplier', $product->supplier);

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage())->withInput();

            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {

			$this->authorize('view', Orders::findOrFail($id));

			$order = Orders::findOrFail($id);

			return view('orders.show')->with('order', $order)->with('supplier', $order->seller);

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

	/**
     * Change order status to packed
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function packed(Request $request)
	{

		try {

			$this->authorize('packing', Orders::findOrFail($request->order_id));

			// Make validation rules
			$this->validate($request, [
	            'order_id'	=> 'required|exists:orders,id',
	        ]);

			DB::beginTransaction();

			$order = Orders::findOrFail($request->order_id);
			if(!$order) throw new Exception(trans('resisten.Order Not Found'));
			$order->shipping_status = 'PACKED';
			$order->save();

			DB::commit();

			return redirect()->action('NotificationsController@index')->with('success', trans('resisten.Order processed'));

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }

	}

    /**
     * Display a listing of the orders.
     *
     * @return \Illuminate\Http\Response
     */
	public function paymentList()
	{
		try {

			$this->authorize('payments', new Orders());

			$user = Auth::user();

			$orders = $user->orders_out()->whereNull('payment_image')
										 ->with('seller', 'buyer', 'product', 'courier')
										 ->orderBy('created_at', 'desc')
										 ->paginate(10);

			return view('orders.payment_list')->with('orders', $orders);

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
	}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function paymentDetails($id)
    {
        try {

			$this->authorize('seePayments', Orders::findOrFail($id));

			$order = Orders::findOrFail($id);

			return view('orders.payment_details')->with('order', $order)->with('supplier', $order->seller);

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Store payment image for payment confirmation.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function payment(Request $request)
	 {
	 	try {

			$this->authorize('seePayments', Orders::findOrFail($request->order_id));

			// Make validation rules
			$this->validate($request, [
	            'order_id'	=> 'required|exists:orders,id',
	            'image'		=> 'required|image',
	        ]);

			DB::beginTransaction();

			$user = User::findOrFail(Auth::id());

			$order = Orders::findOrFail($request->order_id);
			if(!$order) throw new Exception(trans('resisten.Order Not Found'));

			if($user->id <> $order->buyer_id) throw new Exception(trans('resisten.You are not allowed to do this action'));

			// Check the request has file uploaded
			if ($request->hasFile('image') && $request->file('image')->isValid()) {

			    $filename 		= sha1(md5(microtime())).'_'.$user->id.'_'.time();
				$path 			= $request->image->path();
				$extension 		= $request->image->extension();

				$img 			= Image::make($path)
								  ->resize(600, null, function ($constraint) {
								      $constraint->aspectRatio();
								  });

				Storage::put('public/'.$filename.'.'.$extension, $img->encode($extension));
                $image_name['url']				= Storage::url($filename.'.'.$extension);
				$image_name['filename']			= $filename;
				$image_name['extension']		= $extension;
				$request->session()->push('uploadedFile', $image_name);

			}

			if($request->session()->has('uploadedFile')){
                if($order->payment_image){
                    foreach (json_decode($order->payment_image, true) as $image) {
                        Storage::delete('public/'.$image['filename'].'.'.$image['extension']);
                    }
                }
                $order->payment_image = json_encode($request->session()->pull('uploadedFile'));
			}

			$order->save(); // save changes

			DB::commit();

			return redirect()->action('OrdersController@paymentList')->with('success', trans('resisten.Payment Confirmation Image Inserted Successfully'));

		} catch (AuthorizationException $e) {
            if ($request->session()->has('uploadedFile')) {
				foreach($request->session()->pull('uploadedFile') as $image){
					Storage::delete('public/'.$image['filename'].'.'.$image['extension']);
				}
			}
            DB::rollback();
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            if ($request->session()->has('uploadedFile')) {
				foreach($request->session()->pull('uploadedFile') as $image){
					Storage::delete('public/'.$image['filename'].'.'.$image['extension']);
				}
			}
            DB::rollback();
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
	 }

	/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function shipment()
    {
        try {

			$this->authorize('seeShipments', new Orders());

			$user = Auth::user();

			$orders = $user->orders_in()->whereNotNull('payment_image')
										->whereNull('shipping_code')
										->where('shipping_status', '=', 'PACKED')
										->with('seller', 'buyer', 'product', 'courier')
										->orderBy('created_at')
										->paginate(10);

			return view('orders.shipment')->with('orders', $orders);

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

	/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function shippingDetails($id)
    {
        try {

			$this->authorize('seeShippingDetails', Orders::findOrFail($id));

			$order = Orders::findOrFail($id);

			return view('orders.shipping_details')->with('order', $order);

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

	/**
     * Insert shipping code and change order status
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function shippingCode(Request $request)
	{

		try {

			// Make validation rules
			$this->validate($request, [
	            'order_id'		=> 'required|exists:orders,id',
	            'shipping_code'	=> 'required',
	        ]);

			DB::beginTransaction();

			$user = User::findOrFail(Auth::id());

			$order = Orders::findOrFail($request->order_id);
			if(!$order) throw new Exception(trans('resisten.Order Not Found'));
			$order->shipping_date	= Carbon::now()->toDateString();
			$order->shipping_code 	= $request->shipping_code;
			$order->shipping_status = 'ON PROCESS';
			$order->save();

			DB::commit();

			return redirect()->action('OrdersController@shipment')->with('success', trans('resisten.Shipping code submitted'));

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }

	}

	/**
     * Update order shipping status
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateShippingStatus(Request $request)
	{

		try {

			// Make validation rules
			$this->validate($request, [
	            'order_id'		=> 'required|exists:orders,id',
	        ]);

			DB::beginTransaction();

			$user = User::findOrFail(Auth::id());

			$order = Orders::findOrFail($request->order_id);
			if(!$order) throw new Exception(trans('resisten.Order Not Found'));
			if(!$order->shipping_code) throw new Exception(trans('resisten.Shipping code not found'));

			// Check status from rajaongkir
			$post_data = [
						 	'key'		=>'b61074ec862c34d910e3e11c0b38c29b',
							'waybill'	=>$order->shipping_code,
						 	'courier'	=>$order->courier->code,
						 ];

			$check_status = ApiOrdersController::simple_post_curl("http://pro.rajaongkir.com/api/waybill", $post_data);
			if(!$check_status) throw new Exception(trans('resisten.No response from shipment provider'));

			$response	= json_decode($check_status);
			if(!isset($response->rajaongkir->result)) throw new Exception($response->rajaongkir->status->description);

			$status		= $response->rajaongkir->result->summary->status;
			if(!$status) throw new Exception($response->rajaongkir->status->description);

			$order->shipping_status = $status;
			$order->save();

			DB::commit();

			return redirect()->action('OrdersController@index');

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }

	}

	/**
     * Get cities list
     *
     * @return \Illuminate\Http\Response
     */
	public function getCities(Request $request)
	{
		try {

			$this->authorize('create', new Orders());

			// Get Cities
			$getCities		= ApiOrdersController::simple_get_curl('http://pro.rajaongkir.com/api/city',
																	[
																		'key'		=> 'b61074ec862c34d910e3e11c0b38c29b',
																		'province'	=> $request->province,
																	]);
			$cities			= json_decode($getCities);

			$result = '<option value="0">'.trans('resisten.-- Select City --').'</option>';
			foreach ($cities->rajaongkir->results as $city) {
				$selectedCity = $request->selected == $city->city_id ? 'selected' : '';
				$result .= '<option value="'.$city->city_id.'" '.$selectedCity.'>'.$city->type.' '.$city->city_name.'</option>';
			}

			echo $result;

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
	}

	/**
     * Get subdistricts list
     *
     * @return \Illuminate\Http\Response
     */
	public function getSubdistrict(Request $request)
	{
		try {

			$this->authorize('create', new Orders());

			// Get subdistrict
			$getSubdistricts	= ApiOrdersController::simple_get_curl('http://pro.rajaongkir.com/api/subdistrict',
														[
															'key'	=> 'b61074ec862c34d910e3e11c0b38c29b',
															'city'	=> $request->city,
														]);
			$subdistricts		= json_decode($getSubdistricts);

			$result = '<option value="0">'.trans('resisten.-- Select Subdistrict --').'</option>';
			foreach ($subdistricts->rajaongkir->results as $subdistrict) {
				$selectedSubdistrict = $request->selected == $subdistrict->subdistrict_id ? 'selected' : '';
				$result .= '<option value="'.$subdistrict->subdistrict_id.'" '.$selectedSubdistrict.'>'.$subdistrict->subdistrict_name.'</option>';
			}

			echo $result;

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
	}

	/**
     * Get shipping service
     *
     * @return \Illuminate\Http\Response
     */
	public function getShippingServices(Request $request)
	{
		try {

			$this->authorize('create', new Orders());

			// Get shipping services
			$getShippingServices	= ApiOrdersController::simple_post_curl('http://pro.rajaongkir.com/api/cost',
																	[
																		'key'				=> 'b61074ec862c34d910e3e11c0b38c29b',
																		'origin'			=> $request->origin,
																		'originType'		=> 'subdistrict',
																		'destination'		=> $request->destination,
																		'destinationType'	=> 'subdistrict',
																		'weight'			=> $request->weight,
																		'courier'			=> $request->courier,
																	]);
			$services = json_decode($getShippingServices);
			$request->session()->forget('shipping_service');

			$result = '<option value="0" data-cost="0">'.trans('resisten.-- Select Service --').'</option>';
			session(['shipping_service.0.service' => trans('resisten.-- Select Service --'), 'shipping_service.0.cost' => '0']);
			$n=1;
			foreach ($services->rajaongkir->results[0]->costs as $service) {

				session([
							'shipping_service.'.$n.'.service' => $service->service,
							'shipping_service.'.$n.'.cost' => $service->cost[0]->value
						]);

				$result .= '<option value="'.$n.'" data-cost="'.$service->cost[0]->value.'">
								'.$service->service.' ( '. 'Rp. ' . number_format( $service->cost[0]->value, 2, ',', '.') .' )
							</option>';
				$n++;

			}

			echo $result;

		} catch (AuthorizationException $e) {
            Log::error($e);
            echo trans('resisten.This Action is Unauthorized');
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            echo $e->getMessage();
        }
	}

}
