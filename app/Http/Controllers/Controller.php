<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function getDashboardAction()
    {
        if (Auth::user()->hasRole('admin|supplier|reseller|customer'))
            return ('DashboardController@index');
        //NO ROLE Accepted
        else {
            Log::error('User Have No Role #'. Auth::user()->id);
            Auth::logout();
            return ('Auth\LoginController@showLoginForm');
        }
    }
}
