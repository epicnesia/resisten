<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use Auth, DB, Exception, Hash, Image, Log, Storage, Validator;

use App\Http\Controllers\Api\ApiProductsController;
use App\Http\Controllers\Api\ApiOrdersController;

use App\Banks;
use App\User;
use App\Targets;

class SettingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function profile()
    {
        try {

			$user 		= User::findOrFail(Auth::id());

			$targets	= $user->targets()->where('month', '=', Carbon::now()->month)
										  ->where('year', '=', Carbon::now()->year)
										  ->first();

			$omzet		= 0;
			$commision	= 0;
			if(!is_null($targets)){
				$omzet 		= $targets->omzet;
				$commision	= $targets->commision;
			}

			// Get provinces
			$getProvinces	= ApiOrdersController::simple_get_curl('http://pro.rajaongkir.com/api/province', ['key'=>'b61074ec862c34d910e3e11c0b38c29b']);
			$provinces		= json_decode($getProvinces);

			$cities			= null;
			if($user->province_id){
				// Get cities
				$getCities		= ApiOrdersController::simple_get_curl('http://pro.rajaongkir.com/api/city', [
																												'key'=>'b61074ec862c34d910e3e11c0b38c29b',
																												'province'=> old('province_id', $user->province_id),
																											 ]);
				$rCities		= json_decode($getCities);
				if(isset($rCities->rajaongkir->results))
					$cities = $rCities->rajaongkir->results;
			}

			$subdistricts	= null;
			if($user->city_id){
				// Get subdistricts
				$getSubdistricts= ApiOrdersController::simple_get_curl('http://pro.rajaongkir.com/api/subdistrict', [
																												'key'=>'b61074ec862c34d910e3e11c0b38c29b',
																												'city'=> old('city_id', $user->city_id),
																											 ]);
				$rSubdistricts	= json_decode($getSubdistricts);
				if(isset($rSubdistricts->rajaongkir->results))
					$subdistricts = $rSubdistricts->rajaongkir->results;
			}

        	return view('settings.profile')->with('user', $user)
        											->with('provinces', $provinces->rajaongkir->results)
        											->with('cities', $cities)
        											->with('subdistricts', $subdistricts)
													->with('omzet', $omzet)
													->with('commision', $commision);

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function updateProfile(Request $request)
    {
        try {

			// Make validation rules
			if(Auth::user()->hasRole('admin')){
				$this->validate($request, [
		            'email' 			=> 'required|email|max:191',
		            'name' 				=> 'required|max:191',
		            'phone' 			=> 'required|max:191',
		        ]);
			} else {
				$this->validate($request, [
		            'email' 			=> 'required|email|max:191',
		            'name' 				=> 'required|max:191',
		            'phone' 			=> 'required|max:191',
		            'province_id'		=> 'required|numeric|min:1',
		            'province_name'		=> 'required',
		            'city_id'			=> 'required|numeric|min:1',
		            'city_name'			=> 'required',
		            'subdistrict_id'	=> 'required|numeric|min:1',
		            'subdistrict_name'	=> 'required',
		        ]);
			}

			$month 	= Carbon::now()->month;
			$year 	= Carbon::now()->year;

			DB::beginTransaction();

			$user 				= Auth::user();

			if($request->has('new_password')){
				if(!Hash::check($request->old_password, $user->password))
					throw new Exception(trans('resisten.If you want to change password, please insert your old password!'));
				$user->password = bcrypt($request->new_password);
			}

			$user->email			= $request->email;
			$user->name				= $request->name;
			$user->phone			= $request->phone;
			$user->province_id		= $request->province_id;
			$user->province_name	= $request->province_name;
			$user->city_id			= $request->city_id;
			$user->city_name		= $request->city_name;
			$user->subdistrict_id	= $request->subdistrict_id;
			$user->subdistrict_name	= $request->subdistrict_name;

			if($request->has('store_name'))
				$user->store_name = $request->store_name;

			if($request->has('address'))
				$user->address = $request->address;


			// Save targets if exists
			if($request->has('omzet') || $request->has('commision')){

				$target	= Targets::where('user_id', '=', $user->id)->where('month', '=', $month)
								 ->where('year', '=', $year)->first();

				if(!is_null($target)){

					if($request->has('omzet'))
						$target->omzet = $request->omzet;
					if($request->has('commision'))
						$target->commision = $request->commision;

					$target->save();

				} else {

					Targets::create([
									'user_id'	=> $user->id,
									'month'		=> $month,
									'year'		=> $year,
									'omzet'		=> $request->omzet,
									'commision'	=> $request->commision,
									]);

				}

			}

			// Check the request has file uploaded
			if ($request->hasFile('profile_picture') && $request->file('profile_picture')->isValid()) {

			    $filename 		= sha1(md5(microtime())).'_'.$user->id.'_'.time();
				$path 			= $request->profile_picture->path();
				$extension 		= $request->profile_picture->extension();

				$img 			= Image::make($path)
			    					->fit(400, 400);

				Storage::put($filename.'.'.$extension, $img->encode($extension));

                Storage::put('public/'.$filename.'.'.$extension, $img->encode($extension));
                $image_name['url']				= Storage::url($filename.'.'.$extension);
				$image_name['filename']			= $filename;
				$image_name['extension']		= $extension;
				$request->session()->push('uploadedFile', $image_name);

			}

			if($request->session()->has('uploadedFile')){
                if($user->profile_picture){
                    foreach (json_decode($user->profile_picture, true) as $image) {
                        Storage::delete('public/'.$image['filename'].'.'.$image['extension']);
                    }
                }
				$user->profile_picture = json_encode($request->session()->pull('uploadedFile'));
			}

			$user->save(); // Save changes

			DB::commit();

			return redirect()->action('SettingsController@profile')->with('success', trans('resisten.Your profile updated successfully'));

		} catch (AuthorizationException $e) {
            if ($request->session()->has('uploadedFile')) {
				foreach($request->session()->pull('uploadedFile') as $image){
					Storage::delete('public/'.$image['filename'].'.'.$image['extension']);
				}
			}
			DB::rollback();
            Log::error($e);
            return redirect()->action('SettingsController@profile')->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            if ($request->session()->has('uploadedFile')) {
				foreach($request->session()->pull('uploadedFile') as $image){
					Storage::delete('public/'.$image['filename'].'.'.$image['extension']);
				}
			}
			DB::rollback();
            Log::error($e);
            return redirect()->action('SettingsController@profile')->with('error', $e->getMessage())->withInput();
        }
    }

    public function banks()
    {
        try {

			$user 		= User::findOrFail(Auth::id());

        	return view('settings.banks')->with('user', $user)
        										  ->with('banks', $user->banks()->paginate(30));

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function addBank()
    {
        try {

			$this->authorize('create', new Banks());

			return view('settings.addBank');

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

	public function storeBank(Request $request)
    {
        try {

			$this->authorize('create', new Banks());

			// Make validation rules
			$this->validate($request, [
	            'user_id' 			=> 'required|exists:users,id',
	            'bank_name'			=> 'required',
	            'account_number'	=> 'required',
	            'account_name'		=> 'required',
	        ]);

			DB::beginTransaction();

			Banks::create($request->all());

			DB::commit();

			return redirect()->action('SettingsController@banks')->with('success', trans('resisten.The bank added to your account'));

		} catch (AuthorizationException $e) {
            Log::error($e);
			DB::rollback();
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
			DB::rollback();
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage())->withInput();

            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
    }

	public function editBank($id)
    {
        try {

			$this->authorize('update', Banks::findOrFail($id));

			$bank = Banks::findOrFail($id);

			return view('settings.editBank')->with('bank', $bank);

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

	public function updateBank(Request $request, $id)
    {
        try {

			$this->authorize('update', Banks::findOrFail($id));

			// Make validation rules
			$this->validate($request, [
	            'user_id' 			=> 'required|exists:users,id',
	            'bank_name'			=> 'required',
	            'account_number'	=> 'required',
	            'account_name'		=> 'required',
	        ]);

			DB::beginTransaction();

			$bank 					= Banks::findOrFail($id);
			$bank->user_id 			= $request->user_id;
			$bank->bank_name 		= $request->bank_name;
			$bank->account_number 	= $request->account_number;
			$bank->account_name 	= $request->account_name;
			$bank->save();

			DB::commit();

			return redirect()->action('SettingsController@banks')->with('success', trans('resisten.The bank updated successfully'));

		} catch (AuthorizationException $e) {
            Log::error($e);
			DB::rollback();
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
			DB::rollback();
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage())->withInput();

            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
    }

	public function destroyBank($id)
    {
        try {

			$this->authorize('delete', Banks::findOrFail($id));

			DB::beginTransaction();

			$bank = Banks::findOrFail($id);
			$bank->delete();

			DB::commit();

			return redirect()->action('SettingsController@banks')->with('success', trans('resisten.The bank deleted successfully'));

		} catch (AuthorizationException $e) {
            Log::error($e);
			DB::rollback();
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
			DB::rollback();
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage())->withInput();

            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
    }

}
