<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth, DB, Exception, Log, Validator;

use App\ShippingAgents;

class ShippingAgentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {

			$this->authorize('view', new ShippingAgents());

			$couriers = ShippingAgents::orderBy('name')->paginate(10);
			if($request->has('s')){
				$couriers = ShippingAgents::whereRaw(self::getCourierSearchParam($request->s))->orderBy('name')->paginate(10);
			}

			return view('shipping_agents.index')->with('couriers', $couriers);

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {

			$this->authorize('create', new ShippingAgents());

			return view('shipping_agents.create');

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

			$this->authorize('create', new ShippingAgents());

			$this->validate($request, [
								'name' => 'required',
								'code' => 'required',
							]);

			DB::beginTransaction();

			ShippingAgents::create($request->all());

			DB::commit();

			return redirect()->action('ShippingAgentsController@index')->with('success', trans('resisten.Shipping agent created successfully.'));

		} catch (AuthorizationException $e) {
			DB::rollback();
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
        	DB::rollback();
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {

			$this->authorize('update', ShippingAgents::findOrFail($id));

			$courier = ShippingAgents::findOrFail($id);

			return view('shipping_agents.edit')->with('courier', $courier);

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

			$this->authorize('update', ShippingAgents::findOrFail($id));

			$this->validate($request, [
								'name' => 'required',
								'code' => 'required',
							]);

			DB::beginTransaction();

			$courier 				= ShippingAgents::findOrFail($id);
			$courier->name			= $request->name;
			$courier->code			= $request->code;
			$courier->description	= $request->description;
			$courier->save();

			DB::commit();

			return redirect()->action('ShippingAgentsController@index')->with('success', trans('resisten.Shipping agent updated successfully.'));

		} catch (AuthorizationException $e) {
			DB::rollback();
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
        	DB::rollback();
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

			$this->authorize('delete', ShippingAgents::findOrFail($id));

			DB::beginTransaction();

			// Run insert data process
	        $note = ShippingAgents::findOrFail($id);
			$note->delete();

	        DB::commit();

            return redirect()->action('ShippingAgentsController@index')->with('success', trans('resisten.Shipping Agent deleted successfully.'));

		} catch (AuthorizationException $e) {
			DB::rollback();
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
        	DB::rollback();
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

	public static function getCourierSearchParam($str)
	{
		$all = explode(" ", $str);
		$result = '(';

		for ($i = 0; $i < count($all); $i++) {

			$result .= "name LIKE '%" . $all[$i] . "%' OR code LIKE '%" . $all[$i] . "%'  OR description LIKE '%" . $all[$i] . "%' ";
			if ($i + 1 < count($all)) {
				$result .= "OR ";
			}

		}

		return $result.')';
	}

}
