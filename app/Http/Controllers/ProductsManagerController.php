<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth, DB, Exception, finfo, Image, Log, Session, Storage, Validator;

use App\Http\Controllers\Api\ApiProductsController;
use App\Http\Controllers\ProductsController;

use App\User;
use App\Orders;
use App\Products;
use App\Terms;
use App\TermsRelations;

class ProductsManagerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {

			$this->authorize('seeIndex', new Products());

			$products 	  = Products::latest()->paginate(10);
			if($request->has('s')){
				$products = Products::whereRaw(ProductsController::getProductsSearchParam($request->s))->latest()->paginate(10);
			}

			$top_products = Orders::select(DB::raw('product_id, count(*) as total_orders'))->groupBy('product_id')
									->orderBy('total_orders', 'desc')->with('product')->first();

			return view('products_manager.index')->with('products', $products)->with('top_products', $top_products);

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {

			$this->authorize('view', Products::findOrFail($id));

			$product = Products::findOrFail($id);

			return view('products_manager.show')->with('product', $product);

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {

			$this->authorize('update', Products::findOrFail($id));

			$product			= Products::findOrFail($id);
			$user 				= User::findOrFail($product->user_id);
			$terms				= $user->categories()->where('type', '=', 'showcase_category')->orderBy('name')->get();
			$product_categories	= [];
			foreach ($product->categories()->select('term_id as id')->get() as $key => $value) {
				$product_categories[$key] = $value->id;
			}

			if(!is_null($product->image)){

				foreach(json_decode($product->image) as $images){
                    $new_path		= 'temp/'.Auth::id().'/'.$images->filename.'.'.$images->extension;
					Storage::put($new_path, Storage::get('public/'.$images->filename.'.'.$images->extension));
				}

			}

        	return view('products_manager.edit')->with('product', $product)->with('terms', $terms)
												 		 ->with('product_categories', $product_categories);

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

			$this->authorize('update', Products::findOrFail($id));

			// Make validation rules
			$this->validate($request, [
	            'name' 			=> 'required|max:191',
	            'stock' 		=> 'required|numeric',
	            'price' 		=> 'required|numeric',
	            'commision' 	=> 'required|numeric',
	            'weight' 		=> 'required|numeric',
	            'description' 	=> 'required',
	            'show_to' 		=> 'required|numeric',
				'images' 		=> 'required|array',
	        ]);

			DB::beginTransaction();

			$product = Products::findOrFail($id);

			// Run update data process
	        $product->name			= $request->name;
	        $product->stock			= $request->stock;
	        $product->price			= $request->price;
	        $product->commision		= $request->commision;
	        $product->weight		= $request->weight;
	        $product->description	= $request->description;
	        $product->show_to		= $request->show_to;
			$product->save();

			if($request->has('showcase_categories')){

				// Delete term relations
				foreach ($product->categories()->select('term_id as id')->get() as $key => $value) {
					$terms = Terms::findOrFail($value->id);
					$terms->count -= 1;
					$terms->save();
				}
				$product->categories()->detach();

				// Fill with the new one
				foreach ($request->showcase_categories as $category) {

					TermsRelations::create([
											'product_id'=> $product->id,
											'term_id'	=> $category,
											]);

					$terms = Terms::findOrFail($category);
					$terms->count += 1;
					$terms->save();

				}
			}

			if (!isset($request->images) || empty($request->images)) throw new Exception(trans('resisten.Please select min 1 image'));

            foreach(json_decode($product->image, true) as $must_delete_image){
				Storage::delete('public/'.$must_delete_image['filename'].'.'.$must_delete_image['extension']);
			}
            
            foreach ($request->images as $key => $image) {
                if(Storage::exists('temp/'.Auth::id().'/'.$image))
					Storage::put('public/'.$image, Storage::get('temp/'.Auth::id().'/'.$image));

				$filename						= explode('.', $image);
				$image_name['url']				= Storage::url($image);
				$image_name['filename']			= $filename[0];
				$image_name['extension']		= $filename[1];
				$request->session()->push('uploadedFile', $image_name);

			}

			if($request->session()->has('uploadedFile')){
				$product->image = json_encode($request->session()->pull('uploadedFile'));
				$product->save();
			}

			Storage::deleteDirectory('temp/'.Auth::id());

			DB::commit();

            return redirect()->action('ProductsManagerController@index')->with('success', trans('resisten.Product Updated Successfully'));

		} catch (AuthorizationException $e) {
            if ($request->session()->has('uploadedFile')) {
				foreach($request->session()->pull('uploadedFile') as $image){
					Storage::delete('public/'.$image['filename'].'.'.$image['extension']);
				}
			}
			DB::rollback();
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            if ($request->session()->has('uploadedFile')) {
				foreach($request->session()->pull('uploadedFile') as $image){
					Storage::delete('public/'.$image['filename'].'.'.$image['extension']);
				}
			}
			DB::rollback();
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getLine().' '.$e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
