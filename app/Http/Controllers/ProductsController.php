<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Api\ApiProductsController;

use Auth, DB, Exception, finfo, Image, Log, Session, Storage, Validator;

use App\User;
use App\Favorites;
use App\Products;
use App\Relations;
use App\Terms;
use App\TermsRelations;

class ProductsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function newProducts()
    {
        try {

			$this->authorize('seeNewProducts', new Products());

			$user 		= User::findOrFail(Auth::id());
			$relation	= [];
			$relation	= Relations::select('child as id')->where('parent', '=', $user->id)->where('status','=','2')->get()->toArray();
			array_push($relation, $user->id);

			if($user->type == 1){
				$products	= Products::whereIn('user_id', $relation)
										-> where(function ($query) use ($user) {
											$query -> where('show_to', '=', '3')
												   -> orWhere('show_to', '=', $user->type);
										})
										-> join('users', function($join){
											$join->on('users.id', '=', 'products.user_id');
										})
										-> select('users.name as supplier_name', 'users.profile_picture',
										 		  'users.province_id', 'users.city_id',
										 		  'users.subdistrict_id', 'products.*'
												  )
										-> orderBy('created_at','desc')
										-> paginate(10);

			} elseif ($user->type == 2) {
				$products 	= Products::whereIn('user_id', $relation)
										-> where(function ($query) use ($user) {
											$query -> where('products.user_id', '=', $user->id)
												   -> orWhere('show_to', '=', '3')
												   -> orWhere('show_to', '=', $user->type);
										})
										-> join('users', function($join){
											$join->on('users.id', '=', 'products.user_id');
										})
										-> select('users.name as supplier_name', 'users.profile_picture',
										 		  'users.province_id', 'users.city_id',
										 		  'users.subdistrict_id', 'products.*'
												  )
										-> orderBy('created_at','desc')
										-> paginate(10);
			}

			$fav 		= Favorites::where('user_id', '=', $user->id)->get();
			$favorites	= [];
			foreach ($fav as $value) {
				$favorites[] = $value->product_id;
			}

        	return view('products.new_products')->with('products', $products)
        												 ->with('favorites', $favorites);

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

	public function index($slug = null)
    {
        try {

			$this->authorize('seeIndex', new Products());

			$user 				= User::findOrFail(Auth::id());
			$products 			= $user->products()-> orderBy('created_at','desc')-> paginate(10);
			if(!is_null($slug)){
				$terms			= Terms::findBySlugOrFail($slug);
				$products		= $terms->products()->paginate(10);
			}
			$showcase_categories= $user->categories()->where('type', '=', 'showcase_category')->orderBy('name')->get();

        	return view('products.index')->with('showcase_categories', $showcase_categories)
        										  ->with('products', $products);

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {

			$this->authorize('create', new Products());

			$user = User::findOrFail(Auth::id());
			$terms= $user->categories()->where('type', '=', 'showcase_category')->orderBy('name')->get();

        	return view('products.create')->with('terms', $terms);

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

			$this->authorize('create', new Products());

			// Make validation rules
			$this->validate($request, [
	            'name' 			=> 'required|max:191',
	            'stock' 		=> 'required|numeric',
	            'price' 		=> 'required|numeric',
	            'commision' 	=> 'required|numeric',
	            'weight' 		=> 'required|numeric',
	            'description' 	=> 'required',
	            'show_to' 		=> 'required|numeric',
				'images' 		=> 'required|array',
	        ]);

			DB::beginTransaction();

			$user = Auth::user();

			// Run insert data process
	        $product = Products::create([
	        	'user_id'		=> $user->id,
	            'name' 			=> $request['name'],
	            'stock' 		=> $request['stock'],
	            'price' 		=> $request['price'],
	            'commision' 	=> $request['commision'],
	            'weight' 		=> $request['weight'],
	            'description' 	=> $request['description'],
	            'show_to' 		=> $request['show_to'],
	        ]);

			if($request->has('showcase_categories')){
				foreach ($request->showcase_categories as $category) {
					TermsRelations::create([
											'product_id'=> $product->id,
											'term_id'	=> $category,
											]);
					$terms = Terms::findOrFail($category);
					$terms->count += 1;
					$terms->save();
				}
			}

			if (!isset($request->images) || empty($request->images)) throw new Exception(trans('resisten.Please select min 1 image'));

			foreach ($request->images as $key => $image) {
				if(Storage::exists('temp/'.Auth::id().'/'.$image))
					Storage::put('public/'.$image, Storage::get('temp/'.Auth::id().'/'.$image));

				$filename						= explode('.', $image);
				$image_name['url']				= Storage::url($image);
				$image_name['filename']			= $filename[0];
				$image_name['extension']		= $filename[1];
				$request->session()->push('uploadedFile', $image_name);

			}

			if($request->session()->has('uploadedFile')){
				$product->image = json_encode($request->session()->pull('uploadedFile'));
				$product->save();
			}

			Storage::deleteDirectory('temp/'.Auth::id());

			DB::commit();

            return redirect()->action('ProductsController@index')->with('success', trans('resisten.Product Inserted Successfully'));

		} catch (AuthorizationException $e) {
			if ($request->session()->has('uploadedFile')) {
				foreach($request->session()->pull('uploadedFile') as $image){
					Storage::delete('public/'.$image['filename'].'.'.$image['extension']);
				}
			}
			DB::rollback();
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
			if ($request->session()->has('uploadedFile')) {
				foreach($request->session()->pull('uploadedFile') as $image){
					Storage::delete('public/'.$image['filename'].'.'.$image['extension']);
				}
			}
			DB::rollback();
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage())->withInput();

            return redirect()->back()->with('error', $e->getMessage())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {

			$this->authorize('view', Products::findOrFail($id));

			$product	= Products::findOrFail($id);

			$user = Auth::user();

			$related	= User::whereExists(function ($query) use ($user) {
						        $query->select(DB::raw(1))
					        		  ->from('user_relations')
					        		  ->whereRaw('user_relations.parent = '.$user->id.' AND user_relations.child = users.id');
						  })->count();

			if($related <= 0)
				throw new Exception(trans('resisten.This Action is Unauthorized'));

        	return view('products.show')->with('product', $product);

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {

			$this->authorize('update', Products::findOrFail($id));

			$user 				= User::findOrFail(Auth::id());
			$terms				= $user->categories()->where('type', '=', 'showcase_category')->orderBy('name')->get();
			$product			= Products::findOrFail($id);
			$product_categories	= [];
			foreach ($product->categories()->select('term_id as id')->get() as $key => $value) {
				$product_categories[$key] = $value->id;
			}

			if(!is_null($product->image)){

				foreach(json_decode($product->image) as $images){
					$new_path		= 'temp/'.Auth::id().'/'.$images->filename.'.'.$images->extension;
					Storage::put($new_path, Storage::get('public/'.$images->filename.'.'.$images->extension));
				}

			}

        	return view('products.edit')->with('product', $product)
                                        ->with('terms', $terms)
                                        ->with('product_categories', $product_categories);

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

			$this->authorize('update', Products::findOrFail($id));

			// Make validation rules
			$this->validate($request, [
	            'name' 			=> 'required|max:191',
	            'stock' 		=> 'required|numeric',
	            'price' 		=> 'required|numeric',
	            'commision' 	=> 'required|numeric',
	            'weight' 		=> 'required|numeric',
	            'description' 	=> 'required',
	            'show_to' 		=> 'required|numeric',
				'images' 		=> 'required|array',
	        ]);

			DB::beginTransaction();

			$product = Products::findOrFail($id);

			// Run update data process
	        $product->name			= $request->name;
	        $product->stock			= $request->stock;
	        $product->price			= $request->price;
	        $product->commision		= $request->commision;
	        $product->weight		= $request->weight;
	        $product->description	= $request->description;
	        $product->show_to		= $request->show_to;
			$product->save();

			if($request->has('showcase_categories')){

				// Delete term relations
				foreach ($product->categories()->select('term_id as id')->get() as $key => $value) {
					$terms = Terms::findOrFail($value->id);
					$terms->count -= 1;
					$terms->save();
				}
				$product->categories()->detach();

				// Fill with the new one
				foreach ($request->showcase_categories as $category) {

					TermsRelations::create([
											'product_id'=> $product->id,
											'term_id'	=> $category,
											]);

					$terms = Terms::findOrFail($category);
					$terms->count += 1;
					$terms->save();

				}
			}

			if (!isset($request->images) || empty($request->images)) throw new Exception(trans('resisten.Please select min 1 image'));

            foreach(json_decode($product->image, true) as $must_delete_image){
				Storage::delete('public/'.$must_delete_image['filename'].'.'.$must_delete_image['extension']);
			}

			foreach ($request->images as $key => $image) {
                if(Storage::exists('temp/'.Auth::id().'/'.$image))
					Storage::put('public/'.$image, Storage::get('temp/'.Auth::id().'/'.$image));

				$filename						= explode('.', $image);
				$image_name['url']				= Storage::url($image);
				$image_name['filename']			= $filename[0];
				$image_name['extension']		= $filename[1];
				$request->session()->push('uploadedFile', $image_name);

			}

			if($request->session()->has('uploadedFile')){
				$product->image = json_encode($request->session()->pull('uploadedFile'));
				$product->save();
			}

			Storage::deleteDirectory('temp/'.Auth::id());

			DB::commit();

            return redirect()->action('ProductsController@index')->with('success', trans('resisten.Product Updated Successfully'));

		} catch (AuthorizationException $e) {
			if ($request->session()->has('uploadedFile')) {
				foreach($request->session()->pull('uploadedFile') as $image){
					Storage::delete('public/'.$image['filename'].'.'.$image['extension']);
				}
			}
			DB::rollback();
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
			if ($request->session()->has('uploadedFile')) {
				foreach($request->session()->pull('uploadedFile') as $image){
					Storage::delete('public/'.$image['filename'].'.'.$image['extension']);
				}
			}
			DB::rollback();
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getLine().' '.$e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function favorites()
	 {
	 	try {

			$this->authorize('seeNewProducts', new Products());

			$user 		= User::findOrFail(Auth::id());

			$fav 		= Favorites::where('user_id', '=', $user->id)->get();
			$favorites	= [];
			foreach ($fav as $value) {
				$favorites[] = $value->product_id;
			}

        	return view('products.favorites')->with('products', $user->favorites()->orderBy('created_at', 'desc')->paginate(8))
        											  ->with('favorites', $favorites);

		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
	 }

	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function updateFavorites(Request $request)
	 {
	 	try {

			// Make validation rules
			$this->validate($request, [
	            'product' 	=> 'required|exists:products,id',
	        ]);

			$user = User::findOrFail(Auth::id());

			DB::beginTransaction();

			$favorite = Favorites::where('user_id', '=', $user->id)->where('product_id', '=', $request->product)->first();

			if(!is_null($favorite)){
				$favorite->delete();
			} else {
				Favorites::create(['user_id' => $user->id, 'product_id' => $request->product]);
			}

			DB::commit();

            return 'success';

		} catch (\Exception $e) {
			DB::rollback();
            Log::error($e);
            return $e->getMessage();
        }
	 }

	 /**
	 * Get the products list from search param.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function search(Request $request)
	{

		try {

			$this->authorize('seeNewProducts', new Products());

			if(!$request->has('s')) throw new Exception(trans('resisten.No string parameter'));

			$user 		= Auth::user();
			$products 	= Products::whereRaw(self::getProductsSearchParam($request->s))->paginate(8);

			$fav 		= Favorites::where('user_id', '=', $user->id)->get();
			$favorites	= [];
			foreach ($fav as $value) {
				$favorites[] = $value->product_id;
			}

			return view('products.search')->with('products', $products)
												   ->with('favorites', $favorites);

        } catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }

	}

	/**
	 * Get the valid search parameter.
	 *
	 * @param  String  $str
	 * @return String $result
	 */
	public static function getProductsSearchParam($str) {

		$all 	= explode(" ", $str);
		$result = '';

		for ($i = 0; $i < count($all); $i++) {

			$result .= "name LIKE '%" . $all[$i] . "%' OR description LIKE '%" . $all[$i] . "%' ";
			if ($i + 1 < count($all)) {
				$result .= "OR ";
			}

		}

		return $result;

	}

	 /**
     * Store uploaded images to storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function uploadImage(Request $request)
	 {

		try {

			$this->authorize('create', new Products());

			$filename 		= sha1(md5(microtime())).'_'.Auth::id().'_'.time();
			$path 			= $request->file->path();
			$extension 		= $request->file->extension();
			$img 			= Image::make($path)->fit(800,600);
			$new_path		= 'temp/'.Auth::id().'/'.$filename.'.'.$extension;
			Storage::put($new_path, $img->encode($extension));

			return $filename.'.'.$extension;

		} catch (AuthorizationException $e) {
            Log::error($e);
        } catch (\Exception $e) {
            Log::error($e);
        }

	 }

	 /**
     * Delete uploaded images to storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function deleteImage(Request $request)
	 {

		try {

			$this->authorize('create', new Products());

			Storage::delete('temp/'.Auth::id().'/'.$request->file);

		} catch (AuthorizationException $e) {
            Log::error($e);
        } catch (\Exception $e) {
            Log::error($e);
        }

	 }

}
