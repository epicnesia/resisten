<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Carbon\Carbon;
use DB, Exception, Hash, Image, Log, Storage, Validator;

use App\Http\Controllers\Api\ApiProductsController;

use App\Banks;
use App\User;
use App\Targets;

class ApiUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
			
			// Make validation rules
			$validator = Validator::make($request->all(), [
	            'email' => 'required|email|max:191|unique:users',
	            'name' => 'required|max:191',
	            'phone' => 'required|max:191',
	            'password' => 'required|min:6',
	            'type' => 'required|numeric',
	            'android_id' => 'required',
	        ]);
			
			$errorMessage = $validator->errors();
			$message = '';
			foreach ($errorMessage->all() as $value) {
				$message .= $value.PHP_EOL;
			}
			
			// Run Validation
			if ($validator->fails()) throw new Exception($message);
			
			DB::beginTransaction();
	        
			// Run insert data process
	        $proc = User::create([
	            'email' => $request['email'],
	            'name' => $request['name'],
	            'phone' => $request['phone'],
	            'password' => bcrypt($request['password']),
	            'api_token' => sha1(md5(time())),
	            'type' => $request['type'],
	            'android_id' => $request['android_id'],
	        ]);
			
			if($request['type'] == 1) {
				$proc->assignRole('reseller');
			} else if($request['type'] == 2) {
				$proc->assignRole('supplier');
			}
			
			// Send email verification to new registered user
			/*
			Mail::send('auth.emails.verify_email', ['user' => $proc], function ($m) use ($proc) {
	            $m->from('no-reply@resisten.id', '');
				$m->to($proc->email, $proc->name)->subject('');
	        });
			*/
			
			DB::commit();
			
			return response()->json(array('status' => true, 'message' => trans('resisten.Register Success')));
			
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return response()->json(array('status' => false, 'message' => $e -> getMessage()));
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
			
			// Make validation rules
			$validator = Validator::make($request->all(), [
	            'api_token' 		=> 'bail|required|exists:users,api_token',
	            'email' 			=> 'required|email|max:191',
	            'name' 				=> 'required|max:191',
	            'phone' 			=> 'required|max:191',
	            'province_id'		=> 'required|numeric|min:1',
	            'province_name'		=> 'required',
	            'city_id'			=> 'required|numeric|min:1',
	            'city_name'			=> 'required',
	            'subdistrict_id'	=> 'required|numeric|min:1',
	            'subdistrict_name'	=> 'required',
	        ]);
			
			$errorMessage = $validator->errors();
			$message = '';
			foreach ($errorMessage->all() as $value) {
				$message .= $value.PHP_EOL;
			}
			
			// Run Validation
			if ($validator->fails()) throw new Exception($message);
			
			$month 	= Carbon::now()->month;
			$year 	= Carbon::now()->year;
			
			DB::beginTransaction();
			
			$user 				= User::where('api_token', '=', $request->api_token)->first();
			
			if($request->has('new_password')){
				if(!Hash::check($request->old_password, $user->password)) 
					throw new Exception(trans('resisten.If you want to change password, please insert your old password!'));
				$user->password = bcrypt($request->new_password);
			}
			
			$user->email			= $request->email;
			$user->name				= $request->name;
			$user->phone			= $request->phone;
			$user->province_id		= $request->province_id;
			$user->province_name	= $request->province_name;
			$user->city_id			= $request->city_id;
			$user->city_name		= $request->city_name;
			$user->subdistrict_id	= $request->subdistrict_id;
			$user->subdistrict_name	= $request->subdistrict_name;
			
			if($request->has('store_name'))
				$user->store_name = $request->store_name;
			
			if($request->has('address'))
				$user->address = $request->address;
			
			
			// Save targets if exists
			if($request->has('sales') || $request->has('turnover')){
				
				$target	= Targets::where('user_id', '=', $user->id)->where('month', '=', $month)
								 ->where('year', '=', $year)->first();
								 
				if(!is_null($target)){
					
					if($request->has('sales'))
						$target->sales = $request->sales;
					if($request->has('turnover'))
						$target->turnover = $request->turnover;
						
					$target->save();
					
				} else {
					
					Targets::create([
									'user_id'	=> $user->id,
									'month'		=> $month,
									'year'		=> $year,
									'sales'		=> $request->sales,
									'turnover'	=> $request->turnover,
									]);
					
				}
				
			}
			
			// Check the request has file uploaded
			if ($request->hasFile('profile_picture') && $request->file('profile_picture')->isValid()) {
			    	
			    $filename 		= sha1(md5(microtime())).'_'.$user->id.'_'.time();
				$path 			= $request->profile_picture->path();
				$extension 		= $request->profile_picture->extension();
				
				$img 			= Image::make($path)
			    					->fit(400, 400);
				
				Storage::disk('google')->put($filename.'.'.$extension, $img->encode($extension));
				
				$listContents 	= Storage::disk('google')->listContents();
				$id 			= ApiProductsController::getId($listContents, 'filename', $filename);
				$fileID 		= $id['path'];
				
			}
			
			if(isset($fileID)){
				
				Storage::disk('google')->delete($user->profile_picture);
				$user->profile_picture = $fileID;
				
			}
			
			$user->save(); // Save changes
			
			DB::commit();
			
			return response()->json(array('status' => true, 'message' => trans('resisten.Data updated succesfully'), 
										  'profile_picture' => $user->profile_picture
										  )
									);
			
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return response()->json(array('status' => false, 'message' => $e -> getMessage()));
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	/**
     * User's login request
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
    	
		try {
			
			DB::beginTransaction();
			
			// Make validation rules
			$validator = Validator::make($request->all(), [
	            'email' => 'required|email|max:255',
	            'password' => 'required|min:6',
	        ]);
	        
			$errorMessage = $validator->errors();
			$message = '';
			foreach ($errorMessage->all() as $value) {
				$message .= $value.PHP_EOL;
			}
			
			// Run Validation
			if ($validator->fails()) throw new Exception($message);
	        
			// Trying to get user data
	        $user = User::where('email','=',$request['email'])->first();
			
			// Check user existence
			if(is_null($user)) throw new Exception(trans('resisten.User Not Found'));
			
			// Check the user's password is matched
			if(!Hash::check($request['password'], $user->password)) throw new Exception(trans('resisten.User Not Found'));
			
			// Regenerate API token
			$user->api_token = sha1(md5(time()));
			
			// Save new token
			$user->save();
			
			$loginData = ['id'				=> $user->id, 
						  'name' 			=> $user->name, 
						  'store_name'		=> $user->store_name, 
						  'email' 			=> $user->email,
						  'phone' 			=> $user->phone,
						  'api_token' 		=> $user->api_token, 
						  'profile_picture'	=> $user->profile_picture,
						  'province_id'		=> $user->province_id,
						  'province_name'	=> $user->province_name,
						  'city_id'			=> $user->city_id,
						  'city_name'		=> $user->city_name,
						  'subdistrict_id'	=> $user->subdistrict_id,
						  'subdistrict_name'=> $user->subdistrict_name,
						  'address'			=> $user->address,
						  'type' 			=> $user->type
						 ];
			
			DB::commit();
			return response()->json(array('status' => true, 'message' => $loginData));
			
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return response()->json(array('status' => false, 'message' => $e -> getMessage()));
		}
		
    }

	/**
     * Load user's target for this month
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function loadTarget(Request $request)
    {
    	
		try {
			
			$user 		= User::where('api_token', '=', $request->api_token)->first();
			
			$targets	= $user->targets()->where('month', '=', Carbon::now()->month)
										  ->where('year', '=', Carbon::now()->year)
										  ->first();
			
			return response()->json(array('status' => true, 'message' => $targets));
			
		} catch (Exception $e) {
			Log::error($e);

			return response()->json(array('status' => false, 'message' => $e -> getMessage()));
		}
		
    }

	/**
     * Add user's banks
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addBank(Request $request)
    {
    	
		try {
			
			// Make validation rules
			$validator = Validator::make($request->all(), [
	            'api_token' 		=> 'bail|required|exists:users,api_token',
	            'bank_name' 		=> 'required|max:191',
	            'account_number' 	=> 'required|max:191',
	            'account_name' 		=> 'required|max:191',
	        ]);
			
			$errorMessage = $validator->errors();
			$message = '';
			foreach ($errorMessage->all() as $value) {
				$message .= $value.PHP_EOL;
			}
			
			// Run Validation
			if ($validator->fails()) throw new Exception($message);
	        
			// Trying to get user data
	        $user = User::where('api_token','=',$request->api_token)->first();
			
			// Check user existence
			if(is_null($user)) throw new Exception(trans('resisten.User Not Found'));
			
			DB::beginTransaction();
			
			// Check if there is same bank account
			$bank = Banks::where('user_id','=',$user->id)->where('bank_name','=',$request->bank_name)
						 ->where('account_number','=',$request->account_number)
						 ->where('account_name','=',$request->account_name)
						 ->first();
			
			if(!is_null($bank)) throw new Exception(trans('resisten.Bank already exists'));
			
			// Insert new one if the bank not exists
			$bank = Banks::create($request->all());
			$bank->user_id = $user->id;
			$bank->save();
			
			DB::commit();
			
			return response()->json(array('status' => true, 'message' => trans('resisten.The bank added to your account')));
			
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return response()->json(array('status' => false, 'message' => $e -> getMessage()));
		}
		
    }

	/**
     * Get user's banks
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getBank(Request $request)
    {
    	
		try {
	        
			$user 	= User::where('api_token', '=', $request->api_token)->first();
			
			$banks	= $user->banks()->paginate(10); 
			
			return response()->json(array('status' => true, 'message' => $banks));
			
		} catch (Exception $e) {
			Log::error($e);

			return response()->json(array('status' => false, 'message' => $e -> getMessage()));
		}
		
    }

	/**
     * Get seller's banks
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getSellerBank(Request $request)
    {
    	
		try {
	        
			$user 	= User::where('api_token', '=', $request->api_token)->first();
			
			$seller	= User::findOrFail($request->seller_id);
			
			$banks	= $seller->banks()->paginate(10); 
			
			return response()->json(array('status' => true, 'message' => $banks));
			
		} catch (Exception $e) {
			Log::error($e);

			return response()->json(array('status' => false, 'message' => $e -> getMessage()));
		}
		
    }

	/**
     * Delete user's banks
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteBank(Request $request)
    {
    	
		try {
			
			// Make validation rules
			$validator = Validator::make($request->all(), [
	            'api_token' => 'bail|required|exists:users,api_token',
	            'bank_id' 	=> 'required|exists:banks,id',
	        ]);
			
			$errorMessage = $validator->errors();
			$message = '';
			foreach ($errorMessage->all() as $value) {
				$message .= $value.PHP_EOL;
			}
			
			// Run Validation
			if ($validator->fails()) throw new Exception($message);
	        
			// Trying to get user data
	        $user = User::where('api_token','=',$request->api_token)->first();
			
			// Check user existence
			if(is_null($user)) throw new Exception(trans('resisten.User Not Found'));
			
			DB::beginTransaction();
			
			// Check the bank and delete it
			$bank = Banks::where('user_id','=',$user->id)->where('id','=',$request->bank_id)->first();
			$bank->delete();
			
			DB::commit();
			
			return response()->json(array('status' => true, 'message' => trans('resisten.The bank deleted successfully')));
			
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return response()->json(array('status' => false, 'message' => $e -> getMessage()));
		}
		
    }

}
