<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Controllers\RelationsController;

use DB, Exception, Log, Validator;

use App\User;
use App\Relations;

class ApiRelationsController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request, $id) {
		try {
			
			$viewer		= User::where('api_token', '=', $request->api_token)->first();
			$user		= User::findOrFail($id);
			$products	= $user->products()->where('show_to', '=', $viewer->type)->paginate(8);
			$orders 	= $user->orders_out()->with('seller', 'buyer', 'product', 'courier')
											 ->orderBy('created_at', 'desc')
											 ->paginate(10);
        	
			if($user->type == 1){
				return response()->json(array('status' => true, 'message' => ['reseller'=>$user, 'orders'=>$orders]));
			} elseif ($user->type == 2){
				return response()->json(array('status' => true, 'message' => ['supplier'=>$user, 'products'=>$products]));
			}
			
        } catch (Exception $e) {
			Log::error($e);

			return response()->json(array('status' => false, 'message' => $e -> getMessage()));
		}
	}
	
	/**
	 * Get Cooperation request
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function getCoopRequest(Request $request)
	{
		
		try {
			
			$user		= User::where('api_token','=',$request->api_token)->first();
			
			if(!$user) throw new Exception(trans('resisten.User Not Found'));
			
			$coopReqs	= Relations::where('parent', '=', $user->id)->where('status', '=', '1')->paginate(10);
			$result		= [];
			foreach ($coopReqs as $requester) {
				array_push($result, $requester->requestant_user);
			}
        	
			return response()->json(array('status' => true, 'message' => $result));
			
        } catch (Exception $e) {
			Log::error($e);

			return response()->json(array('status' => false, 'message' => $e -> getMessage()));
		}
		
	}

	/**
	 * Display the list of supplier that related with user.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function supplierList(Request $request) {
		try {
			
			$user		= User::where('api_token','=',$request->api_token)->first();
			
			if(!$user) throw new Exception(trans('resisten.User Not Found'));
			
			$suppliers	= User::whereExists(function ($query) use ($user) {
									$query->select(DB::raw(1))
					                      ->from('user_relations')
					                      ->whereRaw('user_relations.parent = '.$user->id.' AND user_relations.child = users.id');
								})
								->where('type', '=', 2)
								->join('user_relations', function($join) use ($user) {
									$join->on('user_relations.child', '=', 'users.id')
										 ->where('user_relations.parent', '=', $user->id);
								})
								->select('users.id', 
										 'users.name', 
										 'users.store_name', 
										 'users.email', 
										 'users.phone', 
										 'users.profile_picture', 
										 'user_relations.status as relation_status')
								->paginate(10);
        	
			return response()->json(array('status' => true, 'message' => $suppliers));
			
        } catch (Exception $e) {
			Log::error($e);

			return response()->json(array('status' => false, 'message' => $e -> getMessage()));
		}
	}

	/**
	 * Display the list of reseller that related with user.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function resellerList(Request $request) {
		try {
			
			$user		= User::where('api_token','=',$request->api_token)->first();
			
			if(!$user) throw new Exception(trans('resisten.User Not Found'));
			
			$suppliers	= User::whereExists(function ($query) use ($user) {
									$query->select(DB::raw(1))
					                      ->from('user_relations')
					                      ->whereRaw('user_relations.parent = '.$user->id.' AND user_relations.child = users.id');
								})
								->where('type', '=', 1)
								->join('user_relations', function($join) use ($user) {
									$join->on('user_relations.child', '=', 'users.id')
										 ->where('user_relations.parent', '=', $user->id);
								})
								->select('users.id', 
										 'users.name', 
										 'users.store_name', 
										 'users.email', 
										 'users.phone', 
										 'users.profile_picture', 
										 'user_relations.status as relation_status')
								->paginate(10);
        	
			return response()->json(array('status' => true, 'message' => $suppliers));
			
        } catch (Exception $e) {
			Log::error($e);

			return response()->json(array('status' => false, 'message' => $e -> getMessage()));
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	/**
	 * Store user relations between supplier and reseller.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function add(Request $request) {
		
		try {
			
			// Make validation rules
			$validator = Validator::make($request->all(), [
	            'api_token' 	=> 'bail|required|exists:users,api_token',
	            'supplier'		=> 'required|numeric|exists:users,id'
	        ]);
			
			$errorMessage = $validator->errors();
			$message = '';
			foreach ($errorMessage->all() as $value) {
				$message .= $value.PHP_EOL;
			}
			
			// Run Validation
			if ($validator->fails()) throw new Exception($message);
			
			DB::beginTransaction();
        	
			$user = User::where('api_token','=',$request->api_token)->first();
			
			if(!$user) throw new Exception(trans('resisten.User Not Found'));
	        
			// Run insert data process
	        if(Relations::where('parent', '=', $user->id)->where('child', '=', $request->supplier)->count() == 0){
	        	
				Relations::create([	'parent'	=>$user->id, 
									'child'		=>$request->supplier, 
									'requestant'=>$user->id, 
									'request_to'=>$request->supplier
								  ]);
								  
				Relations::create([	'parent'	=>$request->supplier, 
									'child'		=>$user->id, 
									'requestant'=>$user->id, 
									'request_to'=>$request->supplier
								  ]);
				
	        }
			
			DB::commit();
			
			return response()->json(array('status' => true, 'message' => trans('resisten.Supplier Added Successfully')));
			
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return response()->json(array('status' => false, 'message' => $e -> getMessage()));
		}
		
	}

	/**
	 * Accept request of user relation.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function accept(Request $request) {
		
		try {
			
			// Make validation rules
			$validator = Validator::make($request->all(), [
	            'api_token' => 'bail|required|exists:users,api_token',
	            'child'		=> 'required|numeric|exists:users,id'
	        ]);
			
			$errorMessage = $validator->errors();
			$message = '';
			foreach ($errorMessage->all() as $value) {
				$message .= $value.PHP_EOL;
			}
			
			// Run Validation
			if ($validator->fails()) throw new Exception($message);
			
			DB::beginTransaction();
        	
			$user = User::where('api_token','=',$request->api_token)->first();
			$child= User::findOrFail($request->child);
			
			if(!$user) throw new Exception(trans('resisten.User Not Found'));
	        
			// Run update data process
	        Relations::where('parent', '=', $user->id)->where('child', '=', $request->child)->update(['status'=> '2']);
	        Relations::where('parent', '=', $request->child)->where('child', '=', $user->id)->update(['status'=> '2']);
			
			DB::commit();
			
			return response()->json(array('status' => true, 'message' => trans('resisten.Request accepted'), 
										  'child' => $child->type));
			
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return response()->json(array('status' => false, 'message' => $e -> getMessage()));
		}
		
	}

	/**
	 * Reject request of user relation.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function reject(Request $request) {
		
		try {
			
			// Make validation rules
			$validator = Validator::make($request->all(), [
	            'api_token' => 'bail|required|exists:users,api_token',
	            'child'		=> 'required|numeric|exists:users,id'
	        ]);
			
			$errorMessage = $validator->errors();
			$message = '';
			foreach ($errorMessage->all() as $value) {
				$message .= $value.PHP_EOL;
			}
			
			// Run Validation
			if ($validator->fails()) throw new Exception($message);
			
			DB::beginTransaction();
        	
			$user = User::where('api_token','=',$request->api_token)->first();
			
			if(!$user) throw new Exception(trans('resisten.User Not Found'));
	        
			// Run update data process
	        Relations::where('parent', '=', $user->id)->where('child', '=', $request->child)
	        		 ->where('status', '=', '1')->delete();
	        Relations::where('parent', '=', $request->child)->where('child', '=', $user->id)
	        		 ->where('status', '=', '1')->delete();
			
			DB::commit();
			
			return response()->json(array('status' => true, 'message' => trans('resisten.Request rejected')));
			
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return response()->json(array('status' => false, 'message' => $e -> getMessage()));
		}
		
	}

	/**
	 * Get the suppliers list from search param.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function search(Request $request) {
		
		try {
			
			if(!$request->has('s')) throw new Exception('Please insert name, store name or phone!');
			$user		= User::where('api_token','=',$request->api_token)->first();
			
			if(!$user) throw new Exception(trans('resisten.User Not Found'));
			
			$suppliers	= User::whereNotExists(function ($query) use ($user, $request) {
									$query->select(DB::raw(1))
					                      ->from('user_relations')
					                      ->whereRaw('user_relations.parent = '.$user->id.' AND user_relations.child = users.id');
								})
								->where('type', '=', 2)
								->whereRaw(RelationsController::getSupplierSearchParam($request->s, $user->id))
								->select('id', 'name', 'store_name', 'profile_picture')
								->get();
        	
			return response()->json(array('status' => true, 'message' => $suppliers));
			
        } catch (Exception $e) {
			Log::error($e);

			return response()->json(array('status' => false, 'message' => $e -> getMessage()));
		}
		
	}

}
