<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use Carbon\Carbon;
use DB, Exception, finfo, Image, Log, Storage, Validator;

use App\Http\Controllers\Api\ApiProductsController;

use App\User;
use App\Orders;
use App\Products;
use App\Relations;

class ApiOrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
        	
			if(!$request->has('need')) throw new Exception("Error Processing Request");
			
			$user = User::where('api_token','=',$request->api_token)->first();
			
			$orders = null;
			if($request->need == 'in') {
				
				$orders = $user->orders_in()->whereNotNull('payment_image')
											->whereNotNull('shipping_code')
											->with('seller', 'buyer', 'product', 'courier')
											->orderBy('created_at', 'desc')
											->paginate(10);
											
			} elseif($request->need == 'in_new') {
				
				$orders = $user->orders_in()->whereNotNull('payment_image')
											->whereNull('shipping_code')
											->where('shipping_status', '=', 'NEW')
											->with('seller', 'buyer', 'product', 'courier')
											->orderBy('created_at')
											->paginate(10);
											
			}elseif($request->need == 'in_packed') {
				
				$orders = $user->orders_in()->whereNotNull('payment_image')
											->whereNull('shipping_code')
											->where('shipping_status', '=', 'PACKED')
											->with('seller', 'buyer', 'product', 'courier')
											->orderBy('created_at', 'desc')
											->paginate(10);
											
			} elseif ($request->need == 'all_in') {
				
				$orders = $user->orders_in()->with('seller', 'buyer', 'product', 'courier')
											->orderBy('created_at', 'desc')
											->paginate(10);
			
			} elseif ($request->need == 'all_out') {
				
				$orders = $user->orders_out()->with('seller', 'buyer', 'product', 'courier')
											 ->orderBy('created_at', 'desc')
											 ->paginate(10);
			
			} elseif ($request->need == 'out') {
				
				$orders = $user->orders_out()->whereNotNull('payment_image')
											 ->whereNotNull('shipping_code')
											 ->with('seller', 'buyer', 'product', 'courier')
											 ->orderBy('created_at', 'desc')
											 ->paginate(10);
			
			} elseif ($request->need == 'payment') {
			
				$orders = $user->orders_out()->whereNull('payment_image')
											 ->with('seller', 'buyer', 'product', 'courier')
											 ->orderBy('created_at', 'desc')
											 ->paginate(10);
			
			}
			
			return response()->json(array('status' => true, 'message' => $orders));
        	
		} catch (Exception $e) {
			Log::error($e);

			return response()->json(array('status' => false, 'message' => $e -> getMessage()));
		}
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
			
			// Make validation rules
			$validator = Validator::make($request->all(), [
	            'api_token' 				=> 'bail|required|exists:users,api_token',
	            'product_id'				=> 'required|exists:products,id',
	            'count' 					=> 'required|numeric|min:1',
	            'total_weight' 				=> 'required|numeric|min:1',
	            'total_price' 				=> 'required|numeric|min:1',
	            'shipping_province_id' 		=> 'required|numeric|min:1',
	            'shipping_city_id' 			=> 'required|numeric|min:1',
	            'shipping_subdistrict_id' 	=> 'required|numeric|min:1',
	            'shipping_cost' 			=> 'required|numeric|min:1',
	            'shipping_name' 			=> 'required',
	            'shipping_phone' 			=> 'required',
	            'shipping_address' 			=> 'required',
	            'shipping_agents_id' 		=> 'required|exists:shipping_agents,id',
	            'shipping_service_code' 	=> 'required',
	        ]);
			
			$errorMessage = $validator->errors();
			$message = '';
			foreach ($errorMessage->all() as $value) {
				$message .= $value.PHP_EOL;
			}
			
			// Run Validation
			if ($validator->fails()) throw new Exception($message);
			
			DB::beginTransaction();
        	
			$user = User::where('api_token','=',$request->api_token)->first();
			if(!$user) throw new Exception(trans('resisten.User Not Found'));
			
			// Run input process
			$product = Products::findOrFail($request->product_id);
			if($product->stock < $request->count) throw new Exception(trans('resisten.Sorry, this product stock is not enough!'));
			
			$relations = Relations::where('parent', '=', $user->id)->where('child', '=', $product->user_id)
									->where('status', '=', 2)->first();
			
			if($user->type == 1){
				
				if($product->show_to == 2 || !$relations) throw new Exception(trans('resisten.You are not allowed to order this product!'));
				
			} elseif($user->type == 2){
				
				if($product->show_to == 1 || !$relations) throw new Exception(trans('resisten.You are not allowed to order this product!'));
				
			}

			$order = Orders::create($request->all());
			$order->seller_id 			= $product->supplier->id;
			$order->buyer_id 			= $user->id;
			$order->total_commisions	= $product->commision * $request->count;
			$order->save();
			$product->stock 	-= $request->count;
			$product->save();
			
			DB::commit();
			
			return response()->json(array('status' => true, 
										  'message' => trans('resisten.Your Order Submitted Successfully'), 
										  'order_id' => $order->id, 
										  'seller_id' => $order->seller_id,
										  )
									);
			
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return response()->json(array('status' => false, 'message' => $e -> getMessage()));
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Store payment image for payment confirmation.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function payment(Request $request)
	 {
	 	try {
	 		
			// Make validation rules
			$validator = Validator::make($request->all(), [
	            'api_token'	=> 'bail|required|exists:users,api_token',
	            'order_id'	=> 'required|exists:orders,id',
	            'image'		=> 'required|image',
	        ]);
			
			$errorMessage = $validator->errors();
			$message = '';
			foreach ($errorMessage->all() as $value) {
				$message .= $value.PHP_EOL;
			}
			
			// Run Validation
			if ($validator->fails()) throw new Exception($message);
			
			DB::beginTransaction();
        	
			$user = User::where('api_token','=',$request->api_token)->first();
			if(!$user) throw new Exception(trans('resisten.User Not Found'));
			
			$order = Orders::findOrFail($request->order_id);
			if(!$order) throw new Exception(trans('resisten.Order Not Found'));
			
			if($user->id <> $order->buyer_id) throw new Exception(trans('resisten.You are not allowed to do this action'));
			
			
			// Check the request has file uploaded
			if ($request->hasFile('image') && $request->file('image')->isValid()) {
			    	
			    $filename 		= sha1(md5(microtime())).'_'.$user->id.'_'.time();
				$path 			= $request->image->path();
				$extension 		= $request->image->extension();
				
				$img 			= Image::make($path)
								  ->resize(600, null, function ($constraint) {
								      $constraint->aspectRatio();
								  });
				
				Storage::disk('google')->put($filename.'.'.$extension, $img->encode($extension));
				
				$listContents 	= Storage::disk('google')->listContents();
				$id 			= ApiProductsController::getId($listContents, 'filename', $filename);
				$fileID 		= $id['path'];
				
			}
			
			if(isset($fileID)){
				Storage::disk('google')->delete($order->payment_image);
				$order->payment_image = $fileID;
			}
			
			$order->save(); // save changes
			
			DB::commit();
			
			return response()->json(array('status' => true, 'message' => trans('resisten.Payment Confirmation Image Inserted Successfully')));
			
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return response()->json(array('status' => false, 'message' => $e -> getMessage()));
		}
	 }

	/**
     * Change order status to packed
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function packed(Request $request)
	{
		
		try {
	 		
			// Make validation rules
			$validator = Validator::make($request->all(), [
	            'api_token'	=> 'bail|required|exists:users,api_token',
	            'order_id'	=> 'required|exists:orders,id',
	        ]);
			
			$errorMessage = $validator->errors();
			$message = '';
			foreach ($errorMessage->all() as $value) {
				$message .= $value.PHP_EOL;
			}
			
			// Run Validation
			if ($validator->fails()) throw new Exception($message);
			
			DB::beginTransaction();
        	
			$user = User::where('api_token','=',$request->api_token)->first();
			if(!$user) throw new Exception(trans('resisten.User Not Found'));
			
			$order = Orders::findOrFail($request->order_id);
			if(!$order) throw new Exception(trans('resisten.Order Not Found'));
			$order->shipping_status = 'PACKED';
			$order->save();
			
			DB::commit();
			
			return response()->json(array('status' => true, 'message' => trans('resisten.Order processed')));
			
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return response()->json(array('status' => false, 'message' => $e -> getMessage()));
		}
		
	}

	/**
     * Insert shipping code and change order status
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function shippingCode(Request $request)
	{
		
		try {
	 		
			// Make validation rules
			$validator = Validator::make($request->all(), [
	            'api_token'		=> 'bail|required|exists:users,api_token',
	            'order_id'		=> 'required|exists:orders,id',
	            'shipping_code'	=> 'required',
	        ]);
			
			$errorMessage = $validator->errors();
			$message = '';
			foreach ($errorMessage->all() as $value) {
				$message .= $value.PHP_EOL;
			}
			
			// Run Validation
			if ($validator->fails()) throw new Exception($message);
			
			DB::beginTransaction();
        	
			$user = User::where('api_token','=',$request->api_token)->first();
			if(!$user) throw new Exception(trans('resisten.User Not Found'));
			
			$order = Orders::findOrFail($request->order_id);
			if(!$order) throw new Exception(trans('resisten.Order Not Found'));
			$order->shipping_date	= Carbon::now()->toDateString();
			$order->shipping_code 	= $request->shipping_code;
			$order->shipping_status = 'ON PROCESS';
			$order->save();
			
			DB::commit();
			
			return response()->json(array('status' => true, 'message' => trans('resisten.Shipping code submitted')));
			
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return response()->json(array('status' => false, 'message' => $e -> getMessage()));
		}
		
	}

	/**
     * Update order shipping status
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateShippingStatus(Request $request)
	{
		
		try {
	 		
			// Make validation rules
			$validator = Validator::make($request->all(), [
	            'api_token'		=> 'bail|required|exists:users,api_token',
	            'order_id'		=> 'required|exists:orders,id',
	        ]);
			
			$errorMessage = $validator->errors();
			$message = '';
			foreach ($errorMessage->all() as $value) {
				$message .= $value.PHP_EOL;
			}
			
			// Run Validation
			if ($validator->fails()) throw new Exception($message);
			
			DB::beginTransaction();
        	
			$user = User::where('api_token','=',$request->api_token)->first();
			if(!$user) throw new Exception(trans('resisten.User Not Found'));
			
			$order = Orders::findOrFail($request->order_id);
			if(!$order) throw new Exception(trans('resisten.Order Not Found'));
			if(!$order->shipping_code) throw new Exception(trans('resisten.Shipping code not found'));
			
			// Check status from rajaongkir
			$post_data = [
						 	'key'		=>'b61074ec862c34d910e3e11c0b38c29b',
							'waybill'	=>$order->shipping_code,
						 	'courier'	=>$order->courier->code,
						 ];
			
			$check_status = self::simple_post_curl("http://pro.rajaongkir.com/api/waybill", $post_data);
			if(!$check_status) throw new Exception(trans('resisten.No response from shipment provider'));
			
			$response	= json_decode($check_status);
			if(!isset($response->rajaongkir->result)) throw new Exception($response->rajaongkir->status->description);
			
			$status		= $response->rajaongkir->result->summary->status;
			if(!$status) throw new Exception($response->rajaongkir->status->description);
			
			$order->shipping_status = $status;
			$order->save();
			
			DB::commit();
			
			return response()->json(array('status' => true, 'message' => trans('resisten.Shipping status updated'), 'shipping_status'=>$status));
			
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return response()->json(array('status' => false, 'message' => $e -> getMessage()));
		}
		
	}

	public static function simple_post_curl($url, $data)
   	{
   		
   		$userAgentList = array('Mozilla/5.0 (compatible; MSIE 9.0; AOL 9.7; AOLBuild 4343.19; Windows NT 6.1; WOW64; Trident/5.0; FunWebProducts)', 'Mozilla/5.0 (Macintosh; U; PPC Mac OS X; pl-PL; rv:1.0.1) Gecko/20021111 Chimera/0.6', 'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/535.7 (KHTML, like Gecko) Comodo_Dragon/16.1.1.0 Chrome/16.0.912.63 Safari/535.7', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/4.0; Deepnet Explorer 1.5.3; Smart 2x2; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)', 'Mozilla/4.0 (compatible; MSIE 5.23; Macintosh; PPC) Escape 5.1.8', 'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0', 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_1; nl-nl) AppleWebKit/532.3+ (KHTML, like Gecko) Fluid/0.9.6 Safari/532.3+', 'Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; XH; rv:8.578.498) fr, Gecko/20121021 Camino/8.723+ (Firefox compatible)', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36', 'Mozilla/5.0 (Future Star Technologies Corp.; Star-Blade OS; x86_64; U; en-US) iNet Browser 4.7', 'Mozilla/5.0 (compatible, MSIE 11, Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1250.0 Iron/22.0.2150.0 Safari/537.4', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.4pre) Gecko/20070404 K-Ninja/2.1.3', 'Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.0.7) Gecko Kazehakase/0.5.6', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows XP 5.1) Lobo/0.98.4', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB; rv:1.9.1.17) Gecko/20110123 Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.2) Gecko/20070225 lolifox/0.32', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.28) Gecko/20120410 Firefox/3.6.28 Lunascape/6.7.1.25446', 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/533.1 (KHTML, like Gecko) Maxthon/3.0.8.2 Safari/533.1', 'Mozilla/5.0 (X11; U; Linux i686; fr-fr) AppleWebKit/525.1+ (KHTML, like Gecko, Safari/525.1+) midori/1.19', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; rv:2.2) Gecko/20110201', 'Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.2a2pre) Gecko/20090908 Ubuntu/9.04 (jaunty) Namoroka/3.6a2pre GTB5 (.NET CLR 3.5.30729)', 'Mozilla/5.0 (Windows; U; Win 9x 4.90; SG; rv:1.9.2.4) Gecko/20101104 Netscape/9.1.0285', 'Opera/9.80 (X11; Linux i686; Ubuntu/14.10) Presto/2.12.388 Version/12.16', '(Windows NT 6.2; WOW64) KHTML/4.11 Gecko/20130308 Firefox/33.0 (PaleMoon/25.1)', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A', 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_6; en-us) AppleWebKit/528.16 (KHTML, like Gecko) Stainless/0.5.3 Safari/525.20.1', 'Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30', 'Mozilla/5.0 (BlackBerry; U; BlackBerry 9900; en) AppleWebKit/534.11+ (KHTML, like Gecko) Version/7.1.0.346 Mobile Safari/534.11+', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows Phone OS 7.5; Trident/5.0; IEMobile/9.0)', 'Opera/9.80 (J2ME/MIDP; Opera Mini/9.80 (S60; SymbOS; Opera Mobi/23.348; U; en) Presto/2.5.25 Version/10.54', 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_7; en-us) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Safari/530.17 Skyfire/2.0', 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.2; WOW64; Trident/4.0; uZardWeb/1.0; Server_USA)');
                        
   		$ch = curl_init();
       	curl_setopt($ch, CURLOPT_URL, $url);
      	curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
     	curl_setopt($ch, CURLOPT_USERAGENT, array_rand($userAgentList));
       	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
       	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
       	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        
       	$curl_result = curl_exec($ch);
       	curl_close($ch);
                
       	return $curl_result;
                
	}

	public static function simple_get_curl($url, $data)
   	{
   		
   		$userAgentList = array('Mozilla/5.0 (compatible; MSIE 9.0; AOL 9.7; AOLBuild 4343.19; Windows NT 6.1; WOW64; Trident/5.0; FunWebProducts)', 'Mozilla/5.0 (Macintosh; U; PPC Mac OS X; pl-PL; rv:1.0.1) Gecko/20021111 Chimera/0.6', 'Mozilla/5.0 (Windows NT 6.2) AppleWebKit/535.7 (KHTML, like Gecko) Comodo_Dragon/16.1.1.0 Chrome/16.0.912.63 Safari/535.7', 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/4.0; Deepnet Explorer 1.5.3; Smart 2x2; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; .NET4.0E)', 'Mozilla/4.0 (compatible; MSIE 5.23; Macintosh; PPC) Escape 5.1.8', 'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0', 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_1; nl-nl) AppleWebKit/532.3+ (KHTML, like Gecko) Fluid/0.9.6 Safari/532.3+', 'Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; XH; rv:8.578.498) fr, Gecko/20121021 Camino/8.723+ (Firefox compatible)', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36', 'Mozilla/5.0 (Future Star Technologies Corp.; Star-Blade OS; x86_64; U; en-US) iNet Browser 4.7', 'Mozilla/5.0 (compatible, MSIE 11, Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1250.0 Iron/22.0.2150.0 Safari/537.4', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.4pre) Gecko/20070404 K-Ninja/2.1.3', 'Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.0.7) Gecko Kazehakase/0.5.6', 'Mozilla/4.0 (compatible; MSIE 6.0; Windows XP 5.1) Lobo/0.98.4', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB; rv:1.9.1.17) Gecko/20110123 Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.2) Gecko/20070225 lolifox/0.32', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.28) Gecko/20120410 Firefox/3.6.28 Lunascape/6.7.1.25446', 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/533.1 (KHTML, like Gecko) Maxthon/3.0.8.2 Safari/533.1', 'Mozilla/5.0 (X11; U; Linux i686; fr-fr) AppleWebKit/525.1+ (KHTML, like Gecko, Safari/525.1+) midori/1.19', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; rv:2.2) Gecko/20110201', 'Mozilla/5.0 (X11; U; Linux x86_64; en-US; rv:1.9.2a2pre) Gecko/20090908 Ubuntu/9.04 (jaunty) Namoroka/3.6a2pre GTB5 (.NET CLR 3.5.30729)', 'Mozilla/5.0 (Windows; U; Win 9x 4.90; SG; rv:1.9.2.4) Gecko/20101104 Netscape/9.1.0285', 'Opera/9.80 (X11; Linux i686; Ubuntu/14.10) Presto/2.12.388 Version/12.16', '(Windows NT 6.2; WOW64) KHTML/4.11 Gecko/20130308 Firefox/33.0 (PaleMoon/25.1)', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.75.14 (KHTML, like Gecko) Version/7.0.3 Safari/7046A194A', 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_6; en-us) AppleWebKit/528.16 (KHTML, like Gecko) Stainless/0.5.3 Safari/525.20.1', 'Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30', 'Mozilla/5.0 (BlackBerry; U; BlackBerry 9900; en) AppleWebKit/534.11+ (KHTML, like Gecko) Version/7.1.0.346 Mobile Safari/534.11+', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows Phone OS 7.5; Trident/5.0; IEMobile/9.0)', 'Opera/9.80 (J2ME/MIDP; Opera Mini/9.80 (S60; SymbOS; Opera Mobi/23.348; U; en) Presto/2.5.25 Version/10.54', 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_7; en-us) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Safari/530.17 Skyfire/2.0', 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.2; WOW64; Trident/4.0; uZardWeb/1.0; Server_USA)');
                        
   		$ch = curl_init();
       	curl_setopt($ch, CURLOPT_URL, $url.'?'.http_build_query($data));
     	curl_setopt($ch, CURLOPT_USERAGENT, array_rand($userAgentList));
       	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
       	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
       	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        
       	$curl_result = curl_exec($ch);
       	curl_close($ch);
                
       	return $curl_result;
                
	}
	
}
