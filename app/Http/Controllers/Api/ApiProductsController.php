<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use DB, Exception, finfo, Image, Log, Storage, Validator;

use App\User;
use App\Products;
use App\Relations;

class ApiProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	
        try {
        	
			$user 		= User::where('api_token', '=', $request->api_token)->first();
			$relation	= Relations::select('child as id')->where('parent', '=', $user->id)->where('status','=','2')->get()->toArray();
			
			if($user->type == 1){
				$products	= Products::whereIn('user_id', $relation)
										-> where(function ($query) use ($user) {
											$query -> where('show_to', '=', '3') 
												   -> orWhere('show_to', '=', $user->type);
										})
										-> join('users', function($join){
											$join->on('users.id', '=', 'products.user_id');
										})
										-> select('users.name as supplier_name', 'users.profile_picture',
										 		  'users.province_id', 'users.city_id', 
										 		  'users.subdistrict_id', 'products.*'
												  )
										-> orderBy('created_at','desc')
										-> paginate(10);
				
			} elseif ($user->type == 2) {
				array_push($relation, $user->id);
				$products 	= Products::whereIn('user_id', $relation)
										-> where(function ($query) use ($user) {
											$query -> where('show_to', '=', '3') 
												   -> orWhere('show_to', '=', $user->type);
										})
										-> join('users', function($join){
											$join->on('users.id', '=', 'products.user_id');
										})
										-> select('users.name as supplier_name', 'users.profile_picture',
										 		  'users.province_id', 'users.city_id', 
										 		  'users.subdistrict_id', 'products.*'
												  )
										-> orderBy('created_at','desc')
										-> paginate(10);
			}
			
			return response()->json(array('status' => true, 'message' => $products));
			
        } catch (Exception $e) {
			Log::error($e);

			return response()->json(array('status' => false, 'message' => $e -> getMessage()));
		}
		
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
			
			// Make validation rules
			$validator = Validator::make($request->all(), [
	            'api_token' 	=> 'bail|required|exists:users,api_token',
	            'name' 			=> 'required|max:191',
	            'stock' 		=> 'required|numeric',
	            'price' 		=> 'required|numeric',
	            'commision' 	=> 'required|numeric',
	            'weight' 		=> 'required|numeric',
	            'description' 	=> 'required',
	            'image' 		=> 'required|image',
	            'show_to' 		=> 'required|numeric',
	        ]);
			
			$errorMessage = $validator->errors();
			$message = '';
			foreach ($errorMessage->all() as $value) {
				$message .= $value.PHP_EOL;
			}
			
			// Run Validation
			if ($validator->fails()) throw new Exception($message);
			
			DB::beginTransaction();
        	
			$user = User::where('api_token','=',$request->api_token)->first();
			
			if(!$user) throw new Exception(trans('resisten.User Not Found'));
			
			// Check the request has file uploaded
			if ($request->hasFile('image') && $request->file('image')->isValid()) {
			    	
			    $filename 		= sha1(md5(microtime())).'_'.$user->id.'_'.time();
				$path 			= $request->image->path();
				$extension 		= $request->image->extension();
				
				$img 			= Image::make($path)
			    					->fit(800, 600);
				
				Storage::disk('google')->put($filename.'.'.$extension, $img->encode($extension));
				
				$listContents 	= Storage::disk('google')->listContents();
				$id 			= self::getId($listContents, 'filename', $filename);
				$fileID 		= $id['path'];
				
			}
	        
			// Run insert data process
	        $proc = Products::create([
	        	'user_id'		=> $user->id,
	            'name' 			=> $request['name'],
	            'stock' 		=> $request['stock'],
	            'price' 		=> $request['price'],
	            'commision' 	=> $request['commision'],
	            'weight' 		=> $request['weight'],
	            'description' 	=> $request['description'],
	            'image' 		=> isset($fileID) ? $fileID : null,
	            'show_to' 		=> $request['show_to'],
	        ]);
			
			DB::commit();
			
			return response()->json(array('status' => true, 'message' => trans('resisten.Product Inserted Successfully')));
			
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return response()->json(array('status' => false, 'message' => $e -> getMessage()));
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	/**
     * Get the file ID from storage.
     */
	public static function getId(Array $array, $key, $value)
	{   
		foreach ($array as $subarray)
		{  
			if (isset($subarray[$key]) && $subarray[$key] == $value)
				return $subarray;       
		} 
	}
	
	/*
	 * Show the image from GDrive
	 */
	 public function showImage($id)
	 {
	 	
		$rawData 	= Storage::disk('google')->get($id);
		return response()->make($rawData, 200, array(
	        'Content-Type' => (new finfo(FILEINFO_MIME))->buffer($rawData)
	    ));
		
	 }
	 
	 /**
     * Show the temporary image.
     *
     * @param  int  $id
     * @return Response
     */
    public function showTemp($id, $path)
    {
    	
		$dir = 'temp/'.$id.'/';
		
        if(Storage::disk('local')->exists($dir.$path)){
        	$rawData 	= Storage::disk('local')->get($dir.$path);
			return response()->make($rawData, 200, array(
		        'Content-Type' => (new finfo(FILEINFO_MIME))->buffer($rawData)
		    ));
        }
    }
	 
}
