<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth, DB, Exception, Log, Validator;

use App\User;
use App\Relations;

class NotificationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
			
			$this->authorize('seeIndex', new Relations());
			
			$user			= Auth::user();
			
			$coopRequest	= Relations::where('parent', '=', $user->id)->where('request_to', '=', $user->id)->where('status', '=', '1')->get();
			$orders 		= $user->orders_in()->whereNotNull('payment_image')
												->whereNull('shipping_code')
												->where('shipping_status', '=', 'NEW')
												->with('seller', 'buyer', 'product', 'courier')
												->orderBy('created_at', 'asc')
												->paginate(10);
			
			return view('notifications.index')->with('coopRequest', $coopRequest)->with('orders', $orders);
			
		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	/**
	 * Get notifications count
	 * 
	 * @return int $notifications
	 */
	 public static function getNotifications()
	 {
	 	
		$user			= Auth::user();
		$notifications 	= 0;
			
		$notifications	+= Relations::where('parent', '=', $user->id)->where('request_to', '=', $user->id)->where('status', '=', '1')->count();
		$notifications	+= $user->orders_in()->whereNotNull('payment_image')->whereNull('shipping_code')
											 ->where('shipping_status', '=', 'NEW')->count();
		
		return $notifications;
		
	 }
	
}
