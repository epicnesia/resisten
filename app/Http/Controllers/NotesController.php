<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use Auth, DB, Exception, Log, Validator;

use App\Notes;
use App\User;

class NotesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
        	
			$this->authorize('view', new Notes());
			
			$user 		= Auth::user();
			
        	return view('notes.index')->with('notes', $user->notes()->paginate(20));
			
		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
        	
			$this->authorize('create', new Notes());
			
        	return view('notes.create');
			
		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
        	
			$this->authorize('create', new Notes());
			
			$this->validate($request, [
				'user_id' 	=> 'required|exists:users,id',
				'title'		=> 'required',
				'content'	=> 'required',
			]);
			
			DB::beginTransaction();
	        
			// Run insert data process
	        Notes::create($request->all());
	        
	        DB::commit();

            return redirect()->action('NotesController@index')->with('success', trans('resisten.Notes saved'));
			
		} catch (AuthorizationException $e) {
			DB::rollback();
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
        	DB::rollback();
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
        	
			$this->authorize('update', Notes::findOrFail($id));
			
			$note = Notes::findOrFail($id);
			
        	return view('notes.edit')->with('note', $note);
			
		} catch (AuthorizationException $e) {
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
        	
			$this->authorize('update', Notes::findOrFail($id));
			
			$this->validate($request, [
				'user_id' 	=> 'required|exists:users,id',
				'title'		=> 'required',
				'content'	=> 'required',
			]);
			
			DB::beginTransaction();
	        
			// Run insert data process
	        $note = Notes::findOrFail($id);
			$note->user_id 	= $request->user_id;
			$note->title	= $request->title;
			$note->content	= $request->content;
			$note->save();
	        
	        DB::commit();

            return redirect()->action('NotesController@index')->with('success', trans('resisten.Notes saved'));
			
		} catch (AuthorizationException $e) {
			DB::rollback();
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
        	DB::rollback();
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
        	
			$this->authorize('delete', Notes::findOrFail($id));
			
			DB::beginTransaction();
	        
			// Run insert data process
	        $note = Notes::findOrFail($id);
			$note->delete();
	        
	        DB::commit();

            return redirect()->action('NotesController@index')->with('success', trans('resisten.Notes deleted'));
			
		} catch (AuthorizationException $e) {
			DB::rollback();
            Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
        	DB::rollback();
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getMessage());
        }
    }
}
