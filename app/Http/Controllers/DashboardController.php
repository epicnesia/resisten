<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;
use Auth, DB, Session, Log;

use App\User;
use App\Orders;
use App\Products;
use App\Relations;
use App\Targets;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
			try {

						$user	= Auth::user();
						$now	= Carbon::now();

						$data	= [];
						if($user->hasRole('admin')){
							$data['total_users']             = User::count();
							$data['total_products']          = Products::count();
							$data['total_orders']            = Orders::count();
							$data['total_sales']             = Orders::sum('count');
							$data['total_new_orders']        = Orders::where('shipping_status', '=', 'NEW')->count();
							$data['total_packed_orders']     = Orders::where('shipping_status', '=', 'PACKED')->count();
							$data['total_on_process_orders'] = Orders::where('shipping_status', '=', 'ON PROCESS')->count();
							$data['total_delivered_orders']	 = Orders::where('shipping_status', '=', 'DELIVERED')->count();

							$data['latest_users']            = User::latest()->limit(8)->get();
							$data['latest_products']         = Products::latest()->limit(4)->get();
							$data['latest_orders']           = Orders::latest()->limit(10)->get();

						} elseif($user->hasRole('supplier')){
							$data['total_orders_out']        = $user->orders_out()->count();
							$data['total_orders_in']         = $user->orders_in()->count();
							$data['total_sales']             = $user->orders_in()->sum('count');
							$data['total_omzet']             = $user->orders_in()->sum('total_price');
							$data['total_new_orders']        = $user->orders_in()->where('shipping_status', '=', 'NEW')->count();
							$data['total_packed_orders']     = $user->orders_in()->where('shipping_status', '=', 'PACKED')->count();
							$data['total_on_process_orders'] = $user->orders_in()->where('shipping_status', '=', 'ON PROCESS')->count();
							$data['total_delivered_orders']	 = $user->orders_in()->where('shipping_status', '=', 'DELIVERED')->count();
							$data['total_omzet_this_month']	 = $user->orders_in()->whereMonth('created_at', $now->month)->sum('total_price');
							$data['total_omzet_this_day']    = $user->orders_in()->whereDay('created_at', $now->day)->sum('total_price');

							$data['target'] = Targets::where('month', '=', $now->month)
													 ->where('year', '=', $now->year)
													 ->where('omzet', '>', '0')->first();
							if(!is_null($data['target'])){
								$data['omzet_achievement_this_month'] 	= floor(($data['total_omzet_this_month'] / $data['target']->omzet) * 100);
								$data['target_omzet_per_day'] 			= floor($data['target']->omzet / $now->daysInMonth);
								$data['omzet_achievement_this_day'] 	= floor(($data['total_omzet_this_day'] / $data['target_omzet_per_day']) * 100);
							}
						} elseif($user->hasRole('reseller')){
							$data['total_orders_out']			= $user->orders_out()->count();
							$data['total_purchased_products']	= $user->orders_out()->sum('count');
							$data['total_omzet']				= $user->orders_out()->sum('total_price');
							$data['total_commisions']			= $user->orders_out()->sum('total_commisions');
							$data['total_new_orders']			= $user->orders_out()->where('shipping_status', '=', 'NEW')->count();
							$data['total_packed_orders']		= $user->orders_out()->where('shipping_status', '=', 'PACKED')->count();
							$data['total_on_process_orders']	= $user->orders_out()->where('shipping_status', '=', 'ON PROCESS')->count();
							$data['total_delivered_orders']		= $user->orders_out()->where('shipping_status', '=', 'DELIVERED')->count();
							$data['total_omzet_this_month']		= $user->orders_out()->whereMonth('created_at', $now->month)->sum('total_price');
							$data['total_omzet_this_day']		= $user->orders_out()->whereDay('created_at', $now->day)->sum('total_price');
							$data['total_commisions_this_month']= $user->orders_out()->whereMonth('created_at', $now->month)->sum('total_commisions');
							$data['total_commisions_this_day']	= $user->orders_out()->whereDay('created_at', $now->day)->sum('total_commisions');

							$data['target'] = $user->targets()->where('month', '=', $now->month)
                                                              ->where('year', '=', $now->year)
                                                              ->where('omzet', '>', '0')
                                                              ->where('commision', '>', '0')
                                                              ->first();
							if(!is_null($data['target'])){
								$data['omzet_achievement_this_month'] 		= floor(($data['total_omzet_this_month'] / $data['target']->omzet) * 100);
								$data['target_omzet_per_day'] 				= floor($data['target']->omzet / $now->daysInMonth);
								$data['omzet_achievement_this_day']         = floor(($data['total_omzet_this_day'] / $data['target_omzet_per_day']) * 100);
								$data['commisions_achievement_this_month']  = floor(($data['total_commisions_this_month'] / $data['target']->commision) * 100);
								$data['target_commisions_per_day'] 			= floor($data['target']->commision / $now->daysInMonth);
								$data['commisions_achievement_this_day'] 	= floor(($data['total_commisions_this_day'] / $data['target_commisions_per_day']) * 100);
							}
						}

	        	return view('dashboard.index')->with('data', $data);

		} catch (AuthorizationException $e) {
			Log::error($e);
            return redirect()->action($this->getDashboardAction())->with('error', trans('resisten.This Action is Unauthorized'));
        } catch (\Exception $e) {
            Log::error($e);
            //Avoid Redirect Loop
            if (url()->previous() == url()->current())
                return redirect()->action($this->getDashboardAction())->with('error', $e->getMessage());

            return redirect()->back()->with('error', $e->getLine(). $e->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
