<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatMessages extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'chat_messages';
	
	/**
     * All of the relationships to be touched.
     *
     * @var array
     */
    protected $touches = ['chat_room'];
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'chat_id', 'user_id', 'message', 'status',
    ];
	
	/**
     * Get the user.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
	
	/**
     * Get the chat room that the message belongs to.
     */
    public function chat_room()
    {
        return $this->belongsTo('App\ChatRooms', 'chat_id');
    }
}
