<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */
    public function seeIndex(User $user, User $userModel)
	{
		if (!$user->hasRole('admin'))
            return false;
		
		return true;
	}
	
    public function view(User $user, User $userModel)
    {
        if (!$user->hasRole('admin'))
			return false;
		
        return true;
    }

    /**
     * Determine whether the user can create users.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (!$user->hasRole('admin'))
            return false;

        return true;
    }

    /**
     * Determine whether the user can update the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */
    public function update(User $user, User $userModel)
    {
        if (!$user->hasRole('admin'))
            return false;

        return true;
    }
	
    public function changeStatus(User $user, User $userModel)
    {
        if (!$user->hasRole('admin'))
            return false;

        return true;
    }

    /**
     * Determine whether the user can delete the user.
     *
     * @param  \App\User  $user
     * @param  \App\User  $user
     * @return mixed
     */
    public function delete(User $user, User $userModel)
    {
        if (!$user->hasRole('admin'))
            return false;

        return true;
    }
}
