<?php

namespace App\Policies;

use App\User;
use App\Banks;
use Illuminate\Auth\Access\HandlesAuthorization;

class BanksPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the banks.
     *
     * @param  \App\User  $user
     * @param  \App\Banks  $banks
     * @return mixed
     */
    public function view(User $user, Banks $banks)
    {
        if (!$user->hasRole('admin|supplier|reseller'))
            return false;
		
		return true;
    }

    /**
     * Determine whether the user can create banks.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (!$user->hasRole('admin|supplier|reseller'))
            return false;
		
		return true;
    }

    /**
     * Determine whether the user can update the banks.
     *
     * @param  \App\User  $user
     * @param  \App\Banks  $banks
     * @return mixed
     */
    public function update(User $user, Banks $banks)
    {
        if (!$user->hasRole('admin|supplier|reseller'))
            return false;
		
		return $user->id == $banks->user_id;
    }

    /**
     * Determine whether the user can delete the banks.
     *
     * @param  \App\User  $user
     * @param  \App\Banks  $banks
     * @return mixed
     */
    public function delete(User $user, Banks $banks)
    {
        if (!$user->hasRole('admin|supplier|reseller'))
            return false;
		
		return true;
    }
}
