<?php

namespace App\Policies;

use App\User;
use App\Terms;
use Illuminate\Auth\Access\HandlesAuthorization;

class TermsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the terms.
     *
     * @param  \App\User  $user
     * @param  \App\Terms  $terms
     * @return mixed
     */
    public function view(User $user, Terms $terms)
    {
        if (!$user->hasRole('admin|supplier'))
            return false;
		
		return true;
    }

    /**
     * Determine whether the user can create terms.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (!$user->hasRole('admin|supplier'))
            return false;
		
		return true;
    }

    /**
     * Determine whether the user can update the terms.
     *
     * @param  \App\User  $user
     * @param  \App\Terms  $terms
     * @return mixed
     */
    public function update(User $user, Terms $terms)
    {
        if (!$user->hasRole('admin|supplier'))
            return false;
		
		return $user->id == $terms->user_id;
    }

    /**
     * Determine whether the user can delete the terms.
     *
     * @param  \App\User  $user
     * @param  \App\Terms  $terms
     * @return mixed
     */
    public function delete(User $user, Terms $terms)
    {
        if (!$user->hasRole('admin|supplier'))
            return false;
		
		return $user->id == $terms->user_id;
    }
}
