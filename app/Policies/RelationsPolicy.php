<?php

namespace App\Policies;

use App\User;
use App\Relations;
use Illuminate\Auth\Access\HandlesAuthorization;

class RelationsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the relations.
     *
     * @param  \App\User  $user
     * @param  \App\Relations  $relations
     * @return mixed
     */
    public function seeIndex(User $user, Relations $relations)
    {
        if (!$user->hasRole('supplier'))
            return false;
		
		return true;
    }
	
    public function viewDetails(User $user, Relations $relations)
    {
        if (!$user->hasRole('admin|supplier|reseller'))
            return false;
		
		return true; //$user->id == $relations->request_to;
    }
	
    public function viewResellers(User $user, Relations $relations)
    {
        if (!$user->hasRole('supplier'))
            return false;
		
		return true;
    }
	
    public function view(User $user, Relations $relations)
    {
        if (!$user->hasRole('admin|supplier|reseller'))
            return false;
		
		return true;
    }

    /**
     * Determine whether the user can create relations.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (!$user->hasRole('admin|supplier|reseller'))
            return false;
		
		return true;
    }

    /**
     * Determine whether the user can update the relations.
     *
     * @param  \App\User  $user
     * @param  \App\Relations  $relations
     * @return mixed
     */
    public function requestApproval(User $user, Relations $relations)
    {
        if (!$user->hasRole('supplier'))
            return false;
		
		return $user->id == $relations->request_to;
    }
	
    public function update(User $user, Relations $relations)
    {
        if (!$user->hasRole('admin'))
            return false;
		
		return true;
    }

    /**
     * Determine whether the user can delete the relations.
     *
     * @param  \App\User  $user
     * @param  \App\Relations  $relations
     * @return mixed
     */
    public function delete(User $user, Relations $relations)
    {
        if (!$user->hasRole('admin'))
            return false;
		
		return true;
    }
}
