<?php

namespace App\Policies;

use App\User;
use App\Orders;
use App\Relations;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrdersPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the orders.
     *
     * @param  \App\User  $user
     * @param  \App\Orders  $orders
     * @return mixed
     */
    public function seeIndex(User $user, Orders $orders)
    {
        if (!$user->hasRole('admin'))
            return false;
		
		return true;
    }
	
    public function view(User $user, Orders $orders)
    {
        if (!$user->hasRole('admin|supplier|reseller'))
            return false;
		
		if (!$user->hasRole('admin'))
			return $user->id == $orders->seller_id || $user->id == $orders->buyer_id;
		
		return true;
    }

    /**
     * Determine whether the user can see the payment list page.
     *
     * @param  \App\User  $user
     * @param  \App\Orders  $orders
     * @return mixed
     */
    public function payments(User $user, Orders $orders)
    {
        if (!$user->hasRole('supplier|reseller'))
            return false;
		
		return true;
    }

    /**
     * Determine whether the user can see the payment details page.
     *
     * @param  \App\User  $user
     * @param  \App\Orders  $orders
     * @return mixed
     */
    public function seePayments(User $user, Orders $orders)
    {
        if (!$user->hasRole('supplier|reseller'))
            return false;
		
		return $user->id == $orders->buyer_id;
    }

    /**
     * Determine whether the user can see the shipments page.
     *
     * @param  \App\User  $user
     * @param  \App\Orders  $orders
     * @return mixed
     */
    public function seeShipments(User $user, Orders $orders)
    {
        if (!$user->hasRole('supplier'))
            return false;
		
		return true;
    }

    /**
     * Determine whether the user can see the shipping details page.
     *
     * @param  \App\User  $user
     * @param  \App\Orders  $orders
     * @return mixed
     */
    public function seeShippingDetails(User $user, Orders $orders)
    {
        if (!$user->hasRole('supplier'))
            return false;
		
		return $user->id == $orders->seller_id;
    }

    /**
     * Determine whether the user can create orders.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (!$user->hasRole('admin|supplier|reseller'))
            return false;
		
		return true;
    }

    /**
     * Determine whether the user can update the orders.
     *
     * @param  \App\User  $user
     * @param  \App\Orders  $orders
     * @return mixed
     */
    public function update(User $user, Orders $orders)
    {
        if (!$user->hasRole('admin'))
            return false;
		
		return true;
    }

    /**
     * Determine whether the user can update the orders status to packed.
     *
     * @param  \App\User  $user
     * @param  \App\Orders  $orders
     * @return mixed
     */
    public function packing(User $user, Orders $orders)
    {
        if (!$user->hasRole('supplier'))
            return false;
		
		if($orders->shipping_status != 'NEW' && is_null($orders->payment_image))
			return false;
		
		return $user->id == $orders->seller_id;
    }

    /**
     * Determine whether the user can delete the orders.
     *
     * @param  \App\User  $user
     * @param  \App\Orders  $orders
     * @return mixed
     */
    public function delete(User $user, Orders $orders)
    {
        if (!$user->hasRole('admin'))
            return false;
		
		return true;
    }
}
