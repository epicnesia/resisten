<?php

namespace App\Policies;

use App\User;
use App\Notes;
use Illuminate\Auth\Access\HandlesAuthorization;

class NotesPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the notes.
     *
     * @param  \App\User  $user
     * @param  \App\Notes  $notes
     * @return mixed
     */
    public function view(User $user, Notes $notes)
    {
        if (!$user->hasRole('admin|supplier|reseller'))
            return false;
		
		return true;
    }

    /**
     * Determine whether the user can create notes.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (!$user->hasRole('admin|supplier|reseller'))
            return false;
		
		return true;
    }

    /**
     * Determine whether the user can update the notes.
     *
     * @param  \App\User  $user
     * @param  \App\Notes  $notes
     * @return mixed
     */
    public function update(User $user, Notes $notes)
    {
        if (!$user->hasRole('admin|supplier|reseller'))
            return false;
		
		return $user->id == $notes->user_id;
    }

    /**
     * Determine whether the user can delete the notes.
     *
     * @param  \App\User  $user
     * @param  \App\Notes  $notes
     * @return mixed
     */
    public function delete(User $user, Notes $notes)
    {
        if (!$user->hasRole('admin|supplier|reseller'))
            return false;
		
		return $user->id == $notes->user_id;
    }
}
