<?php

namespace App\Policies;

use App\User;
use App\ChatRooms;
use Illuminate\Auth\Access\HandlesAuthorization;

class ChatsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the chatRooms.
     *
     * @param  \App\User  $user
     * @param  \App\ChatRooms  $chatRooms
     * @return mixed
     */
    public function seeIndex(User $user, ChatRooms $chatRooms)
    {
        if (!$user->hasRole('admin|supplier|reseller'))
            return false;
		
		return true;
    }
	
    public function view(User $user, ChatRooms $chatRooms)
    {
        if (!$user->hasRole('admin|supplier|reseller'))
            return false;
		
		return (($user->id == $chatRooms->sender) || ($user->id == $chatRooms->receiver));
    }

    /**
     * Determine whether the user can create chatRooms.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (!$user->hasRole('admin|supplier|reseller'))
            return false;
		
		return true;
    }

    /**
     * Determine whether the user can update the chatRooms.
     *
     * @param  \App\User  $user
     * @param  \App\ChatRooms  $chatRooms
     * @return mixed
     */
    public function update(User $user, ChatRooms $chatRooms)
    {
        if (!$user->hasRole('admin|supplier|reseller'))
            return false;
		
		return (($user->id == $chatRooms->sender) || ($user->id == $chatRooms->receiver));
    }

    /**
     * Determine whether the user can delete the chatRooms.
     *
     * @param  \App\User  $user
     * @param  \App\ChatRooms  $chatRooms
     * @return mixed
     */
    public function delete(User $user, ChatRooms $chatRooms)
    {
        if (!$user->hasRole('admin|supplier|reseller'))
            return false;
		
		return (($user->id == $chatRooms->sender) || ($user->id == $chatRooms->receiver));
    }
}
