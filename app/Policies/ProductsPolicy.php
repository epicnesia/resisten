<?php

namespace App\Policies;

use App\User;
use App\Products;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the products.
     *
     * @param  \App\User  $user
     * @param  \App\Products  $products
     * @return mixed
     */
    public function seeNewProducts(User $user, Products $products)
    {
    	
		if(!$user->hasRole('admin|supplier|reseller|customer'))
			return false;
		
        return true;
		
    }
	
    public function seeIndex(User $user, Products $products)
    {
    	
		if(!$user->hasRole('admin|supplier'))
			return false;
		
        return true;
		
    }
	
    public function view(User $user, Products $products)
    {
    	
		if(!$user->hasRole('admin|supplier|reseller|customer'))
			return false;
		
		if($user->hasRole('supplier') && $products->show_to == 1)
			return false;
		
		if($user->hasRole('reseller') && $products->show_to == 2)
			return false;
		
        return true;
		
    }

    /**
     * Determine whether the user can create products.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        
		if(!$user->hasRole('admin|supplier'))
			return false;
		
        return true;
		
    }
    public function order(User $user, Products $products)
    {
        if (!$user->hasRole('supplier|reseller'))
            return false;
		
		return $user->id != $products->user_id;
    }

    /**
     * Determine whether the user can update the products.
     *
     * @param  \App\User  $user
     * @param  \App\Products  $products
     * @return mixed
     */
    public function update(User $user, Products $products)
    {
        
		if(!$user->hasRole('admin|supplier'))
			return false;
		
		if(!$user->hasRole('admin')){
			if($user->id <> $products->user_id)
				return false;
		}
		
        return true;
		
    }

    /**
     * Determine whether the user can delete the products.
     *
     * @param  \App\User  $user
     * @param  \App\Products  $products
     * @return mixed
     */
    public function delete(User $user, Products $products)
    {
    	
        if(!$user->hasRole('admin|supplier'))
			return false;
		
		if($user->hasRole('supplier')){
			if($user->id <> $products->user_id)
				return false;
		}
		
        return true;
		
    }
}
