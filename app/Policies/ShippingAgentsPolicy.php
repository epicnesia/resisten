<?php

namespace App\Policies;

use App\User;
use App\ShippingAgents;
use Illuminate\Auth\Access\HandlesAuthorization;

class ShippingAgentsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the shippingAgents.
     *
     * @param  \App\User  $user
     * @param  \App\ShippingAgents  $shippingAgents
     * @return mixed
     */
    public function view(User $user, ShippingAgents $shippingAgents)
    {
        if (!$user->hasRole('admin'))
            return false;
		
		return true;
    }

    /**
     * Determine whether the user can create shippingAgents.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (!$user->hasRole('admin'))
            return false;
		
		return true;
    }

    /**
     * Determine whether the user can update the shippingAgents.
     *
     * @param  \App\User  $user
     * @param  \App\ShippingAgents  $shippingAgents
     * @return mixed
     */
    public function update(User $user, ShippingAgents $shippingAgents)
    {
        if (!$user->hasRole('admin'))
            return false;
		
		return true;
    }

    /**
     * Determine whether the user can delete the shippingAgents.
     *
     * @param  \App\User  $user
     * @param  \App\ShippingAgents  $shippingAgents
     * @return mixed
     */
    public function delete(User $user, ShippingAgents $shippingAgents)
    {
        if (!$user->hasRole('admin'))
            return false;
		
		return true;
    }
}
