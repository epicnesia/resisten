<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingAgents extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'shipping_agents';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'code', 'description', 'status'
    ];
}
