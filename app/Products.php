<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'stock', 'price', 'sale_price', 'commision', 'description', 'weight', 'image', 'show_to', 'status',
    ];
	
	/**
     * Get the supplier.
     */
    public function supplier()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
	
	/**
     * Get the product's category.
     */
    public function categories()
    {
        $term = new TermsRelations();
        return $this->belongsToMany('App\Terms', $term->getTable(), 'product_id', 'term_id');
    }
}
