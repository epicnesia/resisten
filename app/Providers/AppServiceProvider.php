<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Potsky\LaravelLocalizationHelpers\LaravelLocalizationHelpersServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        if ($this->app->environment('local'))
            $this->app->register(LaravelLocalizationHelpersServiceProvider::class);
        if (App::environment('production')) {
            URL::forceScheme('https');
        }
    }
}
