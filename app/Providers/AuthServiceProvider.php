<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' 	=> 'App\Policies\ModelPolicy',
        'App\Products' 	=> 'App\Policies\ProductsPolicy',
        'App\Relations' => 'App\Policies\RelationsPolicy',
        'App\Orders' 	=> 'App\Policies\OrdersPolicy',
        'App\Banks' 	=> 'App\Policies\BanksPolicy',
        'App\Notes' 	=> 'App\Policies\NotesPolicy',
        'App\ChatRooms' => 'App\Policies\ChatsPolicy',
        'App\Terms' 	=> 'App\Policies\TermsPolicy',
        'App\User' 		=> 'App\Policies\UserPolicy',
        'App\ShippingAgents' => 'App\Policies\ShippingAgentsPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
