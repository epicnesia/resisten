<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banks extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'banks';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'bank_name', 'account_number', 'account_name', 'status'
    ];
	
	/**
     * Get the user.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
