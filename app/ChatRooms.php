<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatRooms extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'chat_rooms';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sender', 'receiver', 'info', 'status',
    ];
	
	/**
     * Get the sender user.
     */
    public function sender_user()
    {
        return $this->belongsTo('App\User', 'sender');
    }
	
	/**
     * Get the receiver user.
     */
    public function receiver_user()
    {
        return $this->belongsTo('App\User', 'receiver');
    }
	
	/**
     * Get the messages.
     */
    public function messages()
    {
        return $this->hasMany('App\ChatMessages', 'chat_id');
    }
    
    public static function boot()
    {
    	parent::boot();
    	
    	static::deleted(function($data)
    	{
    		$data->messages()->delete();
		});
	}
	
}
