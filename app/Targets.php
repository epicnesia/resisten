<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Targets extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'targets';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'month', 'year', 'sales', 'omzet', 'status',
    ];
}
