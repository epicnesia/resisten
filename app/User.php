<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kodeine\Acl\Traits\HasRole;

class User extends Authenticatable
{
    use Notifiable, HasRole;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'password', 'name', 'store_name', 'email', 'phone', 'api_token', 'profile_picture',
        'province_id', 'province_name', 'city_id', 'city_name', 'subdistrict_id', 'subdistrict_name',
        'address', 'type', 'android_id', 'gcm_registration_id', 'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token', 'android_id'
    ];
	
	/**
     * Get the supplier products.
     */
    public function products()
    {
        return $this->hasMany('App\Products', 'user_id');
    }
	
	/**
     * Get the user's target.
     */
    public function targets()
    {
        return $this->hasMany('App\Targets', 'user_id');
    }
	
	/**
     * Get the reseller's / supplier's orders out.
     */
    public function orders_out()
    {
        return $this->hasMany('App\Orders', 'buyer_id');
    }
	
	/**
     * Get the supplier's orders in.
     */
    public function orders_in()
    {
        return $this->hasMany('App\Orders', 'seller_id');
    }
	
	/**
     * Get the user's banks.
     */
    public function banks()
    {
        return $this->hasMany('App\Banks', 'user_id');
    }
	
	/**
     * Get the user's notes.
     */
    public function notes()
    {
        return $this->hasMany('App\Notes', 'user_id');
    }
	
	/**
     * Get the user's favorites.
     */
    public function favorites()
    {
        $favorites = new Favorites();
        return $this->belongsToMany('App\Products', $favorites->getTable(), 'user_id', 'product_id');
    }
	
	/**
     * Get the user's child.
     */
    public function child()
    {
        $related = new Relations();
        return $this->belongsToMany('App\User', $related->getTable(), 'parent', 'child');
    }
	
	/**
     * Get the user's categories.
     */
    public function categories()
    {
        return $this->hasMany('App\Terms', 'user_id');
    }
	
}
