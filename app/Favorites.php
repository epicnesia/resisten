<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorites extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'favorites';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'product_id',
    ];
	
	/**
     * Get the user.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
	
	/**
     * Get the product.
     */
    public function product()
    {
        return $this->belongsTo('App\Products', 'product_id');
    }
}
