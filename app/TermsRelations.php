<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TermsRelations extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'terms_relations';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 'term_id', 'status',
    ];
}
