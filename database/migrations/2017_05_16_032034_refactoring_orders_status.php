<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RefactoringOrdersStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('orders', 'shipping_status')){
        	Schema::table('orders', function (Blueprint $table) {
	        	$table->dropColumn('shipping_status');
	        });
        }
		
		Schema::table('orders', function (Blueprint $table) {
        	$table->string('shipping_status')->default('NEW')->nullable()->after('shipping_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('orders', function (Blueprint $table) {
        	$table->dropColumn('shipping_status');
        });
    }
}
