<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpgradePriceLength extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE orders MODIFY count BIGINT UNSIGNED NOT NULL DEFAULT 0');
        DB::statement('ALTER TABLE orders MODIFY total_weight BIGINT UNSIGNED NOT NULL DEFAULT 0');
        DB::statement('ALTER TABLE orders MODIFY total_price BIGINT UNSIGNED NOT NULL DEFAULT 0');
        DB::statement('ALTER TABLE orders MODIFY shipping_cost BIGINT UNSIGNED NOT NULL DEFAULT 0');
        DB::statement('ALTER TABLE orders MODIFY grand_total BIGINT UNSIGNED NOT NULL DEFAULT 0');
		
        DB::statement('ALTER TABLE products MODIFY stock BIGINT NOT NULL DEFAULT 0');
        DB::statement('ALTER TABLE products MODIFY price BIGINT UNSIGNED NOT NULL DEFAULT 0');
        DB::statement('ALTER TABLE products MODIFY sale_price BIGINT UNSIGNED NOT NULL DEFAULT 0');
        DB::statement('ALTER TABLE products MODIFY commision BIGINT UNSIGNED NOT NULL DEFAULT 0');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
