<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('password');
            $table->string('name');
            $table->string('store_name')->nullable();
            $table->string('email')->unique();
            $table->string('phone');
			$table->string('api_token', 100)->unique()->nullable();
            $table->text('profile_picture')->nullable();
            $table->tinyInteger('type')->default(0)->unsigned()->comment('0 = Custommer, 1 = Reseller, 2 = Supplier');
			$table->string('android_id', 100)->nullable();
			$table->rememberToken();
            $table->tinyInteger('status')->default(1)->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
