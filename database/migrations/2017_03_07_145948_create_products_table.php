<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->comment('Supplier ID');
            $table->string('name');
            $table->integer('stock')->default(0);
            $table->integer('price')->default(0)->unsigned();
            $table->integer('sale_price')->default(0)->unsigned()->nullable();
            $table->integer('commision')->default(0)->unsigned();
            $table->text('description')->nullable();
            $table->integer('weight')->default(0)->unsigned()->comment('Gram');
            $table->text('image')->nullable();
            $table->tinyInteger('show_to')->default(1)->unsigned()->comment('1 = Reseller, 2 = Supplier, 3 = All');
            $table->tinyInteger('status')->default(1)->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
