<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
        	$table->string('shipping_phone')->after('shipping_cost')->nullable();
        	$table->string('shipping_name')->after('shipping_cost')->nullable();
            $table->renameColumn('province_id', 'shipping_province_id');
            $table->renameColumn('city_id', 'shipping_city_id');
            $table->renameColumn('subdistrict_id', 'shipping_subdistrict_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->renameColumn('shipping_subdistrict_id', 'subdistrict_id');
            $table->renameColumn('shipping_city_id', 'city_id');
            $table->renameColumn('shipping_province_id', 'province_id');
        	$table->dropColumn('shipping_name');
        	$table->dropColumn('shipping_phone');
        });
    }
}
