<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressDataToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
        	$table->text('address')->nullable()->after('profile_picture');
            $table->string('subdistrict_name')->nullable()->after('profile_picture');
            $table->integer('subdistrict_id')->unsigned()->nullable()->after('profile_picture');
            $table->string('city_name')->nullable()->after('profile_picture');
            $table->integer('city_id')->unsigned()->nullable()->after('profile_picture');
            $table->string('province_name')->nullable()->after('profile_picture');
            $table->integer('province_id')->unsigned()->nullable()->after('profile_picture');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('province_id');
            $table->dropColumn('province_name');
            $table->dropColumn('city_id');
            $table->dropColumn('city_name');
            $table->dropColumn('subdistrict_id');
            $table->dropColumn('subdistrict_name');
            $table->dropColumn('address');
        });
    }
}
