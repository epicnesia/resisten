<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShippingNamesToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
        	$table->string('shipping_province_name')->nullable()->after('shipping_province_id');
        	$table->string('shipping_city_name')->nullable()->after('shipping_city_id');
        	$table->string('shipping_subdistrict_name')->nullable()->after('shipping_subdistrict_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
        	$table->dropColumn('shipping_subdistrict_name');
        	$table->dropColumn('shipping_city_name');
        	$table->dropColumn('shipping_province_name');
        });
    }
}
