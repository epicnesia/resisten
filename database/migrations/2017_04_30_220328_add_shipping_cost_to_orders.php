<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShippingCostToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->integer('shipping_cost')->unsigned()->nullable()->after('subdistrict_id');
            $table->string('shipping_service_code')->nullable()->after('shipping_agents_id');
			$table->renameColumn('customer_id', 'buyer_id');
			$table->renameColumn('products_id', 'product_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
			$table->renameColumn('product_id', 'products_id');
        	$table->renameColumn('buyer_id', 'customer_id');
            $table->dropColumn('shipping_cost');
            $table->dropColumn('shipping_service_code');
        });
    }
}
