<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('products_id')->unsigned()->nullable();
            $table->integer('customer_id')->unsigned()->nullable()->comment('Get ID from users table');
            $table->integer('count')->default(0)->unsigned()->nullable();
            $table->integer('total_weight')->default(0)->unsigned()->nullable();
            $table->integer('total_price')->default(0)->unsigned()->nullable();
            $table->text('description')->nullable();
            $table->text('shipping_address')->nullable();
            $table->date('shipping_date')->nullable();
			$table->integer('shipping_agents_id')->default(0)->unsigned()->nullable();
			$table->string('shipping_code')->nullable();
            $table->tinyInteger('shipping_status')->default(0)->unsigned()->comment('0 = Pending, 1 = Packed, 2 = Sent, 3 = Completed');
            $table->tinyInteger('status')->default(1)->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
