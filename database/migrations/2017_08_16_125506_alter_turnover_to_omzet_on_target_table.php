<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTurnoverToOmzetOnTargetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('targets', function (Blueprint $table) {
            $table->renameColumn('sales', 'omzet');
			$table->renameColumn('turnover', 'commision');
        });
		
		DB::statement('ALTER TABLE targets MODIFY omzet BIGINT UNSIGNED NOT NULL DEFAULT 0');
		DB::statement('ALTER TABLE targets MODIFY commision BIGINT UNSIGNED NOT NULL DEFAULT 0');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('targets', function (Blueprint $table) {
            $table->renameColumn('omzet', 'sales');
            $table->renameColumn('commision', 'turnover');
        });
    }
}
