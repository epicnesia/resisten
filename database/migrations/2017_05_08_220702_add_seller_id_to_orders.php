<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSellerIdToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
        	$table->integer('seller_id')->unsigned()->nullable()->after('product_id')->comment('Get ID from users table');
            $table->text('payment_image')->nullable()->after('grand_total');
        });
		
		Schema::dropIfExists('payments');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('payment_image');
			$table->dropColumn('seller_id');
        });
    }
}
