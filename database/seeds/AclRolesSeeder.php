<?php

use Illuminate\Database\Seeder;
use Kodeine\Acl\Models\Eloquent\Role;
use Kodeine\Acl\Models\Permission;

class AclRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	
    	$role = new Role();
		$roleAdmin = $role->create([
		    'name' => 'Admin',
		    'slug' => 'admin',
		    'description' => 'manage administration privileges'
		]);
		
		$role = new Role();
		$roleBilling = $role->create([
		    'name' => 'Supplier',
		    'slug' => 'supplier',
		    'description' => 'manage supplier privileges'
		]);
		
		$role = new Role();
		$roleBilling = $role->create([
		    'name' => 'Reseller',
		    'slug' => 'reseller',
		    'description' => 'manage reseller privileges'
		]);
		
		$role = new Role();
		$roleBilling = $role->create([
		    'name' => 'Customer',
		    'slug' => 'customer',
		    'description' => 'manage customer privileges'
		]);
    }
}
