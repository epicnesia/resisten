<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ShippingAgentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shipping_agents')->insert([
        	['name' => 'JNE', 'code' => 'jne', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
        	['name' => 'TIKI', 'code' => 'tiki', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
        	['name' => 'J&T', 'code' => 'jnt', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')],
        ]);
    }
}
