<?php

use Illuminate\Database\Seeder;
use App\User;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Add admin user
        $adminId = DB::table('users')->insertGetId([
                                                       'password'   => bcrypt('123456'),
                                                       'name'		=> 'Administrator',
                                                       'store_name'	=> 'Administrator',
                                                       'email'      => 'admin@resisten.id',
                                                       'phone'		=> '1234567890',
                                                       'type'		=> '4',
                                                       'created_at' => date('Y-m-d H:i:s'),
                                                       'updated_at' => date('Y-m-d H:i:s'),
                                                   ]);
        $admin   = User::find($adminId);
        $admin->assignRole('admin'); // Assign Role
    }
}
