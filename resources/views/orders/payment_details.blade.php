@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Order Detail') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->

	<div class="row">
		<div class="col-md-12">
			<div class="box box-resisten">
				<div class="table-responsive">
					<table class="table table-bordered">
						<tr>
							<th class="col-sm-2 text-right">{{ trans('resisten.Order ID') }}</th><td>{{ $order->id }}</td>
						</tr>
						<tr>
							<th class="col-sm-2 text-right">{{ trans('resisten.Product Name') }}</th><td>{{ $order->product->name }}</td>
						</tr>
						<tr>
							<th class="col-sm-2 text-right">{{ trans('resisten.Seller Name') }}</th><td>{{ $order->seller->name }}</td>
						</tr>
						<tr>
							<th class="col-sm-2 text-right">{{ trans('resisten.Count') }}</th><td>{{ $order->count }}</td>
						</tr>
						<tr>
							<th class="col-sm-2 text-right">{{ trans('resisten.Total Weight') }}</th><td>{{ $order->total_weight .' g' }}</td>
						</tr>
						<tr>
							<th class="col-sm-2 text-right">{{ trans('resisten.Total Price') }}</th><td>{{ 'Rp. ' . number_format( $order->total_price, 2, ',', '.') }}</td>
						</tr>
						<tr>
							<th class="col-sm-2 text-right">{{ trans('resisten.Total Shipping Cost') }}</th><td>{{ 'Rp. ' . number_format( $order->shipping_cost, 2, ',', '.') }}</td>
						</tr>
						<tr>
							<th class="col-sm-2 text-right">{{ trans('resisten.Grand Total') }}</th><td>{{ 'Rp. ' . number_format( $order->grand_total, 2, ',', '.') }}</td>
						</tr>
					</table>
				</div>
				<div class="box box-resisten">
					<div class="text-center">
						<h3>{{ trans('resisten.Please make the payment by transfer to the following bank accounts') }}</h3>
					</div>
					<div class="table-responsive">
						@if($supplier)
							@foreach($supplier->banks as $bank)
								<table class="table table-bordered">
									<tr>
										<td class="col-sm-2 text-right">{{ trans('resisten.Bank Name') }}</td><td>{{ $bank->bank_name }}</td>
									</tr>
									<tr>
										<td class="col-sm-2 text-right">{{ trans('resisten.Account Number') }}</td><td>{{ $bank->account_number }}</td>
									</tr>
									<tr>
										<td class="col-sm-2 text-right">{{ trans('resisten.Account Name') }}</td><td>{{ $bank->account_name }}</td>
									</tr>
								</table>
								<hr />
							@endforeach
						@endif
					</div>
				</div>
				<form action="{{ action('OrdersController@payment') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
					{{ csrf_field() }}
					<input type="hidden" name="order_id" value="{{ $order->id }}" />
					<div class="box-body">
						<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
							<label for="image" class="col-sm-2 control-label">{{ trans('resisten.Receipt') }}</label>

							<div class="col-sm-10">
								<input name="image" type="file" class="form-control" id="image" placeholder="{{ trans('resisten.Image') }}" />
								@if ($errors->has('image'))
								<span class="help-block"> <strong>{{ $errors->first('image') }}</strong> </span>
								@endif
							</div>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-resisten pull-right">
							{{ trans('resisten.Submit') }}
						</button>
					</div>
					<!-- /.box-footer -->
				</form>
			</div>
		</div>
	</div>

</section>
<!-- /.content -->
@endsection
