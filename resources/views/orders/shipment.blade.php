@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Shipments') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->
	
	@if(count($orders) > 0)
	<div class="row">
		<div class="col-md-12">
			<div class="box box-resisten">
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>{{ trans('resisten.No') }}</th>
								<th>{{ trans('resisten.Item') }}</th>
								<th>{{ trans('resisten.Amount') }}</th>
								<th>{{ trans('resisten.Options') }}</th>
							</tr>
						</thead>
						<tbody>
							@php $n=1; @endphp
							@foreach($orders as $order)
							
							<tr>
								<td>{{ $n }}</td>
								<td>{{ $order->product->name }}</td>
								<td>{{ 'Rp. ' . number_format( $order->grand_total, 2, ',', '.') }}</td>
								<td>
									<a href="{{ action('OrdersController@shippingDetails', [$order->id]) }}" class="btn btn-resisten">{{ trans('resisten.Detail') }}</a>
								</td>
							</tr>
							
							@php $n++; @endphp
							@endforeach
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-12">
		{{ $orders->links() }}
	</div>
	@endif

</section>
<!-- /.content -->
@endsection
