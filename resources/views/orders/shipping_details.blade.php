@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Order Detail') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->

	<div class="row">
		<div class="col-md-12">
			<div class="box box-resisten">
				<div class="table-responsive">
					<table class="table table-bordered">
						<tr>
							<th class="col-sm-2 text-right">{{ trans('resisten.Order ID') }}</th><td>{{ $order->id }}</td>
						</tr>
						<tr>
							<th class="col-sm-2 text-right">{{ trans('resisten.Product Name') }}</th><td>{{ $order->product->name }}</td>
						</tr>
						<tr>
							<th class="col-sm-2 text-right">{{ trans('resisten.Seller Name') }}</th><td>{{ $order->seller->name }}</td>
						</tr>
						<tr>
							<th class="col-sm-2 text-right">{{ trans('resisten.Count') }}</th><td>{{ $order->count }}</td>
						</tr>
						<tr>
							<th class="col-sm-2 text-right">{{ trans('resisten.Total Weight') }}</th><td>{{ $order->total_weight .' g' }}</td>
						</tr>
						<tr>
							<th class="col-sm-2 text-right">{{ trans('resisten.Total Price') }}</th><td>{{ 'Rp. ' . number_format( $order->total_price, 2, ',', '.') }}</td>
						</tr>
						<tr>
							<th class="col-sm-2 text-right">{{ trans('resisten.Total Shipping Cost') }}</th><td>{{ 'Rp. ' . number_format( $order->shipping_cost, 2, ',', '.') }}</td>
						</tr>
						<tr>
							<th class="col-sm-2 text-right">{{ trans('resisten.Grand Total') }}</th><td>{{ 'Rp. ' . number_format( $order->grand_total, 2, ',', '.') }}</td>
						</tr>
						<tr>
							<th colspan="2"><h3>{{ trans('resisten.Shipping Details') }}</h3></th>
						</tr>
						<tr>
							<th class="col-sm-2 text-right">{{ trans('resisten.Name') }}</th><td>{{ $order->shipping_name }}</td>
						</tr>
						<tr>
							<th class="col-sm-2 text-right">{{ trans('resisten.Phone') }}</th><td>{{ $order->shipping_phone }}</td>
						</tr>
						<tr>
							<th class="col-sm-2 text-right">{{ trans('resisten.Courier') }}</th><td>{{ $order->courier->name }}</td>
						</tr>
						<tr>
							<th class="col-sm-2 text-right">{{ trans('resisten.Shipping Service') }}</th><td>{{ $order->shipping_service_code }}</td>
						</tr>
						<tr>
							<th class="col-sm-2 text-right">{{ trans('resisten.Province') }}</th><td>{{ $order->shipping_province_name }}</td>
						</tr>
						<tr>
							<th class="col-sm-2 text-right">{{ trans('resisten.City') }}</th><td>{{ $order->shipping_city_name }}</td>
						</tr>
						<tr>
							<th class="col-sm-2 text-right">{{ trans('resisten.Subdistrict') }}</th><td>{{ $order->shipping_subdistrict_name }}</td>
						</tr>
						<tr>
							<th class="col-sm-2 text-right">{{ trans('resisten.Address') }}</th><td>{!! $order->shipping_address !!}</td>
						</tr>
							@if(!is_null($order->payment_image))
								@foreach(json_decode($order->payment_image) as $receipt)
								<tr>
									<th class="col-sm-2 text-right">{{ trans('resisten.Payment Receipt') }}</th><td><img class="img-prev-edit" src="{{ url($receipt->url) }}" alt="Payment receipt of {{ $order->id }}" /></td>
								</tr>
								@endforeach
							@else
								<tr>
									<th class="col-sm-2 text-right">{{ trans('resisten.Payment Receipt') }}</th><td><i style="color: red;" class="fa fa-times fa-2x"></i></td>
								</tr>
							@endif
					</table>
				</div>
				<form action="{{ action('OrdersController@shippingCode') }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
					<input type="hidden" name="order_id" value="{{ $order->id }}" />
					<div class="box-body">
						<div class="form-group{{ $errors->has('shipping_code') ? ' has-error' : '' }}">
							<label for="shipping_code" class="col-sm-2 control-label">{{ trans('resisten.Shipping Code') }}</label>

							<div class="col-sm-10">
								<input name="shipping_code" type="text" class="form-control" id="shipping_code" placeholder="{{ trans('resisten.Shipping Code') }}" />
								@if ($errors->has('shipping_code'))
								<span class="help-block"> <strong>{{ $errors->first('shipping_code') }}</strong> </span>
								@endif
							</div>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-resisten pull-right">
							{{ trans('resisten.Submit') }}
						</button>
					</div>
					<!-- /.box-footer -->
				</form>
			</div>
		</div>
	</div>

</section>
<!-- /.content -->
@endsection
