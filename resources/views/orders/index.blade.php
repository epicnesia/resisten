@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Orders') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->
	
	@if(Auth::user()->hasRole('supplier'))
	<div style="margin-bottom: 20px;">
		<a href="{{ url('orders') }}" class="btn btn-resisten">{{ trans('resisten.Orders In') }}</a>
		<a href="{{ url('orders?data=out') }}" class="btn btn-resisten">{{ trans('resisten.Orders Out') }}</a>
	</div>
	@endif
	
		@if(count($orders) > 0)
		<div class="row">
			<div class="col-md-12">
				<div class="box box-resisten">
					<div class="table-responsive">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th style="width: 50px;">{{ trans('resisten.No') }}</th>
									<th>{{ trans('resisten.Item') }}</th>
									<th>{{ trans('resisten.Amount') }}</th>
									<th>{{ trans('resisten.Status') }}</th>
									<th>{{ trans('resisten.Options') }}</th>
								</tr>
							</thead>
							<tbody>
								@php $n=1; @endphp
								@foreach($orders as $order)
								
								<tr>
									<td>{{ $n }}</td>
									<td>
										@if(is_null($order->payment_image))
											@if(Auth::id() == $order->buyer_id)
												<a href="{{ action('OrdersController@paymentDetails', [$order->id]) }}">
											@else
												<a href="{{ action('OrdersController@show', [$order->id]) }}">
											@endif
										@elseif(!is_null($order->payment_image) && is_null($order->shipping_code))
											@if(Auth::id() == $order->seller_id)
												<a href="{{ action('OrdersController@shippingDetails', [$order->id]) }}">
											@else
												<a href="{{ action('OrdersController@show', [$order->id]) }}">
											@endif
										@else
											<a href="{{ action('OrdersController@show', [$order->id]) }}">
										@endif
											{{ $order->product->name }}
										</a>
									</td>
									<td>{{ rupiah_format($order->grand_total) }}</td>
									<td>
										@if($order->shipping_status == 'NEW')
											<span class="label label-danger">{{ $order->shipping_status }}</span>
										@elseif($order->shipping_status == 'PACKED')
											<span class="label label-warning">{{ $order->shipping_status }}</span>
										@elseif($order->shipping_status == 'ON PROCESS')
											<span class="label label-info">{{ $order->shipping_status }}</span>
										@elseif($order->shipping_status == 'DELIVERED')
											<span class="label label-success">{{ $order->shipping_status }}</span>
										@endif
									</td>
									<td>
										<form action="{{ action('OrdersController@updateShippingStatus') }}" method="POST">
											{{ csrf_field() }}
											<input type="hidden" name="order_id" value="{{ $order->id }}" />
											@if(is_null($order->payment_image))
												@if(Auth::id() == $order->buyer_id)
													<a href="{{ action('OrdersController@paymentDetails', [$order->id]) }}" class="btn btn-resisten">
												@else
													<a href="{{ action('OrdersController@show', [$order->id]) }}" class="btn btn-resisten">
												@endif
											@elseif(!is_null($order->payment_image) && is_null($order->shipping_code))
												@if(Auth::id() == $order->seller_id)
													<a href="{{ action('OrdersController@shippingDetails', [$order->id]) }}" class="btn btn-resisten">
												@else
													<a href="{{ action('OrdersController@show', [$order->id]) }}" class="btn btn-resisten">
												@endif
											@else
												<a href="{{ action('OrdersController@show', [$order->id]) }}" class="btn btn-resisten">
											@endif
												{{ trans('resisten.Detail') }}
											</a>
											@if($order->shipping_status == 'ON PROCESS' && !is_null($order->payment_image) && !is_null($order->shipping_code))
											<button type="submit" class="btn btn-resisten">
												<i class="fa fa-refresh"></i> {{ trans('resisten.Update Status') }}
											</button>
											@endif
										</form>
									</td>
								</tr>
								
								@php $n++; @endphp
								@endforeach
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-12">
			@if($data)
				{{ $orders->appends(['data'=>$data])->links() }}
			@else
				{{ $orders->links() }}
			@endif
		</div>
		@endif

</section>
<!-- /.content -->
@endsection
