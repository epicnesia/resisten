@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Create New Order') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->

	<div class="row">
		<div class="col-md-12">
			<div class="box box-resisten">
				<form action="{{ action('OrdersController@store') }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
					<input type="hidden" name="product_id" value="{{ $product->id }}" />
					<div class="box-body">
						<div class="form-group{{ $errors->has('shipping_name') || $errors->has('stock') ? ' has-error' : '' }}">
							<label for="shipping_name" class="col-sm-2 control-label">{{ trans('resisten.Name') }}</label>

							<div class="col-sm-10">
								<input name="shipping_name" type="text" value="{{ old('shipping_name') }}" placeholder="{{ trans('resisten.Name') }}" class="form-control" id="shipping_name">
								@if ($errors->has('shipping_name'))
								<span class="help-block"> <strong>{{ $errors->first('shipping_name') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('shipping_phone') ? ' has-error' : '' }}">
							<label for="shipping_phone" class="col-sm-2 control-label">{{ trans('resisten.Phone') }}</label>

							<div class="col-sm-10">
								<input name="shipping_phone" type="text" value="{{ old('shipping_phone') }}" placeholder="{{ trans('resisten.Phone') }}" class="form-control" id="shipping_phone">
								@if ($errors->has('shipping_phone'))
								<span class="help-block"> <strong>{{ $errors->first('shipping_phone') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('shipping_province_id') ? ' has-error' : '' }}">
							<label for="shipping_province_id" class="col-sm-2 control-label">{{ trans('resisten.Select Province') }}</label>

							<div class="col-sm-10">
								<select id="shipping_province_id" name="shipping_province_id" class="form-control">
									<option value="0">{{ trans('resisten.-- Select Province --') }}</option>
									@foreach($provinces as $province)
										<option value="{{ $province->province_id }}" {{ old('shipping_province_id') == $province->province_id ? "selected" : "" }}>{{ $province->province }}</option>
									@endforeach
								</select>
								<input type="hidden" id="shipping_province_name" name="shipping_province_name" />
								@if ($errors->has('shipping_province_id'))
								<span class="help-block"> <strong>{{ $errors->first('shipping_province_id') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('shipping_city_id') ? ' has-error' : '' }}">
							<label for="shipping_city_id" class="col-sm-2 control-label">{{ trans('resisten.Select City') }}</label>

							<div class="col-sm-10">
								<select id="shipping_city_id" name="shipping_city_id" class="form-control"></select>
								<input type="hidden" id="shipping_city_name" name="shipping_city_name" />
								@if ($errors->has('shipping_city_id'))
								<span class="help-block"> <strong>{{ $errors->first('shipping_city_id') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('shipping_subdistrict_id') ? ' has-error' : '' }}">
							<label for="shipping_subdistrict_id" class="col-sm-2 control-label">{{ trans('resisten.Select Subdistrict') }}</label>

							<div class="col-sm-10">
								<select id="shipping_subdistrict_id" name="shipping_subdistrict_id" class="form-control"></select>
								<input type="hidden" id="shipping_subdistrict_name" name="shipping_subdistrict_name" />
								@if ($errors->has('shipping_subdistrict_id'))
								<span class="help-block"> <strong>{{ $errors->first('shipping_subdistrict_id') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('shipping_address') ? ' has-error' : '' }}">
							<label for="shipping_address" class="col-sm-2 control-label">{{ trans('resisten.Shipping Address') }}</label>

							<div class="col-sm-10">
								<textarea name="shipping_address" rows="5" class="form-control" id="shipping_address" placeholder="{{ trans('resisten.Shipping Address') }}">{{ old('shipping_address') }}</textarea>								
								@if ($errors->has('shipping_address'))
 									<span class="help-block"> <strong>{{ $errors->first('shipping_address') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('name') || $errors->has('count') ? ' has-error' : '' }}">
							<label for="count" class="col-sm-2 control-label">{{ trans('resisten.Count') }}</label>

							<div class="col-sm-3">
								<input name="count" type="number" min="1" max="{{ $product->stock }}" value="{{ old('count') }}" placeholder="{{ trans('resisten.Count') }}" class="form-control" id="count">
								@if ($errors->has('count'))
								<span class="help-block"> <strong>{{ $errors->first('count') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
							<label for="description" class="col-sm-2 control-label">{{ trans('resisten.Description') }}</label>

							<div class="col-sm-10">
								<textarea name="description" rows="5" class="form-control" id="description" placeholder="{{ trans('resisten.Description') }}">{{ old('description') }}</textarea>								
								@if ($errors->has('description'))
 									<span class="help-block"> <strong>{{ $errors->first('description') }}</strong> </span>
								@endif
							</div>
						</div>
						<hr />
						<div class="form-group{{ $errors->has('shipping_agents_id') ? ' has-error' : '' }}">
							<label for="shipping_agents_id" class="col-sm-2 control-label">{{ trans('resisten.Select Courier') }}</label>

							<div class="col-sm-10">
								<select id="shipping_agents_id" name="shipping_agents_id" class="form-control">
									<option value="0">{{ trans('resisten.-- Select Courier --') }}</option>
									@foreach($couriers as $courier)
										<option value="{{ $courier->id }}" {{ old('shipping_agents_id') == $courier->id ? "selected" : "" }} data-code="{{ $courier->code }}">{{ $courier->name }}</option>
									@endforeach
								</select>
								@if ($errors->has('shipping_agents_id'))
								<span class="help-block"> <strong>{{ $errors->first('shipping_agents_id') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('shipping_service_code') ? ' has-error' : '' }}">
							<label for="shipping_service_code" class="col-sm-2 control-label">{{ trans('resisten.Select Service') }}</label>

							<div class="col-sm-10">
								<select id="shipping_service_code" name="shipping_service_code" class="form-control">
									@if(session('shipping_service') !== null)
									
										@foreach(session('shipping_service') as $key => $service)
										<option value="{{ $key }}" {{ old('shipping_service_code') == $key ? 'selected' : '' }} data-cost="{{ $service['cost'] }}">
											{{ $service['service'] }}
											@if($service['cost'] > 0)
												( Rp. {{ number_format( $service['cost'], 2, ',', '.') }} )
											@endif
										</option>
										@endforeach
									
									@endif
								</select>
								@if ($errors->has('shipping_service_code'))
								<span class="help-block"> <strong>{{ $errors->first('shipping_service_code') }}</strong> </span>
								@endif
							</div>
						</div>
						<div id="preview" class="table-responsive">
							<table class="table table-bordered">
								<tr>
									<th class="col-sm-2 text-right">{{ trans('resisten.Stock') }}</th><td class="col-sm-10 text-right">{{ $product->stock }}</td>
								</tr>
								<tr>
									<th class="text-right">{{ trans('resisten.Total Count') }}</th><td class="text-right prevTotalCount"></td>
								</tr>
								<tr>
									<th class="text-right">{{ trans('resisten.Total Weight') }}</th><td class="text-right prevTotalWeight"></td>
								</tr>
								<tr>
									<th class="text-right">{{ trans('resisten.Total Shipping Cost') }}</th><td class="text-right prevTotalShippingCost"></td>
								</tr>
								<tr class="bg-aqua">
									<th class="text-right">{{ trans('resisten.Total Price') }}</th><td class="text-right prevTotalPrice"></td>
								</tr>
								<tr class="bg-orange">
									<th class="text-right">{{ trans('resisten.Grand Total') }}</th><th class="text-right prevGrandTotal"></th>
								</tr>
							</table>
							<div class="jSession">
								<input type="hidden" name="total_weight" value="{{ old('total_weight') }}" />
								<input type="hidden" name="shipping_cost" value="{{ old('shipping_cost') }}" />
								<input type="hidden" name="total_price" value="{{ old('total_price') }}" />
								<input type="hidden" name="grand_total" value="{{ old('grand_total') }}" />
							</div>
						</div>
						<div id="loadingModal">
							<div class="spinner"></div>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-resisten pull-right">
							{{ trans('resisten.Submit') }}
						</button>
					</div>
					<!-- /.box-footer -->
				</form>
			</div>
		</div>
	</div>

</section>
<!-- /.content -->
@endsection
