@extends('layouts.layout_main')

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Order Success') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->

	<div class="row">
		<div class="col-md-12">
			<div class="box box-resisten">
				<div class="text-center">
					<h3>{{ trans('resisten.Order submitted successfully, your order ID:') }}</h3>
					<h1 class="bg-gray" style="padding: 15px 0;">{{ $order->id }}</h1>
					<hr />
					<h3>{{ trans('resisten.The total amount you have to pay:') }}</h3>
					<h1 class="bg-black" style="padding: 15px 0;">{{ 'Rp. ' . number_format( $order->grand_total, 2, ',', '.') }}</h1>
				</div>
			</div>
			<div class="box box-resisten">
				<div class="text-center">
					<h3>{{ trans('resisten.Please make the payment by transfer to the following bank accounts') }}</h3>
				</div>
				<div class="table-responsive">
					@if($supplier)
						@foreach($supplier->banks as $bank)
							<table class="table table-bordered">
								<tr>
									<td class="col-md-3">{{ trans('resisten.Bank Name') }}</td><td>{{ $bank->bank_name }}</td>
								</tr>
								<tr>
									<td class="col-md-3">{{ trans('resisten.Account Number') }}</td><td>{{ $bank->account_number }}</td>
								</tr>
								<tr>
									<td class="col-md-3">{{ trans('resisten.Account Name') }}</td><td>{{ $bank->account_name }}</td>
								</tr>
							</table>
							<hr />
						@endforeach
					@endif
				</div>
			</div>
		</div>
	</div>

</section>
<!-- /.content -->
@endsection
