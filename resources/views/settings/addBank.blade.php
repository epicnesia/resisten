@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Add Bank') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->

	<div class="row">
		<div class="col-md-12">
			<div class="box box-resisten">
				<form action="{{ action('SettingsController@storeBank') }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
					<div class="box-body">
						<input type="hidden" name="user_id" value="{{ Auth::id() }}" />
						<div class="form-group{{ $errors->has('bank_name') ? ' has-error' : '' }}">
							<label for="bank_name" class="col-sm-2 control-label">{{ trans('resisten.Bank Name') }}</label>

							<div class="col-sm-10">
								<input name="bank_name" type="text" value="{{ old('bank_name') }}" placeholder="{{ trans('resisten.Bank Name') }}" class="form-control" id="bank_name">
								@if ($errors->has('bank_name'))
								<span class="help-block"> <strong>{{ $errors->first('bank_name') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('account_number') ? ' has-error' : '' }}">
							<label for="account_number" class="col-sm-2 control-label">{{ trans('resisten.Account Number') }}</label>

							<div class="col-sm-10">
								<input name="account_number" type="text" value="{{ old('account_number') }}" placeholder="{{ trans('resisten.Account Number') }}" class="form-control" id="account_number">
								@if ($errors->has('account_number'))
								<span class="help-block"> <strong>{{ $errors->first('account_number') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('account_name') ? ' has-error' : '' }}">
							<label for="account_name" class="col-sm-2 control-label">{{ trans('resisten.Account Name') }}</label>

							<div class="col-sm-10">
								<input name="account_name" type="text" value="{{ old('account_name') }}" placeholder="{{ trans('resisten.Account Name') }}" class="form-control" id="account_name">
								@if ($errors->has('account_name'))
								<span class="help-block"> <strong>{{ $errors->first('account_name') }}</strong> </span>
								@endif
							</div>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-resisten pull-right">
							{{ trans('resisten.Add') }}
						</button>
					</div>
					<!-- /.box-footer -->
				</form>
			</div>
		</div>
	</div>

</section>
<!-- /.content -->
@endsection
