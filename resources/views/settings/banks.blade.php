@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Banks List') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->

	@if(count($banks) > 0)
	<div class="box">
		<div class="box-body">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>{{ trans('resisten.Bank Name') }}</th>
						<th>{{ trans('resisten.Account Number') }}</th>
						<th>{{ trans('resisten.Account Name') }}</th>
						<th>{{ trans('resisten.Options') }}</th>
					</tr>
				</thead>
				<tbody>
					@foreach($banks as $bank)
					<tr>
						<td>{{ $bank->bank_name }}</td>
						<td>{{ $bank->account_number }}</td>
						<td>{{ $bank->account_name }}</td>
						<td>
							<form action="{{ action('SettingsController@destroyBank', [$bank->id]) }}" method="POST" onsubmit="return confirm('{{ trans('resisten.Are you sure to delete this item?') }}');">
								{{ csrf_field() }}
								<input type="hidden" name="_method" value="DELETE" />
								<a href="{{ action('SettingsController@editBank', [$bank->id]) }}" class="btn btn-info"><i class="fa fa-pencil"></i> {{ trans('resisten.Edit') }}</a>
								<button type="submit" class="btn btn-danger">
									<i class="fa fa-trash-o"></i> {{ trans('resisten.Delete') }}
								</button>
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<!-- /.box-body -->
		<div class="box-footer clearfix">
			{{ $banks->links() }}
		</div>
	</div>
	@endif

	<!-- Float Action Button -->
	<a class="add-btn" href="{{ action('SettingsController@addBank') }}"> <i class="fa fa-plus"></i> </a>
	<!-- End Float Action Button -->

</section>
<!-- /.content -->
@endsection
