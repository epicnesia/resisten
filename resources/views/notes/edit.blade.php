@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Create New Note') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->

	<div class="row">
		<div class="col-md-12">
			<div class="box box-resisten">
				<form action="{{ action('NotesController@update', [$note->id]) }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
					<input type="hidden" name="_method" value="PUT" />
					<div class="box-body">
						<input type="hidden" name="user_id" value="{{ Auth::id() }}" />
						<div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
							<label for="title" class="col-sm-2 control-label">{{ trans('resisten.Title') }}</label>

							<div class="col-sm-10">
								<input name="title" type="text" value="{{ old('title', $note->title) }}" placeholder="{{ trans('resisten.Title') }}" class="form-control" id="title">
								@if ($errors->has('title'))
								<span class="help-block"> <strong>{{ $errors->first('title') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
							<label for="content" class="col-sm-2 control-label">{{ trans('resisten.Content') }}</label>

							<div class="col-sm-10">
								<textarea name="content" id="content" class="form-control summernote" placeholder="{{ trans('resisten.Content') }}">{{ old('content', $note->content) }}</textarea>
								@if ($errors->has('content'))
								<span class="help-block"> <strong>{{ $errors->first('content') }}</strong> </span>
								@endif
							</div>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-resisten pull-right">
							{{ trans('resisten.Save') }}
						</button>
					</div>
					<!-- /.box-footer -->
				</form>
			</div>
		</div>
	</div>

</section>
<!-- /.content -->
@endsection
