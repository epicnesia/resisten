@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Notes') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->
	
	@if(count($notes) > 0)
	<div class="box box-resisten">
		<div class="box-body">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>{{ trans('resisten.Title') }}</th>
						<th>{{ trans('resisten.Created At') }}</th>
						<th>{{ trans('resisten.Options') }}</th>
					</tr>
				</thead>
				<tbody>
					@foreach($notes as $note)
					<tr>
						<td class="col-sm-6">{{ $note->title }}</td>
						<td>{{ $note->created_at->toFormattedDateString() }}</td>
						<td>
							<form action="{{ action('NotesController@destroy', [$note->id]) }}" method="POST" onsubmit="return confirm('{{ trans('resisten.Are you sure to delete this item?') }}');">
								{{ csrf_field() }}
								<input type="hidden" name="_method" value="DELETE" />
								<a href="{{ action('NotesController@edit', [$note->id]) }}" class="btn btn-info"><i class="fa fa-pencil"></i> {{ trans('resisten.Edit') }}</a>
								<button type="submit" class="btn btn-danger">
									<i class="fa fa-trash-o"></i> {{ trans('resisten.Delete') }}
								</button>
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<!-- /.box-body -->
		<div class="box-footer clearfix">
			{{ $notes->links() }}
		</div>
	</div>
	@endif
	
	<!-- Float Action Button -->
	<a class="add-btn" href="{{ action('NotesController@create') }}">
		<i class="fa fa-plus"></i>
	</a>
	<!-- End Float Action Button -->

</section>
<!-- /.content -->
@endsection
