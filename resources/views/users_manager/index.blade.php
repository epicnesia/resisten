@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Users Manager') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->

	<div class="row">
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="info-box bg-aqua">
				<span class="info-box-icon"><i class="fa fa-users"></i></span>

				<div class="info-box-content">
					<span class="info-box-text">{{ trans('resisten.Top Supplier') }}</span>
					<span class="info-box-number">{{ isset($top_supplier->seller->name) && !empty($top_supplier->seller->name) ? $top_supplier->seller->name : '' }}</span>

					<div class="progress">
						<div class="progress-bar" style="width: 100%"></div>
					</div>
					<span class="progress-description"> {{ trans('resisten.Total Orders') }} {{ isset($top_supplier->total_orders) && !empty($top_supplier->total_orders) ? $top_supplier->total_orders : '' }}</span>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<div class="info-box bg-green">
				<span class="info-box-icon"><i class="fa fa-id-card"></i></span>

				<div class="info-box-content">
					<span class="info-box-text">{{ trans('resisten.Top Reseller') }}</span>
					<span class="info-box-number">{{ isset($top_reseller->buyer->name) && !empty($top_reseller->buyer->name) ? $top_reseller->buyer->name : '' }}</span>

					<div class="progress">
						<div class="progress-bar" style="width: 100%"></div>
					</div>
					<span class="progress-description"> {{ trans('resisten.Total Orders') }} {{ isset($top_reseller->total_orders) && !empty($top_reseller->total_orders) ? $top_reseller->total_orders : '' }}</span>
				</div>
			</div>
		</div>
	</div>

	<div class="box box-resisten">
		<div class="box-body">
			<div class="col-sm-6 pull-right" style="margin-bottom: 10px;">
				<div class="row">
					<form action="{{ action('UsersManagerController@index') }}" method="GET">
						<div class="input-group">
							<input class="form-control" type="text" name="s" value="{{ Request::get('s') }}" placeholder="{{ trans('resisten.Type here to search') }}">
							<div class="input-group-btn">
								<button type="submit" class="btn btn-resisten">
									{{ trans('resisten.Search') }}
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			@if(count($users) > 0)
			<table class="table table-bordered">
				<thead>
					<tr>
						<th style="width: 50px;">{{ trans('resisten.ID') }}</th>
						<th>{{ trans('resisten.Name') }}</th>
						<th>{{ trans('resisten.Store Name') }}</th>
						<th>{{ trans('resisten.Email') }}</th>
						<th>{{ trans('resisten.Phone') }}</th>
						<th>{{ trans('resisten.Type') }}</th>
						<th>{{ trans('resisten.Options') }}</th>
					</tr>
				</thead>
				<tbody>
					@foreach($users as $user)
					<tr>
						<td>{{ $user->id }}</td>
						<td>
							<a href="{{ action('UsersManagerController@show', [$user->id]) }}">{{ $user->name }}</a>
						</td>
						<td>{{ $user->store_name !== null ? $user->store_name : '-' }}</td>
						<td>{{ $user->email }}</td>
						<td>{{ $user->phone }}</td>
						<td>{{ $user->type == 1 ? trans('resisten.Reseller') : trans('resisten.Supplier') }}</td>
						<td>
							<a href="{{ action('UsersManagerController@edit', [$user->id]) }}" class="btn btn-info"><i class="fa fa-pencil"></i> {{ trans('resisten.Edit') }}</a>
							@if($user->status == 0)
							<a href="{{ action('UsersManagerController@changeStatus', [$user->id]) }}" class="btn btn-danger"><i class="fa fa-times"></i> {{ trans('resisten.Click to Enable') }}</a>
							@elseif($user->status == 1)
							<a href="{{ action('UsersManagerController@changeStatus', [$user->id]) }}" class="btn btn-success"><i class="fa fa-check"></i> {{ trans('resisten.Click to Disable') }}</a>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{{ $users->appends(['s'=>Request::get('s')])->links() }}
			@endif
		</div>
	</div>

</section>
<!-- /.content -->
@endsection
