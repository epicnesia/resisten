@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Edit User Details') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->
	
	@if($user)
	<div class="box box-resisten">
		<form action="{{ action('UsersManagerController@update', [$user->id]) }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
					{{ csrf_field() }}
					<input type="hidden" name="_method" value="PUT" />
					<div class="box-body">
						<div class="form-group{{ $errors->has('profile_picture') ? ' has-error' : '' }}">
							<label for="profile_picture" class="col-sm-2 control-label">{{ trans('resisten.Profile Picture') }}</label>

							<div class="col-sm-10">
								@if($user->profile_picture)
									<img class="img-prev-edit" src="{{ url('show_image/'.$user->profile_picture) }}" alt="{{ $user->name }} profile picture" />
								@endif
								<input name="profile_picture" type="file" class="form-control" id="profile_picture" placeholder="{{ trans('resisten.Profile Picture') }}" />
								@if ($errors->has('profile_picture'))
								<span class="help-block"> <strong>{{ $errors->first('profile_picture') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<label for="name" class="col-sm-2 control-label">{{ trans('resisten.Name') }}</label>

							<div class="col-sm-10">
								<input name="name" type="text" value="{{ old('name', $user->name) }}" placeholder="{{ trans('resisten.Name') }}" class="form-control" id="name" required>
								@if ($errors->has('name'))
								<span class="help-block"> <strong>{{ $errors->first('name') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
							<label for="email" class="col-sm-2 control-label">{{ trans('resisten.Email') }}</label>

							<div class="col-sm-10">
								<input name="email" type="email" value="{{ old('email', $user->email) }}" placeholder="{{ trans('resisten.Email') }}" class="form-control" id="email" required>
								@if ($errors->has('email'))
								<span class="help-block"> <strong>{{ $errors->first('email') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
							<label for="phone" class="col-sm-2 control-label">{{ trans('resisten.Phone') }}</label>

							<div class="col-sm-10">
								<input name="phone" type="text" value="{{ old('phone', $user->phone) }}" placeholder="{{ trans('resisten.Phone') }}" class="form-control" id="phone" required>
								@if ($errors->has('phone'))
								<span class="help-block"> <strong>{{ $errors->first('phone') }}</strong> </span>
								@endif
							</div>
						</div>
						@if($user->type == 2)
						<div class="form-group{{ $errors->has('store_name') ? ' has-error' : '' }}">
							<label for="store_name" class="col-sm-2 control-label">{{ trans('resisten.Store Name') }}</label>

							<div class="col-sm-10">
								<input name="store_name" type="text" value="{{ old('store_name', $user->store_name) }}" placeholder="{{ trans('resisten.Store Name') }}" class="form-control" id="store_name" required>
								@if ($errors->has('store_name'))
								<span class="help-block"> <strong>{{ $errors->first('store_name') }}</strong> </span>
								@endif
							</div>
						</div>
						@endif
						<div class="form-group{{ $errors->has('province_id') ? ' has-error' : '' }}">
							<label for="province_id" class="col-sm-2 control-label">{{ trans('resisten.Select Province') }}</label>

							<div class="col-sm-10">
								<select id="province_id" name="province_id" class="form-control">
									<option value="0">{{ trans('resisten.-- Select Province --') }}</option>
									@foreach($provinces as $province)
										<option value="{{ $province->province_id }}" {{ old('province_id', $user->province_id) == $province->province_id ? "selected" : "" }}>{{ $province->province }}</option>
									@endforeach
								</select>
								<input type="hidden" id="province_name" name="province_name" value="{{ old('province_name', $user->province_name) }}" />
								@if ($errors->has('province_id'))
								<span class="help-block"> <strong>{{ $errors->first('province_id') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('city_id') ? ' has-error' : '' }}">
							<label for="city_id" class="col-sm-2 control-label">{{ trans('resisten.Select City') }}</label>

							<div class="col-sm-10">
								<select id="city_id" name="city_id" class="form-control">
									@if($cities)
										<option value="0">{{ trans('resisten.-- Select City --') }}</option>
										@foreach($cities as $city)
											<option value="{{ $city->city_id }}" {{ old('city_id', $user->city_id) == $city->city_id ? "selected" : "" }}>{{ $city->type }} {{ $city->city_name }}</option>
										@endforeach
									@endif
								</select>
								<input type="hidden" id="city_name" name="city_name" value="{{ old('city_name', $user->city_name) }}" />
								@if ($errors->has('city_id'))
								<span class="help-block"> <strong>{{ $errors->first('city_id') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('subdistrict_id') ? ' has-error' : '' }}">
							<label for="subdistrict_id" class="col-sm-2 control-label">{{ trans('resisten.Select Subdistrict') }}</label>

							<div class="col-sm-10">
								<select id="subdistrict_id" name="subdistrict_id" class="form-control">
									@if($subdistricts)
										<option value="0">{{ trans('resisten.-- Select Subdistrict --') }}</option>
										@foreach($subdistricts as $subdistrict)
											<option value="{{ $subdistrict->subdistrict_id }}" {{ old('subdistrict_id', $user->subdistrict_id) == $subdistrict->subdistrict_id ? "selected" : "" }}>{{ $subdistrict->subdistrict_name }}</option>
										@endforeach
									@endif
								</select>
								<input type="hidden" id="subdistrict_name" name="subdistrict_name" value="{{ old('subdistrict_name', $user->subdistrict_name) }}" />
								@if ($errors->has('subdistrict_id'))
								<span class="help-block"> <strong>{{ $errors->first('subdistrict_id') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
							<label for="address" class="col-sm-2 control-label">{{ trans('resisten.Address') }}</label>

							<div class="col-sm-10">
								<textarea name="address" placeholder="{{ trans('resisten.Address') }}" class="form-control summernote" id="address" rows="10">{{ old('address', $user->address) }}</textarea>
								@if ($errors->has('address'))
								<span class="help-block"> <strong>{{ $errors->first('address') }}</strong> </span>
								@endif
							</div>
						</div>
						<div id="loadingModal">
							<div class="spinner"></div>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-resisten pull-right">
							{{ trans('resisten.Save') }}
						</button>
					</div>
					<!-- /.box-footer -->
				</form>
	</div>
	@endif

</section>
<!-- /.content -->
@endsection
