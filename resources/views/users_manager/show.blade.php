@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.User Details') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->
	
	@if($user)
	<div class="box box-resisten">
		<div class="box-body">
			<table class="table table-bordered">
				<tr>
					<th class="col-sm-2">{{ trans('resisten.Profile Picture') }}</th>
					<td>
						@if($user->profile_picture)
							<img style="width: 300px;" src="{{ url('show_image/'.$user->profile_picture) }}" alt="{{ $user->name }} Avatar" />
						@else
							<img style="width: 300px;" src="http://placehold.it/160x160" alt="{{ $user->name }} Avatar" />
						@endif
					</td>
				</tr>
				<tr><th>{{ trans('resisten.Name') }}</th><td>{{ $user->name }}</td></tr>
				<tr><th>{{ trans('resisten.Store Name') }}</th><td>{{ $user->store_name !== null ? $user->store_name : '-' }}</td></tr>
				<tr><th>{{ trans('resisten.Email') }}</th><td>{{ $user->email }}</td></tr>
				<tr><th>{{ trans('resisten.Phone') }}</th><td>{{ $user->phone }}</td></tr>
				<tr><th>{{ trans('resisten.Province') }}</th><td>{{ $user->province_name }}</td></tr>
				<tr><th>{{ trans('resisten.City') }}</th><td>{{ $user->city_name }}</td></tr>
				<tr><th>{{ trans('resisten.Subdistrict') }}</th><td>{{ $user->subdistrict_name }}</td></tr>
				<tr><th>{{ trans('resisten.Address') }}</th><td>{!! $user->address !!}</td></tr>
				<tr><th>{{ trans('resisten.Type') }}</th><td>{{ $user->type == 1 ? trans('resisten.Reseller') : trans('resisten.Supplier') }}</td></tr>
				<tr>
					<td colspan="2">
						<a href="{{ action('UsersManagerController@edit', [$user->id]) }}" class="btn btn-info pull-right"><i class="fa fa-pencil"></i> {{ trans('resisten.Edit') }}</a>
					</td>
				</tr>
			</table>
		</div>
	</div>
	@endif

</section>
<!-- /.content -->
@endsection
