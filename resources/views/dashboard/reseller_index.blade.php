<!-- First squares row -->
<div class="row">
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box bg-aqua">
			<span class="info-box-icon"><i class="fa fa-shopping-bag"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">{{ trans('resisten.Total Orders') }}</span>
				<span class="info-box-number">{{ $data['total_orders_out'] }}</span>

				<div class="progress">
					<div class="progress-bar" style="width: 100%"></div>
				</div>
				<span class="progress-description"> {{ trans('resisten.Your total orders') }} </span>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box bg-green">
			<span class="info-box-icon"><i class="fa fa-send"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">{{ trans('resisten.Total Purchased Products') }}</span>
				<span class="info-box-number">{{ $data['total_purchased_products'] }}</span>

				<div class="progress">
					<div class="progress-bar" style="width: 100%"></div>
				</div>
				<span class="progress-description"> {{ trans('resisten.Total Purchased Products') }} </span>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box bg-yellow">
			<span class="info-box-icon"><i class="fa fa-money"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">{{ trans('resisten.Total Omzet') }}</span>
				<span class="info-box-number">{{ rupiah_format($data['total_omzet']) }}</span>

				<div class="progress">
					<div class="progress-bar" style="width: 100%"></div>
				</div>
				<span class="progress-description"> {{ trans('resisten.Total Omzet') }} </span>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box bg-red">
			<span class="info-box-icon"><i class="fa fa-gift"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">{{ trans('resisten.Total Commisions') }}</span>
				<span class="info-box-number">{{ rupiah_format($data['total_commisions']) }}</span>

				<div class="progress">
					<div class="progress-bar" style="width: 100%"></div>
				</div>
				<span class="progress-description"> {{ trans('resisten.Total Commisions') }} </span>
			</div>
		</div>
	</div>
</div>
<!-- End of First squares row -->

<!-- Second squares row -->
<div class="row">
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box bg-navy">
			<span class="info-box-icon"><i class="fa fa-shopping-basket"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">{{ trans('resisten.Total New Orders') }}</span>
				<span class="info-box-number">{{ $data['total_new_orders'] }}</span>

				<div class="progress">
					<div class="progress-bar" style="width: 100%"></div>
				</div>
				<span class="progress-description"> {{ trans('resisten.Total New Orders') }} </span>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box bg-teal">
			<span class="info-box-icon"><i class="fa fa-cube"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">{{ trans('resisten.Total Packed Orders') }}</span>
				<span class="info-box-number">{{ $data['total_packed_orders'] }}</span>

				<div class="progress">
					<div class="progress-bar" style="width: 100%"></div>
				</div>
				<span class="progress-description"> {{ trans('resisten.Total Packed Orders') }} </span>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box bg-purple">
			<span class="info-box-icon"><i class="fa fa-truck"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">{{ trans('resisten.Total On Process Orders') }}</span>
				<span class="info-box-number">{{ $data['total_on_process_orders'] }}</span>

				<div class="progress">
					<div class="progress-bar" style="width: 100%"></div>
				</div>
				<span class="progress-description"> {{ trans('resisten.Total On Process Orders') }} </span>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box bg-maroon">
			<span class="info-box-icon"><i class="fa fa-thumbs-up"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">{{ trans('resisten.Total Delivered Orders') }}</span>
				<span class="info-box-number">{{ $data['total_delivered_orders'] }}</span>

				<div class="progress">
					<div class="progress-bar" style="width: 100%"></div>
				</div>
				<span class="progress-description"> {{ trans('resisten.Total Delivered Orders') }} </span>
			</div>
		</div>
	</div>
</div>
<!-- End of Second squares row -->

@if(!is_null($data['target']))
<div class="row">
	<!-- Daily donut chart -->
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="box box-resisten">
			<div class="box-header">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">{{ trans('resisten.Today Achievement') }}</h3>
            </div>
            <div class="row">
            	<div class="col-md-6 col-sm-6 col-xs-12">
	            	<div class="circliful" data-percent="{{ $data['omzet_achievement_this_day'] }}" data-foregroundColor="#f39c12" data-percentageY="100" data-text="{{ trans('resisten.Omzet') }}" data-textY="120"></div>
					<div class="table-responsive">
						<table class="table table-bordered">
							<tr><th>{{ trans('resisten.Target Omzet Today') }}</th><td>{{ rupiah_format($data['target_omzet_per_day']) }}</td></tr>
							<tr><th>{{ trans('resisten.Total Omzet Today') }}</th><td>{{ rupiah_format($data['total_omzet_this_day']) }}</td></tr>
						</table>
					</div>
            	</div>
            	<div class="col-md-6 col-sm-6 col-xs-12">
	            	<div class="circliful" data-percent="{{ $data['commisions_achievement_this_day'] }}" data-foregroundColor="#dd4b39" data-percentageY="100" data-text="{{ trans('resisten.Commisions') }}" data-textY="120"></div>
					<div class="table-responsive">
						<table class="table table-bordered">
							<tr><th>{{ trans('resisten.Target Commisions Today') }}</th><td>{{ rupiah_format($data['target_commisions_per_day']) }}</td></tr>
							<tr><th>{{ trans('resisten.Total Commisions Today') }}</th><td>{{ rupiah_format($data['total_commisions_this_day']) }}</td></tr>
						</table>
					</div>
            	</div>
            </div>
		</div>
	</div>
	<!-- End of Daily donut chart -->

	<!-- Monthly donut chart -->
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="box box-resisten">
			<div class="box-header">
              <i class="fa fa-bar-chart-o"></i>

              <h3 class="box-title">{{ trans('resisten.This Month Achievement') }}</h3>
            </div>
            <div class="row">
            	<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="circliful" data-percent="{{ $data['omzet_achievement_this_month'] }}" data-foregroundColor="#f39c12" data-percentageY="100" data-text="{{ trans('resisten.Omzet') }}" data-textY="120"></div>
					<div class="table-responsive">
						<table class="table table-bordered">
							<tr><th>{{ trans('resisten.Target Omzet This Month') }}</th><td>{{ rupiah_format($data['target']->omzet) }}</td></tr>
							<tr><th>{{ trans('resisten.Total Omzet This Month') }}</th><td>{{ rupiah_format($data['total_omzet_this_month']) }}</td></tr>
						</table>
					</div>
				</div>
            	<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="circliful" data-percent="{{ $data['commisions_achievement_this_month'] }}" data-foregroundColor="#dd4b39" data-percentageY="100" data-text="{{ trans('resisten.Commisions') }}" data-textY="120"></div>
					<div class="table-responsive">
						<table class="table table-bordered">
							<tr><th>{{ trans('resisten.Target Commisions This Month') }}</th><td>{{ rupiah_format($data['target']->commision) }}</td></tr>
							<tr><th>{{ trans('resisten.Total Commisions This Month') }}</th><td>{{ rupiah_format($data['total_commisions_this_month']) }}</td></tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End of Monthly donut chart -->
</div>
@endif