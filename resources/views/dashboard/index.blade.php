@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>{{ trans('resisten.Dashboard') }}</h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->
	
	@if(Auth::user()->hasRole('admin'))
		@include('dashboard.admin_index')
	@elseif(Auth::user()->hasRole('supplier'))
		@include('dashboard.supplier_index')
	@elseif(Auth::user()->hasRole('reseller'))
		@include('dashboard.reseller_index')
	@endif

</section>
<!-- /.content -->
@endsection
