<!-- First squares row -->
<div class="row">
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box bg-aqua">
			<span class="info-box-icon"><i class="fa fa-users"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">{{ trans('resisten.Total Users') }}</span>
				<span class="info-box-number">{{ $data['total_users'] }}</span>

				<div class="progress">
					<div class="progress-bar" style="width: 100%"></div>
				</div>
				<span class="progress-description"> {{ trans('resisten.Total Users') }} </span>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box bg-green">
			<span class="info-box-icon"><i class="fa fa-cubes"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">{{ trans('resisten.Total Products') }}</span>
				<span class="info-box-number">{{ $data['total_products'] }}</span>

				<div class="progress">
					<div class="progress-bar" style="width: 100%"></div>
				</div>
				<span class="progress-description"> {{ trans('resisten.Total Products') }} </span>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box bg-yellow">
			<span class="info-box-icon"><i class="fa fa-cart-arrow-down"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">{{ trans('resisten.Total Orders') }}</span>
				<span class="info-box-number">{{ $data['total_orders'] }}</span>

				<div class="progress">
					<div class="progress-bar" style="width: 100%"></div>
				</div>
				<span class="progress-description"> {{ trans('resisten.Total Orders') }} </span>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box bg-red">
			<span class="info-box-icon"><i class="fa fa-send"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">{{ trans('resisten.Total Sales') }}</span>
				<span class="info-box-number">{{ $data['total_sales'] }}</span>

				<div class="progress">
					<div class="progress-bar" style="width: 100%"></div>
				</div>
				<span class="progress-description"> {{ trans('resisten.Total Sales') }} </span>
			</div>
		</div>
	</div>
</div>
<!-- End of First squares row -->

<!-- Second squares row -->
<div class="row">
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box bg-navy">
			<span class="info-box-icon"><i class="fa fa-shopping-basket"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">{{ trans('resisten.Total New Orders') }}</span>
				<span class="info-box-number">{{ $data['total_new_orders'] }}</span>

				<div class="progress">
					<div class="progress-bar" style="width: 100%"></div>
				</div>
				<span class="progress-description"> {{ trans('resisten.Total New Orders') }} </span>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box bg-teal">
			<span class="info-box-icon"><i class="fa fa-cube"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">{{ trans('resisten.Total Packed Orders') }}</span>
				<span class="info-box-number">{{ $data['total_packed_orders'] }}</span>

				<div class="progress">
					<div class="progress-bar" style="width: 100%"></div>
				</div>
				<span class="progress-description"> {{ trans('resisten.Total Packed Orders') }} </span>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box bg-purple">
			<span class="info-box-icon"><i class="fa fa-truck"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">{{ trans('resisten.Total On Process Orders') }}</span>
				<span class="info-box-number">{{ $data['total_on_process_orders'] }}</span>

				<div class="progress">
					<div class="progress-bar" style="width: 100%"></div>
				</div>
				<span class="progress-description"> {{ trans('resisten.Total On Process Orders') }} </span>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-6 col-xs-12">
		<div class="info-box bg-maroon">
			<span class="info-box-icon"><i class="fa fa-thumbs-up"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">{{ trans('resisten.Total Delivered Orders') }}</span>
				<span class="info-box-number">{{ $data['total_delivered_orders'] }}</span>

				<div class="progress">
					<div class="progress-bar" style="width: 100%"></div>
				</div>
				<span class="progress-description"> {{ trans('resisten.Total Delivered Orders') }} </span>
			</div>
		</div>
	</div>
</div>
<!-- End of Second squares row -->

<!-- Start of Big Squares row -->
<div class="row">
	@if(!is_null($data['latest_users']))
	<div class="col-sm-4">
		<div class="box box-danger">
			<div class="box-header with-border">
				<h3 class="box-title">{{ trans('resisten.Latest Users') }}</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body no-padding">
				<ul class="users-list clearfix">
					@foreach($data['latest_users'] as $user)
					<li>
						@if($user->profile_picture)
							@foreach(json_decode($user->profile_picture) as $image)
								<img class="img-circle" src="{{ url($image->url) }}" alt="{{ $user->name }} Avatar" />
							@endforeach
						@else
							{!! get_gravatar($user->email, 160, 'mm', 'g', true, ['class'=>'img-circle', 'alt'=> $user->name.' Avatar']) !!}
						@endif
						{{ $user->name }}
						<span class="users-list-date">{{ $user->created_at->toFormattedDateString() }}</span>
					</li>
					@endforeach
				</ul>
				<!-- /.users-list -->
			</div>
			<!-- /.box-body -->
			<div class="box-footer text-center">
				<a href="{{ action('UsersManagerController@index') }}" class="uppercase">View All Users</a>
			</div>
			<!-- /.box-footer -->
		</div>
	</div>
	@endif

	@if(!is_null($data['latest_products']))
	<div class="col-sm-4">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">{{ trans('resisten.Recently Added Products') }}</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<ul class="products-list product-list-in-box">
					@foreach($data['latest_products'] as $product)
					<li class="item">
						<div class="product-img">
							@php $images = json_decode($product->image, true); @endphp
							<a href="{{ action('ProductsManagerController@show', $product->id) }}">
								<img src="{{ url($images[0]['url']) }}" alt="{{ $product->name }} Image">
							</a>
						</div>
						<div class="product-info">
							<a href="{{ action('ProductsManagerController@show', $product->id) }}">
								{{ $product->name }}
							</a>
							<span class="label label-warning pull-right">{{ rupiah_format($product->price) }}</span>
							<span class="product-description">{{ $product->supplier->name }}</span>
						</div>
					</li>
					@endforeach
				</ul>
			</div>
			<!-- /.box-body -->
			<div class="box-footer text-center">
				<a href="{{ action('ProductsManagerController@index') }}" class="uppercase">View All Products</a>
			</div>
			<!-- /.box-footer -->
		</div>
	</div>
	@endif

	@if(!is_null($data['latest_orders']))
	<div class="col-sm-4">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">{{ trans('resisten.Latest Orders') }}</h3>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="table-responsive">
					<table class="table no-margin">
						<thead>
							<tr>
								<th>Order ID</th>
								<th>Item</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							@foreach($data['latest_orders'] as $order)
							<tr>
								<td>{{ $order->id }}</td>
								<td>{{ $order->product->name }}</td>
								<td>
									@if($order->shipping_status == 'NEW')
										<span class="label label-danger">{{ $order->shipping_status }}</span>
									@elseif($order->shipping_status == 'PACKED')
										<span class="label label-warning">{{ $order->shipping_status }}</span>
									@elseif($order->shipping_status == 'ON PROCESS')
										<span class="label label-info">{{ $order->shipping_status }}</span>
									@elseif($order->shipping_status == 'DELIVERED')
										<span class="label label-success">{{ $order->shipping_status }}</span>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<!-- /.table-responsive -->
			</div>
			<!-- /.box-body -->
			<div class="box-footer text-center">
				<a href="{{ action('OrdersManagerController@index') }}" class="uppercase">View All Orders</a>
			</div>
			<!-- /.box-footer -->
		</div>
	</div>
	@endif

</div>
<!-- End of Big Squares row -->
