@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Messages') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->

	@if(count($chats) > 0)
		@foreach($chats as $chat)
			<a href="{{ action('ChatsController@show', [$chat->id]) }}">
				<div class="box box-widget widget-user-2">
					<!-- Add the bg color to the header using any of the bg-* classes -->
					<div class="widget-user-header">
						<div class="widget-user-image">
							@if(Auth::id() == $chat->sender)
								@if($chat->receiver_user->profile_picture)
									@foreach(json_decode($chat->receiver_user->profile_picture) as $profile_picture)
										<img class="img-circle" src="{{ url($profile_picture->url) }}" alt="{{ $chat->receiver_user->name }} Avatar" />
									@endforeach
								@else
									{!! get_gravatar($chat->receiver_user->email, 160, 'mm', 'g', true, ['class'=>'img-circle', 'alt'=> $chat->receiver_user->name.' Avatar']) !!}
								@endif
							@else
								@if($chat->sender_user->profile_picture)
									@foreach(json_decode($chat->sender_user->profile_picture) as $profile_picture)
										<img class="img-circle" src="{{ url($profile_picture->url) }}" alt="{{ $chat->sender_user->name }} Avatar" />
									@endforeach
								@else
									{!! get_gravatar($chat->sender_user->email, 160, 'mm', 'g', true, ['class'=>'img-circle', 'alt'=> $chat->sender_user->name.' Avatar']) !!}
								@endif
							@endif
						</div>
						<!-- /.widget-user-image -->
						<h3 class="widget-user-username">
							@if(Auth::id() == $chat->sender)
								{{ $chat->receiver_user->name }}
							@else
								{{ $chat->sender_user->name }}
							@endif
						</h3>
						<h5 class="widget-user-desc">
							@if(Auth::id() == $chat->sender)
								{{ $chat->receiver_user->type == 1 ? trans('resisten.Reseller') : trans('resisten.Supplier') }}
							@else
								{{ $chat->sender_user->type == 1 ? trans('resisten.Reseller') : trans('resisten.Supplier') }}
							@endif
						</h5>
						<form class="chat-del-btn" action="{{ action('ChatsController@destroy', [$chat->id]) }}" method="POST" onsubmit="return confirm('{{ trans('resisten.Are you sure to delete this item?') }}');">
							{{ csrf_field() }}
							<input type="hidden" name="_method" value="DELETE" />
							<button type="submit" class="btn btn-resisten">
								<i class="fa fa-trash-o"></i> {{ trans('resisten.Delete') }}
							</button>
						</form>
					</div>
				</div>
			</a>
		@endforeach
	@endif

	<!-- Float Action Button -->
	<a class="add-btn" href="{{ action('ChatsController@create') }}"> <i class="fa fa-plus"></i> </a>
	<!-- End Float Action Button -->

</section>
<!-- /.content -->
@endsection
