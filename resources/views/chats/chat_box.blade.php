@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Chat Box') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->
	
	<div class="row">
		<div class="col-md-12">
			<div class="box box-resisten direct-chat direct-chat-resisten">
				<div class="box-body">
					<!-- Conversations are loaded here -->
					<div id="chat-box" class="direct-chat-messages"></div>
					<!--/.direct-chat-messages-->
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<form id="chat-form" action="{{ action('ChatsController@storeChat', [$chat->id]) }}" method="POST">
						{{ csrf_field() }}
						<div class="input-group">
							<input type="hidden" name="chat_id" value="{{ $chat->id }}" />
							<input type="hidden" name="user_id" value="{{ Auth::id() }}" />
							<input type="text" name="message" placeholder="Type Message ..." class="form-control">
							<span class="input-group-btn">
								<button type="submit" class="btn btn-resisten btn-flat">
									{{ trans('resisten.Send') }}
								</button> </span>
						</div>
					</form>
				</div>
				<!-- /.box-footer-->
			</div>
		</div>
	</div>

</section>
<!-- /.content -->
@endsection
