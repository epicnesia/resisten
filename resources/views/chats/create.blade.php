@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Select User to Send Message') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->
		
	@if(count($contacts) > 0)
	<div class="box box-resisten">
		<div class="box-body">
			<table class="table table-bordered">
				<tbody>
					@foreach($contacts as $contact)
					<tr>
						<td>{{ $contact->name }}</td>
						<td>{{ $contact->type == '2' ? trans('resisten.Supplier') : trans('resisten.Reseller') }}</td>
						<td style="width: 10%;">
							<form action="{{ action('ChatsController@store') }}" method="POST">
								{{ csrf_field() }}
								<input type="hidden" name="receiver" value="{{ $contact->id }}" />
								<button type="submit" class="btn btn-resisten">
									{{ trans('resisten.Start Chat') }}
								</button>
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<!-- /.box-body -->
		<div class="box-footer clearfix">
			{{ $contacts->links() }}
		</div>
	</div>
	@endif

</section>
<!-- /.content -->
@endsection
