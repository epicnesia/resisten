@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Orders Manager') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->
	
	
	<div class="box box-resisten">
		<div class="box-body">
			@if(count($orders) > 0)
			<table class="table table-bordered">
				<thead>
					<tr>
						<th style="width: 50px;">{{ trans('resisten.No') }}</th>
						<th>{{ trans('resisten.Item') }}</th>
						<th>{{ trans('resisten.Amount') }}</th>
						<th>{{ trans('resisten.Status') }}</th>
						<th>{{ trans('resisten.Options') }}</th>
					</tr>
				</thead>
				<tbody>
					@foreach($orders as $order)
					<tr>
						<td>{{ $order->id }}</td>
						<td>
							<a href="{{ action('OrdersManagerController@show', [$order->id]) }}">
								{{ $order->product->name }}
							</a>
						</td>
						<td>{{ rupiah_format($order->grand_total) }}</td>
						<td>
							@if($order->shipping_status == 'NEW')
								<span class="label label-danger">{{ $order->shipping_status }}</span>
							@elseif($order->shipping_status == 'PACKED')
								<span class="label label-warning">{{ $order->shipping_status }}</span>
							@elseif($order->shipping_status == 'ON PROCESS')
								<span class="label label-info">{{ $order->shipping_status }}</span>
							@elseif($order->shipping_status == 'DELIVERED')
								<span class="label label-success">{{ $order->shipping_status }}</span>
							@endif
						</td>
						<td>
							<form action="{{ action('OrdersController@updateShippingStatus') }}" method="POST">
								{{ csrf_field() }}
								<input type="hidden" name="order_id" value="{{ $order->id }}" />
								<a href="{{ action('OrdersManagerController@show', [$order->id]) }}" class="btn btn-resisten">
									{{ trans('resisten.Detail') }}
								</a>
								@if($order->shipping_status == 'ON PROCESS' && !is_null($order->payment_image) && !is_null($order->shipping_code))
								<button type="submit" class="btn btn-resisten">
									<i class="fa fa-refresh"></i> {{ trans('resisten.Update Status') }}
								</button>
								@endif
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{{ $orders->appends(['s'=>Request::get('s')])->links() }}
			@endif
		</div>
	</div>

</section>
<!-- /.content -->
@endsection
