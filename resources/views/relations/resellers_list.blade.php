@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Resellers List') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->

	@if(count($resellers) > 0)
	<div class="box box-resisten">
		<div class="box-body">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>{{ trans('resisten.Name') }}</th>
						<th>{{ trans('resisten.Email') }}</th>
						<th>{{ trans('resisten.Phone') }}</th>
						<th>{{ trans('resisten.Status') }}</th>
					</tr>
				</thead>
				<tbody>
					@foreach($resellers as $reseller)
					<tr>
						<td>
							<a href="{{ action('RelationsController@show', [$reseller->id]) }}">
								{{ $reseller->name }}
							</a>
						</td>
						<td>{{ $reseller->email }}</td>
						<td>{{ $reseller->phone }}</td>
						<td>{!! $reseller->relation_status == 2 ? '<span class="label label-success">'.trans('resisten.Approved').'</span>' : '<span class="label label-warning">'.trans('resisten.Pending').'</span>' !!}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<!-- /.box-body -->
		<div class="box-footer clearfix">
			{{ $resellers->links() }}
		</div>
	</div>
	@endif

</section>
<!-- /.content -->
@endsection
