@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Suppliers List') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->

	@if(count($suppliers) > 0)
	<div class="box box-resisten">
		<div class="box-body">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>{{ trans('resisten.Name') }}</th>
						<th>{{ trans('resisten.Store Name') }}</th>
						<th>{{ trans('resisten.Status') }}</th>
					</tr>
				</thead>
				<tbody>
					@foreach($suppliers as $supplier)
					<tr>
						<td>
							<a href="{{ action('RelationsController@show', [$supplier->id]) }}">
								{{ $supplier->name }}
							</a>
						</td>
						<td>{{ !is_null($supplier->store_name) ? $supplier->store_name : '-' }}</td>
						<td>{!! $supplier->relation_status == 2 ? '<span class="label label-success">'.trans('resisten.Approved').'</span>' : '<span class="label label-warning">'.trans('resisten.Pending').'</span>' !!}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<!-- /.box-body -->
		<div class="box-footer clearfix">
			{{ $suppliers->links() }}
		</div>
	</div>
	@else
	<a href="{{ action('RelationsController@create') }}">
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-info alert-dismissable">
					<strong>{{trans('notifications.info')}} :</strong> {{ trans('resisten.You have no related users, click here to add!') }}
				</div>
			</div>
		</div>
	</a>
	@endif

	<!-- Float Action Button -->
	<a class="add-btn" href="{{ action('RelationsController@create') }}"> <i class="fa fa-plus"></i> </a>
	<!-- End Float Action Button -->

</section>
<!-- /.content -->
@endsection
