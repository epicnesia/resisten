@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Add Supplier') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->

	<div class="box box-resisten">
		<div class="box-header with-border">
			<form id="searchSupplierForm" action="{{ action('RelationsController@search') }}" method="GET">
				<div class="input-group input-group-lg">
					<input type="text" name="s" class="form-control" placeholder="{{ trans('resisten.Type here to search') }}" autocomplete="off">
					<span class="input-group-btn">
						<button type="submit" class="btn btn-resisten btn-flat">
							<i class="fa fa-search fa-lg"></i>
						</button> 
					</span>
				</div>
			</form>
		</div>
		<div class="box-body">
			<div id="loader" class="center hidden">
				<i class="fa fa-spinner fa-pulse fa-3x fa-fw margin-bottom"></i>
			</div>
			<div id="searchResult"></div>
		</div>
		<!-- /.box-body -->
	</div>

</section>
<!-- /.content -->
@endsection
