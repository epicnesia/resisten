@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.User Details') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->
	<div class="box box-widget widget-user">
		<div class="widget-user-header bg-resisten">
			<h3 class="widget-user-username">{{ $user->name }}</h3>
			@if($user->type == 2)
			<h5 class="widget-user-desc">{{ !is_null($user->store_name) ? $user->store_name : '-' }}</h5>
			@endif
		</div>
		<div class="widget-user-image">
			@if($user->profile_picture)
				@foreach(json_decode($user->profile_picture) as $profile_picture)
				<img class="img-circle" src="{{ url($profile_picture->url) }}" alt="{{ $user->name }} Avatar" />
				@endforeach
			@else
				{!! get_gravatar($user->email, 160, 'mm', 'g', true, ['class'=>'img-circle', 'alt'=> $user->name.' Avatar']) !!}
			@endif
		</div>
		<div class="box-footer">
			<div class="row">
				<div class="col-sm-4 border-right">
					<div class="description-block">
						<h5 class="description-header">{{ $user->type == 2 ? $user->products()->count() : $user->orders_out()->count() }}</h5>
						<span class="description-text">{{ $user->type == 2 ? trans('resisten.Products') : trans('resisten.Orders') }}</span>
					</div>
					<!-- /.description-block -->
				</div>
				<!-- /.col -->
				<div class="col-sm-4 border-right">
					<div class="description-block">
						<h5 class="description-header">{{ $user->type == 2 ? $user->orders_in()->sum('count') : $user->orders_out()->sum('count') }}</h5>
						<span class="description-text">{{ $user->type == 2 ? trans('resisten.Sales') : trans('resisten.Purchased') }}</span>
					</div>
					<!-- /.description-block -->
				</div>
				<!-- /.col -->
				<div class="col-sm-4">
					<div class="description-block">
						<h5 class="description-header">{{ $user->type == 2 ? $user->orders_in()->where('shipping_status', '=', 'DELIVERED')->count() : $user->orders_out()->where('shipping_status', '=', 'DELIVERED')->count() }}</h5>
						<span class="description-text">{{ $user->type == 2 ? trans('resisten.Delivered') : trans('resisten.Delivered') }}</span>
					</div>
					<!-- /.description-block -->
				</div>
				<!-- /.col -->
				@if($user->type == 2 && !$relations)
					<div class="col-sm-12">
						<form action="{{ action('RelationsController@add') }}" method="POST">
							{{ csrf_field() }}
							<input type="hidden" name="supplier" value="{{ $user->id }}" />
							<button class="btn btn-danger center-block" type="submit">{{ strtoupper(trans('resisten.Add')) }}</button>
						</form>
					</div>
				@elseif($user->type == 1 && $relations->status == 1)
					<form class="col-sm-6" action="{{ action('RelationsController@accept') }}" method="POST">
						{{ csrf_field() }}
						<input type="hidden" name="requestant" value="{{ $relations->requestant_user->id }}" />
						<div class="row">
							<button class="btn btn-success pull-right" type="submit">
								{{ strtoupper(trans('resisten.Accept')) }}
							</button>
						</div>
					</form>
					<form class="col-sm-6" action="{{ action('RelationsController@reject') }}" method="POST">
						{{ csrf_field() }}
						<input type="hidden" name="requestant" value="{{ $relations->requestant_user->id }}" />
						<div class="row">
							<button class="btn btn-danger pull-left reject-btn" type="submit">
								{{ strtoupper(trans('resisten.Reject')) }}
							</button>
						</div>
					</form>
				@endif
			</div>
		</div>
	</div>
	@if($user->type == 2)
		@if(count($products) > 0)
			<div class="row">
				<div class="col-lg-2">
					<div class="box box-default">
						<div class="box-header with-border">
							<h3 class="box-title">{{ trans('resisten.Categories') }}</h3>
						</div>
						<div class="box-footer">
							<ul class="nav nav-stacked">
								<li><a href="{{ action('RelationsController@show', ['id'=>$user->id]) }}" class="no-padding">{{ trans('resisten.All Products') }}</a></li>
								@foreach($showcase_categories as $category)
								<li><a href="{{ route('relations_category', ['id'=> $user->id, 'slug'=>$category->slug]) }}" class="no-padding capitalize">{{ $category->name }}</a></li>
								@endforeach
							</ul>
						</div>
					</div>
				</div>
				<div class="col-lg-10">
					<div class="row">
						@foreach($products as $product)
							<div class="col-lg-3 col-md-4 col-sm-6">
								<div class="box box-widget widget-user-2">
									<div class="widget-user-header">
										<div class="widget-user-image">
											@if($product->supplier->profile_picture)
												@foreach(json_decode($product->supplier->profile_picture) as $profile_picture)
												<img class="img-circle" src="{{ url($profile_picture->url) }}" alt="{{ $product->supplier->name }} Avatar" />
												@endforeach
											@else
												{!! get_gravatar($product->supplier->email, 160, 'mm', 'g', true, ['class'=>'img-circle', 'alt'=> $product->supplier->name.' Avatar']) !!}
											@endif
									   	</div>
									  	<h4 class="widget-user-username">{{ $product->supplier->name }}</h4>
										<h5 class="widget-user-desc">{{ $product->created_at->format('d-M-Y H:i:s') }}</h5>
									</div>
									<div class="box-footer no-padding">
										@if(json_check($product->image))
											<div class="carousel">
												@foreach(json_decode($product->image) as $image)
													<img class="img-responsive" src="{{ url($image->url) }}" alt="{{ $product->name }} Image" />
												@endforeach
											</div>
										@else
										<img class="img-responsive" src="{{ url($product->image) }}" alt="{{ $product->name }} Image" />
										@endif
									</div>
									<div class="box-footer">
										<h4>{{ $product->name }}</h4>
										<ul class="nav nav-stacked">
								           	<li>{{ trans('resisten.Price') }} <span class="pull-right">{{ rupiah_format($product->price) }}</span></li>
								      		<li>{{ trans('resisten.Commision') }} <span class="pull-right">{{ rupiah_format($product->commision) }}</span></li>
								           	@php
								          		$show_to = trans('resisten.All');

								                if($product->show_to == 1){
								                	$show_to = trans('resisten.Reseller');
								                } elseif ($product->show_to == 2) {
								                	$show_to = trans('resisten.Supplier');
								                }

								            @endphp
								        	<li>{{ trans('resisten.Show to') }} <span class="pull-right">{{ $show_to }}</span></li>
								      	</ul>
									</div>
									@if(Auth::id() == $product->user_id)
									<div class="box-footer">
										<a href="{{ action('ProductsController@edit', [$product->id]) }}" class="btn btn-resisten pull-right"><i class="fa fa-edit fa-lg"></i> {{ trans('resisten.Edit') }}</a>
									</div>
									@endif
								</div>
							</div>
						@endforeach
					</div>
				</div>
			</div>
			<div class="col-sm-12">
				{{ $products->links() }}
			</div>
		@endif
	@elseif($user->type == 1)
		@if(count($orders) > 0)
			<div class="box box-resisten">
				<div class="box-header with-border">
					<h3 class="box-title">{{ trans('resisten.Orders') }}</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="table-responsive">
						<table class="table no-margin">
							<thead>
								<tr>
									<th>{{ trans('resisten.Supplier') }}</th>
									<th>{{ trans('resisten.Order ID') }}</th>
									<th>{{ trans('resisten.Date') }}</th>
									<th>{{ trans('resisten.Item') }}</th>
									<th>{{ trans('resisten.Shipping Status') }}</th>
								</tr>
							</thead>
							<tbody>
								@foreach($orders as $order)
								<tr>
									<td>{{ $order->seller->name }}</td>
									<td>{{ $order->id }}</td>
									<td>{{ $order->created_at->format('j-m-Y H:i:s') }}</td>
									<td>{{ $order->product->name }}</td>
									<td>
										@if($order->shipping_status == 'NEW')
											<span class="label label-danger">{{ $order->shipping_status }}</span>
										@elseif($order->shipping_status == 'PACKED')
											<span class="label label-warning">{{ $order->shipping_status }}</span>
										@elseif($order->shipping_status == 'ON PROCESS')
											<span class="label label-info">{{ $order->shipping_status }}</span>
										@elseif($order->shipping_status == 'DELIVERED')
											<span class="label label-success">{{ $order->shipping_status }}</span>
										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<!-- /.table-responsive -->
				</div>
				<!-- /.box-body -->
				<div class="box-footer clearfix">
					{{ $orders->links() }}
	            </div>
			</div>
		@endif
	@endif

</section>
<!-- /.content -->
@endsection
