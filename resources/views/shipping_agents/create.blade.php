@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Add Shipping Agents') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->

	<div class="row">
		<div class="col-md-12">
			<div class="box box-resisten">
				<form action="{{ action('ShippingAgentsController@store') }}" method="POST" class="form-horizontal">
					{{ csrf_field() }}
					<div class="box-body">
						<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
							<label for="name" class="col-sm-2 control-label">{{ trans('resisten.Name') }}</label>

							<div class="col-sm-10">
								<input name="name" type="text" value="{{ old('name') }}" placeholder="{{ trans('resisten.Name') }}" class="form-control" id="name">
								@if ($errors->has('name'))
								<span class="help-block"> <strong>{{ $errors->first('name') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
							<label for="code" class="col-sm-2 control-label">{{ trans('resisten.Code') }}</label>

							<div class="col-sm-10">
								<input name="code" type="text" value="{{ old('code') }}" placeholder="{{ trans('resisten.Code') }}" class="form-control" id="code">
								@if ($errors->has('code'))
								<span class="help-block"> <strong>{{ $errors->first('code') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
							<label for="description" class="col-sm-2 control-label">{{ trans('resisten.Description') }}</label>

							<div class="col-sm-10">
								<textarea name="description" id="description" class="form-control summernote" placeholder="{{ trans('resisten.Description') }}">{{ old('description') }}</textarea>
								@if ($errors->has('description'))
								<span class="help-block"> <strong>{{ $errors->first('description') }}</strong> </span>
								@endif
							</div>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-resisten pull-right">
							{{ trans('resisten.Save') }}
						</button>
					</div>
					<!-- /.box-footer -->
				</form>
			</div>
		</div>
	</div>

</section>
<!-- /.content -->
@endsection
