@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Shipping Agents Manager') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->
	
	
	<div class="box box-resisten">
		<div class="box-body">
			<div class="col-sm-6 pull-right" style="margin-bottom: 10px;">
				<div class="row">
					<form action="{{ action('ShippingAgentsController@index') }}" method="GET">
						<div class="input-group">
							<input class="form-control" type="text" name="s" value="{{ Request::get('s') }}" placeholder="{{ trans('resisten.Type here to search') }}">
							<div class="input-group-btn">
								<button type="submit" class="btn btn-resisten">
									{{ trans('resisten.Search') }}
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			@if(count($couriers) > 0)
			<table class="table table-bordered">
				<thead>
					<tr>
						<th style="width: 50px;">{{ trans('resisten.ID') }}</th>
						<th>{{ trans('resisten.Name') }}</th>
						<th>{{ trans('resisten.Code') }}</th>
						<th>{{ trans('resisten.Description') }}</th>
						<th>{{ trans('resisten.Options') }}</th>
					</tr>
				</thead>
				<tbody>
					@foreach($couriers as $courier)
					<tr>
						<td>{{ $courier->id }}</td>
						<td>{{ $courier->name }}</td>
						<td>{{ $courier->code }}</td>
						<td>{!! $courier->description !!}</td>
						<td>
							<form action="{{ action('ShippingAgentsController@destroy', [$courier->id]) }}" method="POST" onsubmit="return confirm('{{ trans('resisten.Are you sure to delete this item?') }}');">
								{{ csrf_field() }}
								<input type="hidden" name="_method" value="DELETE" />
								<a href="{{ action('ShippingAgentsController@edit', [$courier->id]) }}" class="btn btn-info"><i class="fa fa-pencil"></i> {{ trans('resisten.Edit') }}</a>
								<button type="submit" class="btn btn-danger">
									<i class="fa fa-trash-o"></i> {{ trans('resisten.Delete') }}
								</button>
							</form>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{{ $couriers->appends(['s'=>Request::get('s')])->links() }}
			@endif
		</div>
	</div>
	
	<!-- Float Action Button -->
	<a class="add-btn" href="{{ action('ShippingAgentsController@create') }}">
		<i class="fa fa-plus"></i>
	</a>
	<!-- End Float Action Button -->

</section>
<!-- /.content -->
@endsection
