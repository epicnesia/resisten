@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Products Manager') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="info-box bg-aqua">
				<span class="info-box-icon"><i class="fa fa-cubes"></i></span>

				<div class="info-box-content">
					<span class="info-box-text">{{ trans('resisten.Top Product') }}</span>
					<span class="info-box-number">{{ isset($top_products->product->name) && !empty($top_products->product->name) ? $top_products->product->name : '' }}</span>

					<div class="progress">
						<div class="progress-bar" style="width: 100%"></div>
					</div>
					<span class="progress-description"> {{ trans('resisten.Total Orders') }} {{ isset($top_products->total_orders) && !empty($top_products->total_orders) ? $top_products->total_orders : '' }}</span>
				</div>
			</div>
		</div>
	</div>

	<div class="box box-resisten">
		<div class="box-body">
			<div class="col-sm-6 pull-right" style="margin-bottom: 10px;">
				<div class="row">
					<form action="{{ action('ProductsManagerController@index') }}" method="GET">
						<div class="input-group">
							<input class="form-control" type="text" name="s" value="{{ Request::get('s') }}" placeholder="{{ trans('resisten.Type here to search') }}">
							<div class="input-group-btn">
								<button type="submit" class="btn btn-resisten">
									{{ trans('resisten.Search') }}
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			@if(count($products) > 0)
			<table class="table table-bordered">
				<thead>
					<tr>
						<th style="width: 50px;">{{ trans('resisten.ID') }}</th>
						<th style="width: 300px;">{{ trans('resisten.Image') }}</th>
						<th>{{ trans('resisten.Name') }}</th>
						<th>{{ trans('resisten.Supplier') }}</th>
						<th>{{ trans('resisten.Price') }}</th>
						<th>{{ trans('resisten.Commision') }}</th>
						<th>{{ trans('resisten.Stock') }}</th>
						<th>{{ trans('resisten.Options') }}</th>
					</tr>
				</thead>
				<tbody>
					@foreach($products as $product)
					<tr>
						<td>{{ $product->id }}</td>
						@php $images = json_decode($product->image, true); @endphp
						<td><img class="img-responsive" src="{{ url($images[0]['url']) }}" alt="{{ $product->name }} Image" /></td>
						<td>
							<a href="{{ action('ProductsManagerController@show', [$product->id]) }}">{{ $product->name }}</a>
						</td>
						<td>{{ $product->supplier->name }}</td>
						<td>{{ $product->price }}</td>
						<td>{{ $product->commision }}</td>
						<td>{{ $product->stock }}</td>
						<td>
							<a href="{{ action('ProductsManagerController@edit', [$product->id]) }}" class="btn btn-info"><i class="fa fa-pencil"></i> {{ trans('resisten.Edit') }}</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			{{ $products->appends(['s'=>Request::get('s')])->links() }}
			@endif
		</div>
	</div>

</section>
<!-- /.content -->
@endsection
