@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Product Details') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->

	<div class="row">
		<div class="col-sm-12">
			<div class="box box-resisten">
				<div class="box box-widget">
					<div class="box-header with-border">
						<div class="user-block">
							@if($product->supplier->profile_picture)
								@foreach(json_decode($product->supplier->profile_picture) as $profile_picture)
								<img class="img-circle" src="{{ url($profile_picture->url) }}" alt="{{ $product->supplier->name }} Avatar" />
								@endforeach
							@else
								{!! get_gravatar($product->supplier->email, 160, 'mm', 'g', true, ['class'=>'img-circle', 'alt'=> $product->supplier->name.' Avatar']) !!}
							@endif
							<span class="username">{{ $product->supplier->name }}</span>
							<span class="description">{{ $product->created_at->format('d-M-Y H:i:s') }}</span>
						</div>
						<!-- /.user-block -->
						<div class="box-tools">
							@if(Auth::user()->hasRole('admin'))
								<a href="{{ action('ProductsManagerController@edit', [$product->id]) }}" class="btn btn-resisten pull-right">{{ trans('resisten.Edit') }}</i></a>
							@endif
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="col-md-6 pad">
							@if(json_check($product->image))
								<div class="carousel">
									@foreach(json_decode($product->image) as $image)
										<img class="img-responsive" src="{{ url($image->url) }}" alt="{{ $product->name }} Image" />
									@endforeach
								</div>
							@else
								<img class="img-responsive" src="{{ url($product->image) }}" alt="{{ $product->name }} Image" />
							@endif
						</div>
						<div class="col-md-6 pad">
							<div class="table-responsive">
								<table class="table table-bordered table-hover">
									<tr>
										<th style="width: 20%;">{{ trans('resisten.Product Name') }}</th><td>{{ $product->name }}</td>
									</tr>
									<tr>
										<th style="width: 20%;">{{ trans('resisten.Price') }}</th><td>{{ 'Rp. ' . number_format( $product->price, 2, ',', '.') }}</td>
									</tr>
									<tr>
										<th style="width: 20%;">{{ trans('resisten.Commision') }}</th><td>{{ 'Rp. ' . number_format( $product->commision, 2, ',', '.') }}</td>
									</tr>
									<tr>
										<th style="width: 20%;">{{ trans('resisten.Weight') }}</th><td>{{ $product->weight }} g</td>
									</tr>
									<tr>
										<th style="width: 20%;">{{ trans('resisten.Stock') }}</th><td>{{ $product->stock }}</td>
									</tr>
									<tr>
										<th style="width: 20%;">{{ trans('resisten.Description') }}</th><td>{!! $product->description !!}</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
					<div class="box-footer">
						@if(Auth::user()->hasRole('admin'))
							<a href="{{ action('ProductsManagerController@edit', [$product->id]) }}" class="btn btn-resisten pull-right">{{ trans('resisten.Edit') }}</i></a>
						@endif
					</div>
					<!-- /.box-footer -->
				</div>
			</div>
		</div>
	</div>

</section>
<!-- /.content -->
@endsection
