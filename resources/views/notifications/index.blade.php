@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Notifications') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->

	<div class="nav-tabs-custom">
		<ul class="nav nav-tabs nav-tabs-resisten">
			<li class="active">
				<a href="#orders" data-toggle="tab" aria-expanded="true">{{ trans('resisten.Orders') }}
				@if(isset($orders) && count($orders) > 0) <span class="label label-resisten">{{ count($orders) }}</span> @endif </a>
			</li>
			<li class="">
				<a href="#request" data-toggle="tab" aria-expanded="false"> {{ trans('resisten.Request') }}
				@if(isset($coopRequest) && count($coopRequest) > 0) <span class="label label-resisten">{{ count($coopRequest) }}</span> @endif </a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="orders">
				@if(count($orders) > 0)
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>{{ trans('resisten.No') }}</th>
										<th>{{ trans('resisten.Item') }}</th>
										<th>{{ trans('resisten.Amount') }}</th>
										<th>{{ trans('resisten.Options') }}</th>
									</tr>
								</thead>
								<tbody>
									@php $n=1; @endphp
									@foreach($orders as $order)

									<tr>
										<td>{{ $n }}</td>
										<td>{{ $order->product->name }}</td>
										<td>{{ 'Rp. ' . number_format( $order->grand_total, 2, ',', '.') }}</td>
										<td><a href="{{ action('OrdersController@show', [$order->id]) }}" class="btn btn-resisten">{{ trans('resisten.Detail') }}</a></td>
									</tr>

									@php $n++; @endphp
									@endforeach

								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					{{ $orders->links() }}
				</div>
				@endif
			</div>
			<!-- /.tab-pane -->
			<div class="tab-pane" id="request">
				@if(count($coopRequest) > 0)
				@foreach($coopRequest as $coop)
				<div class="supplier-list clearfix">
					<div class="row">
						@php

						$propict = !is_null($coop->requestant_user->profile_picture) ? url('show_image/'.$coop->requestant_user->profile_picture) : 'http://placehold.it/160x160';

						@endphp
						<div class="col-sm-8 col-xs-6">
							<div class="user-block">
								<a href="{{ action('RelationsController@show', [$coop->requestant_user->id]) }}"> <img class="img-circle" src="{{ $propict }}" alt="User Image"> </a>
								<span class="username"> <a href="{{ action('RelationsController@show', [$coop->requestant_user->id]) }}"> {{ $coop->requestant_user->name }} </a> </span>
							</div>
						</div>
						<div class="col-sm-4 col-xs-6">

							<form action="{{ action('RelationsController@reject') }}" method="POST">
								{{ csrf_field() }}
								<input type="hidden" name="requestant" value="{{ $coop->requestant_user->id }}" />
								<button class="btn btn-danger pull-right reject-btn" type="submit">
									{{ strtoupper(trans('resisten.Reject')) }}
								</button>
							</form>

							<form action="{{ action('RelationsController@accept') }}" method="POST">
								{{ csrf_field() }}
								<input type="hidden" name="requestant" value="{{ $coop->requestant_user->id }}" />
								<button class="btn btn-success pull-right" type="submit">
									{{ strtoupper(trans('resisten.Accept')) }}
								</button>
							</form>

						</div>
					</div>
				</div>
				@endforeach
				@endif
			</div>
			<!-- /.tab-pane -->
		</div>
		<!-- /.tab-content -->
	</div>

</section>
<!-- /.content -->
@endsection
