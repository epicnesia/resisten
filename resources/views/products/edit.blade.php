@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Edit Product') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->

	<div class="row">
		<div class="col-md-12">
			<div class="box box-resisten">
				<form action="{{ action('ProductsController@update', [$product->id]) }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
					<input type="hidden" name="_method" value="PUT" />
					{{ csrf_field() }}
					<div class="box-body">
						<div class="form-group{{ $errors->has('name') || $errors->has('stock') ? ' has-error' : '' }}">
							<label for="name" class="col-sm-2 control-label">{{ trans('resisten.Name') }}</label>

							<div class="col-sm-6">
								<input name="name" type="text" value="{{ old('name', $product->name) }}" placeholder="{{ trans('resisten.Name') }}" class="form-control" id="name">
								@if ($errors->has('name'))
								<span class="help-block"> <strong>{{ $errors->first('name') }}</strong> </span>
								@endif
							</div>

							<label for="stock" class="col-sm-1 control-label">{{ trans('resisten.Stock') }}</label>

							<div class="col-sm-3">
								<input name="stock" type="number" min="1" value="{{ old('stock', $product->stock) }}" placeholder="{{ trans('resisten.Stock') }}" class="form-control" id="stock">
								@if ($errors->has('stock'))
								<span class="help-block"> <strong>{{ $errors->first('stock') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
							<label for="price" class="col-sm-2 control-label">{{ trans('resisten.Price') }}</label>

							<div class="col-sm-10">
								<input name="price" type="number" min="1" value="{{ old('price', $product->price) }}" placeholder="{{ trans('resisten.Price') }}" class="form-control" id="price">
								@if ($errors->has('price'))
								<span class="help-block"> <strong>{{ $errors->first('price') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('commision') ? ' has-error' : '' }}">
							<label for="commision" class="col-sm-2 control-label">{{ trans('resisten.Commision') }}</label>

							<div class="col-sm-10">
								<input name="commision" type="number" min="1" value="{{ old('commision', $product->commision) }}" placeholder="{{ trans('resisten.Commision') }}" class="form-control" id="price">
								@if ($errors->has('commision'))
								<span class="help-block"> <strong>{{ $errors->first('commision') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('weight') ? ' has-error' : '' }}">
							<label for="weight" class="col-sm-2 control-label">{{ trans('resisten.Weight (Gram)') }}</label>

							<div class="col-sm-10">
								<input name="weight" type="number" min="1" value="{{ old('weight', $product->weight) }}" placeholder="{{ trans('resisten.Weight (Gram)') }}" class="form-control" id="price">
								@if ($errors->has('weight'))
								<span class="help-block"> <strong>{{ $errors->first('weight') }}</strong> </span>
								@endif
							</div>
						</div>
						<div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
							<label for="description" class="col-sm-2 control-label">{{ trans('resisten.Description') }}</label>

							<div class="col-sm-10">
								<textarea name="description" class="form-control summernote" id="description" placeholder="{{ trans('resisten.Description') }}">
									{{ old('description', $product->description) }}
								</textarea>								
								@if ($errors->has('description'))
 									<span class="help-block"> <strong>{{ $errors->first('description') }}</strong> </span>
								@endif
							</div>
						</div>
						@if(count($terms) > 0)
						<div class="form-group{{ $errors->has('showcase_categories') ? ' has-error' : '' }}">
							<label for="showcase_categories" class="col-sm-2 control-label">{{ trans('resisten.Showcase Categories') }}</label>

							<div class="col-sm-10">
								<select multiple name="showcase_categories[]" class="form-control">
									@foreach($terms as $term)
									<option value="{{ $term->id }}"{{ old('showcase_categories', $product_categories) !== null && in_array($term->id, old('showcase_categories', $product_categories)) ? ' selected' : '' }}>{{ $term->name }}</option>
									@endforeach
								</select>
								<p class="help-block">{{ trans('resisten.Hold ctrl or shift button to select multiple categories.') }}</p>
								@if ($errors->has('showcase_categories'))
 									<span class="help-block"> <strong>{{ $errors->first('showcase_categories') }}</strong> </span>
								@endif
							</div>
						</div>
						@endif
						<div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
							<label for="image" class="col-sm-2 control-label">{{ trans('resisten.Image') }}</label>

							<div class="col-sm-10">
								<div id="upload_collections" >
									<span class="btn btn-resisten"><i class="fa fa-plus"></i> {{ trans('resisten.Add Image') }}<span>
								</div>
								<div id="status_collections"><div id="progressbar" style="display: none;"></div></div>
									
								<ul id="files_collections" class="filesList list-unstyled clearfix" style="padding: 15px 0 0; margin: 0 -15px;">
									@if(!empty(old('images', json_decode($product->image))))
                                		@php $i = 0; @endphp
                                		@foreach(old('images', json_decode($product->image)) as $image)
                                		
                                		<li class="col-sm-4 imgCol{{ $i }}" style="margin-bottom:15px;">
	                                		<img class="img-responsive" src="{{ url('show_temp/'.Auth::id().'/'.$image->filename.'.'.$image->extension) }}" />
	                                		<input id="fileHidden{{$i}}" type="hidden" name="images[]" value="{{$image->filename.'.'.$image->extension}}" />
	                                		<a class="btn btn-danger remove-image" onclick="delete_image_collections({{$i}})"><i class="fa fa-times"></i></a>
	                                	</li>
                                		
                                		@php $i++; @endphp
                                		@endforeach
                                		
                                	@endif
								</ul>
	                                
								<p class="help-block">{{ trans('resisten.You can upload multiple images and drag them to re-order the position') }}</p>
							</div>
						</div>
						<div class="form-group{{ $errors->has('show_to') ? ' has-error' : '' }}">
							<label for="show_to" class="col-sm-2 control-label">{{ trans('resisten.Post For') }}</label>

							<div class="col-sm-10">
								<select name="show_to" class="form-control">
									<option value="1"{{ old('show_to', $product->show_to) == '1' ? ' selected' : '' }}>{{ trans('resisten.Reseller') }}</option>
									<option value="2"{{ old('show_to', $product->show_to) == '2' ? ' selected' : '' }}>{{ trans('resisten.Supplier') }}</option>
									<option value="3"{{ old('show_to', $product->show_to) == '3' ? ' selected' : '' }}>{{ trans('resisten.All') }}</option>
								</select>
								@if ($errors->has('show_to'))
								<span class="help-block"> <strong>{{ $errors->first('show_to') }}</strong> </span>
								@endif
							</div>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<button type="submit" class="btn btn-resisten pull-right">
							{{ trans('resisten.Edit') }}
						</button>
					</div>
					<!-- /.box-footer -->
				</form>
			</div>
		</div>
	</div>

</section>
<!-- /.content -->
@endsection
