@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Favorites') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->

	@if($products)
	<div class="row">
		@php
			$n=1;
		@endphp
		@foreach($products as $product)

			@php
				$supplier = $product->supplier;
			@endphp

			@if(!is_null($product->supplier->subdistrict_id) && (count($supplier->banks) > 0))

			<div class="col-sm-3">
				<div class="box box-widget widget-user-2">
					<div class="widget-user-header">
						<a href="{{ action('RelationsController@show', [$product->supplier->id]) }}" class="no-style">
							<div class="widget-user-image">
								@if($product->supplier->profile_picture)
									@foreach(json_decode($product->supplier->profile_picture) as $profile_picture)
									<img class="img-circle" src="{{ url($profile_picture->url) }}" alt="{{ $product->supplier->name }} Avatar" />
									@endforeach
								@else
									{!! get_gravatar($product->supplier->email, 160, 'mm', 'g', true, ['class'=>'img-circle', 'alt'=> $product->supplier->name.' Avatar']) !!}
								@endif
							</div>
							<h4 class="widget-user-username">{{ $product->supplier->name }}</h4>
						</a>
						<h5 class="widget-user-desc">{{ $product->created_at->format('d-M-Y H:i:s') }}</h5>
						@if(Auth::id() != $supplier->id)
						<i class="fa {{ in_array($product->id, $favorites) ? 'fa-star fav-active' : 'fa-star-o'}} fa-2x fav-button" data-id="{{ $product->id }}"></i>
						@else
						<a href="{{ action('ProductsController@edit', [$product->id]) }}"><i class="fa fa-edit fa-2x edit-button"></i></a>
						@endif
					</div>
					<div class="box-footer no-padding">
						@if(json_check($product->image))
							<div class="carousel">
								@foreach(json_decode($product->image) as $image)
									<a href="{{ action('ProductsController@show', [$product->id]) }}">
										<img class="img-responsive" src="{{ url($image->url) }}" alt="{{ $product->name }} Image" />
									</a>
								@endforeach
							</div>
						@else
						<img class="img-responsive" src="{{ url($product->image) }}" alt="{{ $product->name }} Image" />
						@endif
					</div>
					<div class="box-footer">
						<h4><a href="{{ action('ProductsController@show', [$product->id]) }}" class="no-style">{{ $product->name }}</a></h4>
						<ul class="nav nav-stacked">
							<li>{{ trans('resisten.Price') }} <span class="pull-right">{{ rupiah_format($product->price) }}</span></li>
					    	<li>{{ trans('resisten.Commision') }} <span class="pull-right">{{ rupiah_format($product->commision) }}</span></li>
						</ul>
					</div>
					@if(Auth::id() != $supplier->id)
					<div class="box-footer">
						<form action="{{ action('ChatsController@store') }}" method="POST" class="pull-right">
							{{ csrf_field() }}
							<input type="hidden" name="receiver" value="{{ $supplier->id }}" />
							<button type="submit" class="btn btn-resisten">
								{{ trans('resisten.Chat') }}
							</button>
							<a href="{{ action('OrdersController@createOrder', [$product->id]) }}" class="btn btn-resisten">{{ trans('resisten.Order') }}</a>
						</form>
					</div>
					@endif
				</div>
			</div>

			@endif

			@php $n++; @endphp
			@if($n%4==1)
				</div>
				<div class="row">
			@endif
		@endforeach
		<div class="col-sm-12">
			{{ $products->links() }}
		</div>
	</div>
	@endif

</section>
<!-- /.content -->
@endsection
