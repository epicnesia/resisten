@extends('layouts.layout_main')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
	<h1> {{ trans('resisten.Products List') }} </h1>
	<div class="breadcrumb">
		<button class="btn btn-resisten btn-sm" onclick="history.back();"><i class="fa fa-arrow-left"></i> {{ trans('resisten.Back') }}</button>
		<button class="btn btn-resisten btn-sm" onclick="history.forward();">{{ trans('resisten.Forward') }} <i class="fa fa-arrow-right"></i></button>
	</div>
</section>

<!-- Main content -->
<section class="content">

	<!-- Notifications -->
	@section('notifications')
	@include('layouts.notifications')
	@show
	<!-- ./ notifications -->

	@if($products)
	<div class="row">
		<div class="col-lg-2">
			<div class="box box-default">
				<div class="box-header with-border">
					<h3 class="box-title">{{ trans('resisten.Categories') }}</h3>
					<a href="{{ action('TermsController@addShowcaseCategories') }}"><i class="fa fa-plus add-button"></i></a>
				</div>
				<div class="box-footer">
					<ul class="nav nav-stacked">
						<li><a href="{{ action('ProductsController@index') }}" class="no-padding">{{ trans('resisten.All Products') }}</a></li>
						@foreach($showcase_categories as $category)
						<li><a href="{{ route('category', ['slug'=>$category->slug]) }}" class="no-padding capitalize">{{ $category->name }}</a></li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
		<div class="col-lg-10">
			<div class="row">
				@foreach($products as $product)
					<div class="col-lg-3 col-md-4 col-sm-6">
						<div class="box box-widget widget-user-2">
							<div class="widget-user-header">
								<div class="widget-user-image">
							      	@if($product->supplier->profile_picture)
										@foreach(json_decode($product->supplier->profile_picture) as $profile_picture)
										<img class="img-circle" src="{{ url($profile_picture->url) }}" alt="{{ $product->supplier->name }} Avatar" />
										@endforeach
									@else
										{!! get_gravatar($product->supplier->email, 160, 'mm', 'g', true, ['class'=>'img-circle', 'alt'=> $product->supplier->name.' Avatar']) !!}
									@endif
							   	</div>
							  	<h4 class="widget-user-username">{{ $product->supplier->name }}</h4>
								<h5 class="widget-user-desc">{{ $product->created_at->format('d-M-Y H:i:s') }}</h5>
							</div>
							<div class="box-footer no-padding">
								@if(json_check($product->image))
									<div class="carousel">
										@foreach(json_decode($product->image) as $image)
											<img class="img-responsive" src="{{ url($image->url) }}" alt="{{ $product->name }} Image" />
										@endforeach
									</div>
								@else
								<img class="img-responsive" src="{{ url($product->image) }}" alt="{{ $product->name }} Image" />
								@endif
							</div>
							<div class="box-footer">
								<h4>{{ $product->name }}</h4>
								<ul class="nav nav-stacked">
						           	<li>{{ trans('resisten.Price') }} <span class="pull-right">{{ rupiah_format($product->price) }}</span></li>
						      		<li>{{ trans('resisten.Commision') }} <span class="pull-right">{{ rupiah_format($product->commision) }}</span></li>
						           	@php
						          		$show_to = trans('resisten.All');

						                if($product->show_to == 1){
						                	$show_to = trans('resisten.Reseller');
						                } elseif ($product->show_to == 2) {
						                	$show_to = trans('resisten.Supplier');
						                }

						            @endphp
						        	<li>{{ trans('resisten.Show to') }} <span class="pull-right">{{ $show_to }}</span></li>
						      	</ul>
							</div>
							<div class="box-footer">
								<a href="{{ action('ProductsController@edit', [$product->id]) }}" class="btn btn-resisten pull-right"><i class="fa fa-edit fa-lg"></i> {{ trans('resisten.Edit') }}</a>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>
	<div class="col-sm-12">
		{{ $products->links() }}
	</div>
	@endif

	<!-- Float Action Button -->
	<a class="add-btn" href="{{ action('ProductsController@create') }}">
		<i class="fa fa-plus"></i>
	</a>
	<!-- End Float Action Button -->

</section>
<!-- /.content -->
@endsection
