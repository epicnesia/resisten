<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>{{ config('app.name', 'Laravel') }}</title>

		<!-- Styles -->
		<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ asset('css/jquery-ui.css') }}" rel="stylesheet">
		<link href="{{ asset('css/AdminLTE.min.css') }}" rel="stylesheet">
		<link href="{{ asset('css/skin-resisten.css') }}" rel="stylesheet">
		<link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
		<link href="{{ asset('plugins/summernote/summernote.css') }}" rel="stylesheet">
		<link href="{{ asset('plugins/circliful/jquery.circliful.css') }}" rel="stylesheet">
		<link href="{{ asset('plugins/slick/slick.css') }}" rel="stylesheet">
		<link href="{{ asset('plugins/slick/slick-theme.css') }}" rel="stylesheet">
		<link href="{{ asset('css/style.css') }}" rel="stylesheet">

		<!-- Scripts -->
		<script>
			window.Laravel = {!! json_encode([
			'csrfToken' => csrf_token(),
			]) !!};
		</script>
	</head>
	<body class="hold-transition skin-resisten sidebar-mini sidebar-collapse">

		@if(Request::path() == 'products/new_products')
		<!-- Search Form -->
		<div class="container-fluid resisten-search-form" style="display:none;">
			<form action="{{ action('ProductsController@search') }}" method="GET">
				<div class="input-group input-group-md">
					<input type="text" name="s" class="form-control" placeholder="{{ trans('resisten.Type here to search product..') }}" autocomplete="off">
					<span class="input-group-btn">
						<button type="submit" class="btn btn-resisten btn-flat">
							<i class="fa fa-search fa-lg"></i>
						</button>
					</span>
				</div>
			</form>
		</div>
		<!-- End Search Form -->
		@endif

		<div class="wrapper">

			@section('navbar')
			@include('layouts.navbar')
			@show

			@section('sidebar')
			@include('layouts.sidebar')
			@show

			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">

				@yield('content')

				<!-- /.content-wrapper -->
			</div>

			@section('footer')
			@include('layouts.footer')
			@show
		</div>

		<!-- Scripts -->
		<script src="{{ asset('js/app.js') }}"></script>
		<script src="{{ asset('js/jquery-ui.js') }}"></script>
		<script src="{{ asset('js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('js/AdminLTE.min.js') }}"></script>
		<script src="{{ asset('plugins/summernote/summernote.min.js') }}"></script>
		<script src="{{ asset('plugins/circliful/jquery.circliful.min.js') }}"></script>
		<script src="{{ asset('plugins/slick/slick.min.js') }}"></script>
		<script src="{{ asset('plugins/AjaxUpload/ajaxupload.3.5.js') }}"></script>
		<script src="{{ asset('plugins/jScroll/jquery.jscroll.js') }}"></script>
		<script src="{{ asset('js/functions.js') }}"></script>
		<script type="text/javascript">

			$('#shipping_province_id').ready(function(){

				getShippingCities();

			});

			$('#shipping_province_id').change(function(){

				$('#shipping_service_code').html('');
				$('#shipping_agents_id').val('0');
				getShippingCities();

			});

			$('#shipping_city_id').change(function(){

				$('#shipping_agents_id').val('0');
				$('#shipping_service_code').html('');
				$('#shipping_agents_id').val('0');
				getShippingSubdistricts();

			});

			$('#shipping_subdistrict_id').change(function(){

				$('#shipping_service_code').html('');
				$('#shipping_agents_id').val('0');
				$('#shipping_subdistrict_name').val($('#shipping_subdistrict_id option:selected').text());

			});

			$('#shipping_agents_id').change(function(){

				getShippingServices();

			});

			$('#count').keyup(function(){

				$('.prevTotalCount').html($('#count').val());
				@if(isset($product->weight))
					var tWeight = $('#count').val() * {{ $product->weight }};
					$('.prevTotalWeight').html(tWeight + ' g');
					$('.jSession input[name=total_weight]').val(tWeight);
				@endif
				$('#shipping_service_code').html('');
				$('#shipping_agents_id').val('0');

			});

			$('#count').ready(function(){
				$('.prevTotalCount').html({{ old('count', '0') }});
				$('.prevTotalWeight').html({{ old('total_weight', '0') }} + ' g');
				$('.prevTotalShippingCost').html(toRp({{ old('shipping_cost', '0') }}));
				$('.prevTotalPrice').html(toRp({{ old('total_price', '0') }}));
				$('.prevGrandTotal').html(toRp({{ old('grand_total', '0') }}));
			});

			$('#shipping_service_code').change(function(){
				$('.prevTotalCount').html($('#count').val());
				getTotals();
			});

			function getShippingCities(){
				$('#shipping_province_name').val($('#shipping_province_id option:selected').text());
				$('#shipping_city_name').val('');
				$('#shipping_subdistrict_name').val('');

				$('#shipping_city_id').html('');

				if($('#shipping_province_id option:selected').val() > 0){
					$("#loadingModal").addClass("loading");
					$.ajax({
	                    type: "POST",
	                    url: "{{ action('OrdersController@getCities') }}",
	                    data: { province : $('#shipping_province_id option:selected').val(), @if(old('shipping_city_id')) selected : {{ old('shipping_city_id') }} @endif },
	                    success: function(r){
							$('#shipping_city_id').html(r);
	                    },
	                    complete: function(r) {
	                    	$("#loadingModal").removeClass("loading");
	                    	getShippingSubdistricts();
	                    }
		           });
				}
			}

			function getShippingSubdistricts(){
				$('#shipping_city_name').val($('#shipping_city_id option:selected').text());
				$('#shipping_subdistrict_name').val('');

				$('#shipping_subdistrict_id').html('');

				if($('#shipping_city_id option:selected').val() > 0){
					$("#loadingModal").addClass("loading");
					$.ajax({
	                    type: "POST",
	                    url: "{{ action('OrdersController@getSubdistrict') }}",
	                    data: { city : $('#shipping_city_id option:selected').val(), @if(old('shipping_subdistrict_id')) selected : {{ old('shipping_subdistrict_id') }} @endif },
	                    success: function(r){
	                    	$("#loadingModal").removeClass("loading");
							$('#shipping_subdistrict_id').html(r);
	                    },
	                    complete: function(r) {
	                    	$('#shipping_subdistrict_name').val($('#shipping_subdistrict_id option:selected').text());
	                    }
		           });
				}
			}

			function getShippingServices(){

				$('#shipping_service_code').html('');

				if($('#shipping_agents_id').val() != ''){

					if($('#shipping_subdistrict_id').val() > 0){

						$("#loadingModal").addClass("loading");

						var mOrigin			= 0;
						var mDestination 	= $('#shipping_subdistrict_id').val();

						@if(isset($product->supplier->subdistrict_id))
							mOrigin 	= '{{ $product->supplier->subdistrict_id }}';
						@endif

						if($('#count').val() > 0){

							var mWeight		= @if(isset($product->weight)) $('#count').val() * {{ $product->weight }}; @endif

							$.ajax({
			                    type: "POST",
			                    url: "{{ action('OrdersController@getShippingServices') }}",
			                    data: { origin 		: mOrigin,
			                    		destination : mDestination,
			                    		weight 		: mWeight,
			                    		courier 	: $('#shipping_agents_id > option:selected').data('code'),
			                    	  },
			                    success: function(r){
			                    	$('#shipping_service_code').html(r);
			                    	$("#loadingModal").removeClass("loading");
			                    },
			                    complete: function(r) {
			                    	getTotals();
			                    }
				           });

						} else {
							$('#shipping_agents_id').val('0');
							$("#loadingModal").removeClass("loading");
							alert("{{ trans('resisten.Please input count first!') }}");
						}

					} else {
						$('#shipping_agents_id').val('0');
						$("#loadingModal").removeClass("loading");
						alert("{{ trans('resisten.You must select subdistrict first!') }}");
					}

				}

			}

			function getTotals(){
				var shippingCost= $('#shipping_service_code option:selected').data('cost');
				var tPrice		= 0;
				@if(!empty($product->price))
					tPrice		= $('#count').val() * {{ $product->price }};
				@endif
				var grandTotal	= shippingCost + tPrice;

				$('.prevTotalShippingCost').html(toRp(shippingCost));
				$('.jSession input[name=shipping_cost]').val(shippingCost);

				$('.prevTotalPrice').html(toRp(tPrice));
				$('.jSession input[name=total_price]').val(tPrice);

				$('.prevGrandTotal').html(toRp(grandTotal));
				$('.jSession input[name=grand_total]').val(grandTotal);
			}

			function toRp(a,b,c,d,e){e=function(f){return f.split('').reverse().join('')};b=e(parseInt(a,10).toString());for(c=0,d='';c<b.length;c++){d+=b[c];if((c+1)%3===0&&c!==(b.length-1)){d+='.';}}return'Rp.\t'+e(d)+',00'}

/*=============================================================================================================================================
 * Profiles
 * ============================================================================================================================================
 */


			$('#province_id').change(function(){

				getCities();

			});

			$('#city_id').change(function(){

				getSubdistricts();

			});

			$('#subdistrict_id').ready(function(){

				$('#subdistrict_name').val($('#subdistrict_id option:selected').text());

			});

			$('#subdistrict_id').change(function(){

				$('#subdistrict_name').val($('#subdistrict_id option:selected').text());

			});

			function getCities(){
				$('#province_name').val($('#province_id option:selected').text());
				$('#city_name').val('');
				$('#subdistrict_name').val('');

				$('#city_id').html('');

				if($('#province_id option:selected').val() > 0){
					$("#loadingModal").addClass("loading");
					$.ajax({
	                    type: "POST",
	                    url: "{{ action('OrdersController@getCities') }}",
	                    data: { province : $('#province_id option:selected').val(), @if(old('city_id')) selected : {{ old('city_id') }} @endif },
	                    success: function(r){
	                    	$("#loadingModal").removeClass("loading");
							$('#city_id').html(r);
	                    },
	                    complete: function(r) {
	                    	getSubdistricts();
	                    }
		           });
				}
			}

			function getSubdistricts(){
				$('#city_name').val($('#city_id option:selected').text());
				$('#subdistrict_name').val('');

				$('#subdistrict_id').html('');

				if($('#city_id option:selected').val() > 0){
					$("#loadingModal").addClass("loading");
					$.ajax({
	                    type: "POST",
	                    url: "{{ action('OrdersController@getSubdistrict') }}",
	                    data: { city : $('#city_id option:selected').val(), @if(old('subdistrict_id')) selected : {{ old('subdistrict_id') }} @endif },
	                    success: function(r){
	                    	$("#loadingModal").removeClass("loading");
							$('#subdistrict_id').html(r);
	                    }
		           });
				}
			}

/*=============================================================================================================================================
 * Favorite Button
 * ============================================================================================================================================
 */
			$('.fav-button').click(function(e){

				e.preventDefault();
				if($(this).hasClass('fa-star-o')){
					$(this).removeClass('fa-star-o');
					$(this).addClass('fa-star fav-active');
				} else {
					$(this).addClass('fa-star-o');
					$(this).removeClass('fa-star fav-active');
				}

				$.ajax({
					type: "POST",
					url: "{{ action('ProductsController@updateFavorites') }}",
					data: { product : $(this).data('id') }
				});

			});

/*=============================================================================================================================================
 * Chats
 * ============================================================================================================================================
 */

		var lastMessage;
		$('#chat-box').ready(function(e){
			setInterval(updateChat, 2000);
		});

		$('#chat-form').submit(function(e){

			e.preventDefault();

			@if(!empty($chat))

				if($('input[name=message]').val() != ''){
					$.ajax({
						type: "POST",
						url: $('#chat-form').attr('action'),
						data: {
							chat_id: $('input[name=chat_id]').val(),
							user_id: $('input[name=user_id]').val(),
							message: $('input[name=message]').val(),
						},
						success: function(data){
							$('input[name=message]').val('');
						}
					});
				}

			@endif
		});

		function updateChat()
		{
			@if(!empty($chat))
				$.ajax({
					type: "POST",
					url: "{{ action('ChatsController@getChatRoom', [$chat->id]) }}",
					data: {
						last: lastMessage,
					},
					dataType: "json",
					success: function(data){
						if(data.message.length > 0){
							$.each(data.message, function(index, element) {

								var pos		= '',
									namePos = 'pull-left',
									timePos	= 'pull-right';
								if({{ Auth::id() }} == element.user_id){
									pos 	= 'right';
									namePos = 'pull-right';
									timePos	= 'pull-left';
								}

								var propict = '{!! get_gravatar(' + element.user.email + ', 160, 'mm', 'g', true, ['class'=>'direct-chat-img', 'alt'=> "' + element.user.name + ' Image"]) !!}';
								if(element.user.profile_picture != null){
									var json = $.parseJSON(element.user.profile_picture);
									$(json).each(function(i,val) {
										propict = '<img class="direct-chat-img" src="{{ url('/') }}' + val.url + '" alt="' + element.user.name + ' Image">';
									});
								}

								var divData = $('<div class="direct-chat-msg ' + pos +'">' +
													'<div class="direct-chat-info clearfix">' +
														'<span class="direct-chat-name ' + namePos + '">' + element.user.name + '</span>' +
														'<span class="direct-chat-timestamp ' + timePos + '">' + element.created_at + '</span>' +
													'</div>' + propict +
													'<div class="direct-chat-text">' + element.message + '</div>' +
												'</div>');

								$('#chat-box').append(divData);
								if(document.getElementById('chat-box')){
									document.getElementById('chat-box').scrollTop = document.getElementById('chat-box').scrollHeight;
								}
								lastMessage = element.id;
							});
						}
					},
				});
			@endif
		}

/*=============================================================================================================================================
 * Upload Images
 * ============================================================================================================================================
 */

	$("#files_collections").sortable();
	$("#files_collections").disableSelection();
	$("#datepicker").datepicker();
	$("#progressbar").progressbar({
		value: false
	});

	if(document.getElementById('upload_collections')){
		var btnUpload = $('#upload_collections');
		var i = {{isset($product->image)&&!empty($product->image)?count(json_decode($product->image))-1:'0'}};
		new AjaxUpload(btnUpload, {
			action : "{{ action('ProductsController@uploadImage') }}",
			name : 'file',
			onSubmit : function(file, ext) {
				if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext))) {
					// extension is not allowed
					status.text('Only JPG, PNG or GIF files are allowed');
					return false;
				}
				$("#progressbar").show();
				i++;
			},
			onComplete : function(file, response) {
				$("#progressbar").hide();
				//Add uploaded file to list
				if (response === "") {
					$('<li></li>').appendTo('#files_collections').text(response).addClass('bg-danger');
				} else {
					$('<li class="col-sm-4" style="margin-bottom:15px;"></li>').appendTo('#files_collections').html('<img class="img-responsive" src="{{ url("show_temp/".Auth::id()."/") }}/' + response + '" /><input id="fileHidden' + i + '" type="hidden" name="images[]" value="' + response + '" /><a class="btn btn-danger remove-image" onclick="delete_image_collections(' + i + ')"><i class="fa fa-times"></i></a>').addClass('imgCol' + i + '');
				}
			}
		});

		function delete_image_collections(i) {
			var status = confirm("Are you sure you want to delete? it will permanently deleted..!!");
			if (status == true) {
				var file = $('#fileHidden' + i + '').val();
				$.ajax({
					type : 'POST',
					url : "{{ action('ProductsController@deleteImage') }}",
					data : {
						file : file
					}
				}).done(function() {
					$('li.imgCol' + i + '').remove();
				});
			}
		}
	}

		</script>
	</body>
</html>
