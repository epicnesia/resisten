<!-- Sidebar Menu -->
<ul class="sidebar-menu">
	<li class="header" style="text-transform: uppercase;">
		{{ trans('resisten.Admin Menu')}}
	</li>
	<!-- Optionally, you can add icons to the links -->
	<li>
		<a href="{{ action('UsersManagerController@index') }}"><i class="fa fa-users fa-lg"></i> <span>{{ trans('resisten.Users Manager') }}</span></a>
	</li>
	<li>
		<a href="{{ action('ProductsManagerController@index') }}"><i class="fa fa-cubes fa-lg"></i> <span>{{ trans('resisten.Products Manager') }}</span></a>
	</li>
	<li>
		<a href="{{ action('OrdersManagerController@index') }}"><i class="fa fa-shopping-cart fa-lg"></i> <span>{{ trans('resisten.Orders Manager') }}</span></a>
	</li>
	<li>
		<a href="{{ action('ShippingAgentsController@index') }}"><i class="fa fa-truck fa-lg"></i> <span>{{ trans('resisten.Shipping Agents') }}</span></a>
	</li>
	<li>
		<a href="{{ action('SettingsController@profile') }}"><i class="fa fa-user"></i> <span>{{ trans('resisten.Edit Profile') }}</span></a>
	</li>
</ul>
<!-- /.sidebar-menu -->