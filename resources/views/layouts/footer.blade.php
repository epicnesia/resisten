<!-- Main Footer -->
<footer class="main-footer">
	<!-- To the right -->
	<div class="pull-right hidden-xs">
		Anything you want
	</div>
	<!-- Default to the left -->
	<strong>Copyright &copy; 2017 <a href="{{ url('/') }}">{{ env('APP_NAME') }}</a>.</strong> All rights reserved.
</footer>