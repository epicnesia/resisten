<!-- Main Header -->
<header class="main-header">

	@if (Auth::guest())
	<nav class="navbar navbar-static-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<a href="{{ url('/') }}" class="navbar-brand"><strong>{{ config('app.name', 'Laravel') }}</strong>.id</a>
			</div>

			<div class="collapse navbar-collapse pull-left" id="navbar-collapse">
				<ul class="nav navbar-nav"></ul>
			</div>

			<div class="navbar-custom-menu">
				<ul class="nav navbar-nav">
					<li>
						<a href="{{ route('login') }}">{{ trans('resisten.Login') }}</a>
					</li>
					<li>
						<a href="{{ route('register') }}">{{ trans('resisten.Register') }}</a>
					</li>
				</ul>
			</div>
		</div>
		<!-- /.container-fluid -->
	</nav>
	@else
	<!-- Logo -->
	<a href="{{ url('/dashboard') }}" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels --> <span class="logo-mini"><strong>R</strong>.id</span> <!-- logo for regular state and mobile devices --> <span class="logo-lg"><strong>{{ config('app.name', 'Laravel') }}</strong>.id</span> </a>

	<!-- Header Navbar -->
	<nav class="navbar navbar-static-top" role="navigation">
		<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> </a>

		<!-- Navbar Right Menu -->
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<li>
					<a href="{{ route('logout') }}"
					onclick="event.preventDefault();
					document.getElementById('logout-form').submit();"> {{ trans('resisten.Logout') }} </a>

					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						{{ csrf_field() }}
					</form>
				</li>
			</ul>
		</div>
	</nav>
	@endif
</header>