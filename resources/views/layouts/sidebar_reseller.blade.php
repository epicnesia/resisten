<!-- Sidebar Menu -->
<ul class="sidebar-menu">
	<li class="header" style="text-transform: uppercase;">
		{{ trans('resisten.Reseller Menu')}}
	</li>
	<!-- Optionally, you can add icons to the links -->
	<li>
		<a href="{{ action('ProductsController@newProducts') }}"><i class="fa fa-heart fa-lg"></i> <span>{{ trans('resisten.New Products') }}</span></a>
	</li>
	<li>
		<a href="{{ action('ChatsController@index') }}"><i class="fa fa-envelope fa-lg"></i> <span>{{ trans('resisten.Messages') }}</span></a>
	</li>
	<li>
		<a href="{{ action('OrdersController@index') }}"><i class="fa fa-shopping-cart fa-lg"></i> <span>{{ trans('resisten.Orders') }}</span></a>
	</li>
	<li>
		<a href="{{ action('ProductsController@favorites') }}"><i class="fa fa-star fa-lg"></i> <span>{{ trans('resisten.Favorites') }}</span></a>
	</li>
	<li>
		<a href="{{ action('RelationsController@supplierList') }}"><i class="fa fa-users fa-lg"></i> <span>{{ trans('resisten.Suppliers') }}</span></a>
	</li>
	<li>
		<a href="{{ action('OrdersController@paymentList') }}"><i class="fa fa-credit-card-alt fa-lg"></i> <span>{{ trans('resisten.Confirm Payment') }}</span></a>
	</li>
	<li>
		<a href="{{ action('NotesController@index') }}"><i class="fa fa-sticky-note fa-lg"></i> <span>{{ trans('resisten.Notes') }}</span></a>
	</li>
	<li class="treeview">
		<a href="#"> 
			<i class="fa fa-gear fa-lg"></i> 
			<span>Settings</span> 
			<span class="pull-right-container"> <i class="fa fa-angle-left fa-lg pull-right"></i> </span> 
		</a>
		<ul class="treeview-menu" style="display: none;">
			<li>
				<a href="{{ action('SettingsController@profile') }}"><i class="fa fa-circle-o"></i> {{ trans('resisten.Profile') }}</a>
			</li>
			<li>
				<a href="{{ action('SettingsController@banks') }}"><i class="fa fa-circle-o"></i> {{ trans('resisten.Banks') }}</a>
			</li>
		</ul>
	</li>
</ul>
<!-- /.sidebar-menu -->