<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>{{ config('app.name', 'Laravel') }}</title>

		<!-- Styles -->
		<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
		<link href="{{ asset('css/AdminLTE.min.css') }}" rel="stylesheet">
		<link href="{{ asset('css/skin-resisten.css') }}" rel="stylesheet">
		<link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

		<!-- Scripts -->
		<script>
			window.Laravel = {!! json_encode([
			'csrfToken' => csrf_token(),
			]) !!};
		</script>
	</head>
	<body class="hold-transition skin-resisten layout-top-nav">
		<div class="wrapper">

			@section('navbar')
			@include('layouts.navbar')
			@show

			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper" style="padding-top:50px;">

				@yield('content')

				<!-- /.content-wrapper -->
			</div>

			@section('footer')
			@include('layouts.footer')
			@show
		</div>

		<!-- Scripts -->
		<script src="{{ asset('js/app.js') }}"></script>
		<script src="{{ asset('js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('js/AdminLTE.min.js') }}"></script>
	</body>
</html>
