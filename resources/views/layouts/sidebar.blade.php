<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">

		<!-- Sidebar user panel (optional) -->
		<div class="user-panel">
			<div class="pull-left image">
				@if(Auth::user()->profile_picture)
					@foreach(json_decode(Auth::user()->profile_picture) as $profile_picture)
						<img class="img-circle" src="{{ url($profile_picture->url) }}" alt="{{ Auth::user()->name }} Avatar" />
					@endforeach
				@else
					{!! get_gravatar(Auth::user()->email, 160, 'mm', 'g', true, ['class'=>'img-circle', 'alt'=> Auth::user()->name.' Avatar']) !!}
				@endif
			</div>
			<div class="pull-left info">
				<p>
					{{ Auth::user()->name }}
				</p>
				<!-- Status -->
				@if(Auth::user()->hasRole('admin'))
					<i class="fa fa-circle text-success"></i> Admin
				@elseif(Auth::user()->hasRole('supplier'))
					<i class="fa fa-circle text-success"></i> Supplier
				@elseif(Auth::user()->hasRole('reseller'))
					<i class="fa fa-circle text-success"></i> Reseller
				@endif
			</div>
		</div>

		@if(Auth::user()->hasRole('admin'))
			@include('layouts.sidebar_admin')
		@elseif(Auth::user()->hasRole('supplier'))
			@include('layouts.sidebar_supplier')
		@elseif(Auth::user()->hasRole('reseller'))
			@include('layouts.sidebar_reseller')
		@endif

	</section>
	<!-- /.sidebar -->
</aside>
