<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | following language lines contain default error messages used by
    | validator class. Some of these rules have multiple versions such
    | as size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute harus disetujui / dicentang.',
    'active_url'           => ':attribute bukanlah URL yang benar.',
    'after'                => ':attribute harus tanggal setelah :date.',
    'after_or_equal'       => ':attribute harus tanggal :date atau setelahnya.',
    'alpha'                => ':attribute harus hanya berisi huruf.',
    'alpha_dash'           => ':attribute harus hanya berisi huruf, angka atau dash (-).',
    'alpha_num'            => ':attribute harus hanya berisi huruf dan angka.',
    'array'                => ':attribute harus data bertype array.',
    'before'               => ':attribute harus tanggal sebelum :date.',
    'before_or_equal'      => ':attribute harus tanggal :date atau sebelumnya.',
    'between'              => [
        'numeric' => ':attribute harus angka diantara :min dan :max.',
        'file'    => ':attribute harus file berukuran min :min dan max :max kilobytes.',
        'string'  => ':attribute harus huruf berjumlah antara min :min dan max :max karakter.',
        'array'   => ':attribute harus array dengan jumlah data antara :min dan :max item.',
    ],
    'boolean'              => ':attribute field ini haruslah bertipe boolean (TRUE or FALSE).',
    'confirmed'            => ':attribute konfirmasi data gagal.',
    'date'                 => ':attribute bukanlah format tanggal yang benar.',
    'date_format'          => ':attribute formatnya harus :format.',
    'different'            => ':attribute dan :other berbeda.',
    'digits'               => ':attribute haruslah :digits digits.',
    'digits_between'       => ':attribute haruslah diantara :min dan :max digit.',
    'dimensions'           => ':attribute dimensi gambar salah.',
    'distinct'             => ':attribute field memiliki nilai duplikat / sama.',
    'email'                => ':attribute haruslah alamat email yang benar.',
    'exists'               => ':attribute tidak ada.',
    'file'                 => ':attribute haruslah sebuah file.',
    'filled'               => ':attribute field harus memiliki nilai.',
    'image'                => ':attribute haruslah sebuah gambar.',
    'in'                   => ':attribute attribute terpilih salah.',
    'in_array'             => ':attribute field tidak ada di :other.',
    'integer'              => ':attribute haruslah sebuah data bertype integer.',
    'ip'                   => ':attribute haruslah alamat IP yang valid.',
    'json'                 => ':attribute haruslah data bertype JSON yang valid.',
    'max'                  => [
        'numeric' => ':attribute tidak boleh lebih dari :max.',
        'file'    => ':attribute tidak boleh lebih dari :max kilobytes.',
        'string'  => ':attribute tidak boleh lebih dari :max karakter.',
        'array'   => ':attribute tidak boleh lebih dari :max items.',
    ],
    'mimes'                => ':attribute haruslah sebuah file dari type: :values.',
    'mimetypes'            => ':attribute haruslah sebuah file dari type: :values.',
    'min'                  => [
        'numeric' => ':attribute minimal :min.',
        'file'    => ':attribute minimal :min kilobytes.',
        'string'  => ':attribute minimal :min karakter.',
        'array'   => ':attribute minimal :min items.',
    ],
    'not_in'               => ':attribute terpilih tidak valid.',
    'numeric'              => ':attribute haruslah sebuah angka.',
    'present'              => ':attribute field harus tersedia.',
    'regex'                => ':attribute format salah.',
    'required'             => ':attribute field wajib diisi.',
    'required_if'          => ':attribute field wajib diisi jika :other adalah :value.',
    'required_unless'      => ':attribute field wajib diisi kecuali :other adalah :values.',
    'required_with'        => ':attribute field wajib diisi ketika :values sudah tersedia.',
    'required_with_all'    => ':attribute field wajib diisi ketika :values sudah tersedia.',
    'required_without'     => ':attribute field wajib diisi ketika :values tidak tersedia.',
    'required_without_all' => ':attribute field wajib diisi apabila tidak ada satupun :values yang tersedia.',
    'same'                 => ':attribute and :other must match.',
    'size'                 => [
        'numeric' => ':attribute harus berukuran :size.',
        'file'    => ':attribute harus berukuran :size kilobytes.',
        'string'  => ':attribute harus berukuran :size karakter.',
        'array'   => ':attribute harus terdiri dari :size item.',
    ],
    'string'               => ':attribute haruslah sebuah string.',
    'timezone'             => ':attribute haruslah sebuah zona yang valid.',
    'unique'               => ':attribute sudah diambil.',
    'uploaded'             => ':attribute gagal untuk mengupload.',
    'url'                  => ':attribute format URL salah.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
