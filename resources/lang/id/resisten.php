<?php

return array (
  '-- Select City --' 									=> '-- Pilih Kota --',
  '-- Select Courier --' 								=> '-- Pilih Agen Pengiriman --',
  '-- Select Province --' 								=> '-- Pilih Provinsi --',
  '-- Select Service --' 								=> '-- Pilih Layanan --',
  '-- Select Subdistrict --' 							=> '-- Pilih Kecamatan --',
  'Accept' 												=> 'Terima',
  'Account Name' 										=> 'Rekening Atas Nama',
  'Account Number' 										=> 'No Rekening',
  'Add' 												=> 'Tambah',
  'Add Bank' 											=> 'Tambah Bank',
  'Add Image' 											=> 'Tambah Gambar',
  'Add New Categories' 									=> 'Tambah Kategori Baru',
  'Add Shipping Agents' 								=> 'Tambah Agen Pengiriman',
  'Add Supplier' 										=> 'Tambah Supplier',
  'Address' 											=> 'Alamat',
  'Admin Menu' 											=> 'Menu Admin',
  'All' 												=> 'Semua',
  'All Products' 										=> 'Semua Produk',
  'Amount' 												=> 'Jumlah',
  'Approved' 											=> 'Disetujui',
  'Are you sure to delete this item?' 					=> 'Apakah Anda yakin akan menghapus item ini?',
  'Back' 												=> 'Back',
  'Bank Name' 											=> 'Nama Bank',
  'Bank already exists' 								=> 'Bank sudah ada',
  'Banks' 												=> 'Bank',
  'Banks List' 											=> 'Daftar Bank',
  'Categories' 											=> 'Kategori',
  'Categories List' 									=> 'Daftar Kategori',
  'Categories created successfully.' 					=> 'Kategori berhasil dibuat.',
  'Categories deleted successfully.' 					=> 'Kategori berhasil dihapus.',
  'Categories updated successfully.' 					=> 'Kategori berhasil diupdate.',
  'Chat' 												=> 'Chat',
  'Chat Box' 											=> 'Chat Box',
  'City' 												=> 'Kota',
  'Click here to edit' 									=> 'Klik di sini untuk mengedit',
  'Click to Disable' 									=> 'Klik untuk Disable',
  'Click to Enable' 									=> 'Klik untuk Enable',
  'Code' 												=> 'Kode',
  'Commision' 											=> 'Komisi',
  'Commisions' 											=> 'Komisi',
  'Confirm Password' 									=> 'Konfirmasi Password',
  'Confirm Payment' 									=> 'Konfirmasi Pembayaran',
  'Confirm Payments List'								=> 'Daftar Konfirmasi Pembayaran',
  'Content' 											=> 'Konten',
  'Count' 												=> 'Jumlah',
  'Courier' 											=> 'Kurir',
  'Create New Note' 									=> 'Buat Catatan Baru',
  'Create New Order' 									=> 'Buat Order Baru',
  'Created At' 											=> 'Terdaftar pada',
  'Dashboard' 											=> 'Dashboard',
  'Data updated succesfully' 							=> 'Data berhasil diupdate',
  'Date' 												=> 'Tanggal',
  'Delete' 												=> 'Hapus',
  'Delivered' 											=> 'Delivered',
  'Description' 										=> 'Deskripsi',
  'Detail' 												=> 'Detail',
  'E-Mail Address' 										=> 'Alamat E-Mail',
  'Edit' 												=> 'Edit',
  'Edit Bank' 											=> 'Edit Bank',
  'Edit Product' 										=> 'Edit Produk',
  'Edit Profile' 										=> 'Edit Profil',
  'Edit Shipping Agents' 								=> 'Edit Agen Pengiriman',
  'Edit Showcase Categories' 							=> 'Edit Kategori Etalase',
  'Edit User Details' 									=> 'Edit Detail User',
  'Email' 												=> 'Email',
  'Favorites' 											=> 'Favorit',
  'Forward' 											=> 'Forward',
  'Grand Total' 										=> 'Grand Total',
  'Hold ctrl or shift button to select multiple categories.'
  														=> 'Tahan tombol ctrl atau shift untuk memilih lebih dari satu kategori.',
  'ID' 													=> 'ID',
  'If you want to change password, please insert your old password!'
  														=> 'Apabila Anda ingin merubah password Anda, harap isi password lama Anda!',
  'Image' 												=> 'Gambar',
  'Images' 												=> 'Gambar',
  'Item' 												=> 'Item',
  'Latest Orders' 										=> 'Order Terbaru',
  'Latest Users' 										=> 'User Terbaru',
  'Login'												=> 'Login',
  'Logout'												=> 'Logout',
  'Messages' 											=> 'Pesan',
  'Name' 												=> 'Nama',
  'New Password' 										=> 'Password Baru',
  'New Products' 										=> 'Product Baru',
  'No' 													=> 'No',
  'No response from shipment provider' 					=> 'Tidak ada respon dari penyedia jasa pengiriman',
  'No string parameter' 								=> 'Tidak ada parameter',
  'Notes' 												=> 'Catatan',
  'Notes deleted' 										=> 'Catatan dihapus',
  'Notes saved' 										=> 'Catatan disimpan',
  'Notifications' 										=> 'Notifikasi',
  'Old Password' 										=> 'Password Lama',
  'Omzet' 												=> 'Omzet',
  'Options' 											=> 'Options',
  'Order'												=> 'Order',
  'Order Detail' 										=> 'Detail Order',
  'Order ID' 											=> 'ID Order',
  'Order Not Found' 									=> 'Order tidak ditemukan.',
  'Order Success' 										=> 'Order Success',
  'Order processed' 									=> 'Order diproses',
  'Order submitted successfully, your order ID:' 		=> 'Order berhasil dibuat, order ID Anda adalah:',
  'Orders' 												=> 'Order',
  'Orders In' 											=> 'Order Masuk',
  'Orders Manager' 										=> 'Manajemen Order',
  'Orders Out' 											=> 'Order Keluar',
  'Pack' 												=> 'Pack',
  'Password' 											=> 'Password',
  'Payment Confirmation Image Inserted Successfully' 	=> 'Bukti transfer pembayaran berhasil diupload',
  'Payment Receipt' 									=> 'Bukti transfer pembayaran',
  'Pending' 											=> 'Pending',
  'Phone' 												=> 'Phone',
  'Please input count first!' 							=> 'Silahkan masukkan jumlah dulu!',
  'Please make the payment by transfer to the following bank accounts'
  														=> 'Silahkan melakukan transfer pembayaran ke rekening berikut',
  'Please select a subdistrict and fill out the bank before start selling, or your product will not appear in the new product list'
  														=> 'Harap melengkapi profil, memilih kecamatan dan menambah bank Anda, atau produk Anda tidak akan ditampilkan di daftar produk baru',
  'Please select min 1 image' 							=> 'Harap memilih minimal 1 gambar',
  'Post' 												=> 'Post',
  'Post For' 											=> 'Post Untuk',
  'Post New Product' 									=> 'Post Produk Baru',
  'Price' 												=> 'Harga',
  'Product Details' 									=> 'Detail Produk',
  'Product Inserted Successfully' 						=> 'Produk berhasil ditambahkan',
  'Product Name' 										=> 'Nama Produk',
  'Product Updated Successfully' 						=> 'Produk berhasil diupdate',
  'Products' 											=> 'Produk',
  'Products List' 										=> 'Daftar Produk',
  'Products Manager' 									=> 'Manajemen Produk',
  'Profile' 											=> 'Profil',
  'Profile Picture' 									=> 'Profile Picture',
  'Province' 											=> 'Provinsi',
  'Purchased' 											=> 'Terbeli',
  'Receipt' 											=> 'Bukti Transfer',
  'Recently Added Products' 							=> 'Produk yang baru saja ditambahkan',
  'Register' 											=> 'Daftar',
  'Register Success' 									=> 'Pendaftaran Berhasil',
  'Reject' 												=> 'Tolak',
  'Request' 											=> 'Permintaan',
  'Request accepted' 									=> 'Permintaan diterima',
  'Request rejected' 									=> 'Permintaan ditolak',
  'Reseller' 											=> 'Reseller',
  'Reseller Menu' 										=> 'Menu Reseller',
  'Resellers' 											=> 'Reseller',
  'Resellers List' 										=> 'Daftar Reseller',
  'Sales' 												=> 'Sales',
  'Save' 												=> 'Simpan',
  'Search' 												=> 'Cari',
  'Search result for' 									=> 'Hasil pencarian untuk',
  'Select City' 										=> 'Pilih Kota',
  'Select Courier' 										=> 'Pilih Agen Pengiriman',
  'Select Province' 									=> 'Pilih Provinsi',
  'Select Service' 										=> 'Pilih Layanan',
  'Select Subdistrict' 									=> 'Pilih Kecamatan',
  'Select User to Send Message' 						=> 'Pilih user untuk dikirimi pesan',
  'Seller Name' 										=> 'Nama Seller',
  'Send' 												=> 'Kirim',
  'Shipment' 											=> 'Pengiriman',
  'Shipments' 											=> 'Pengiriman',
  'Shipping Address' 									=> 'Alamat Pengiriman',
  'Shipping Agent deleted successfully.' 				=> 'Agen Jasa Pengiriman berhasil dihapus.',
  'Shipping Agents' 									=> 'Agen Jasa Pengiriman',
  'Shipping Agents Manager' 							=> 'Manajemen Agen Jasa Pengiriman',
  'Shipping Code' 										=> 'No Resi',
  'Shipping Details' 									=> 'Detail Pengiriman',
  'Shipping Service' 									=> 'Layanan Jasa Pengiriman',
  'Shipping Status' 									=> 'Status Pengiriman',
  'Shipping agent created successfully.' 				=> 'Agen Jasa Pengiriman berhasil dibuat.',
  'Shipping agent updated successfully.' 				=> 'Agen Jasa Pengiriman berhasil dihapus.',
  'Shipping code not found' 							=> 'No Resi tidak ditemukan',
  'Shipping code submitted' 							=> 'No Resi berhasil dimasukkan',
  'Shipping status updated' 							=> 'Status pengiriman berhasil diupdate',
  'Show to' 											=> 'Tunjukan pada',
  'Showcase Categories' 								=> 'Kategori Etalase',
  'Slug' 												=> 'Slug',
  'Sorry, this product stock is not enough!' 			=> 'Maaf, stok produk ini tidak cukup!',
  'Start Chat' 											=> 'Mulai Chat',
  'Status' 												=> 'Status',
  'Stock' 												=> 'Stok',
  'Store Name' 											=> 'Nama Toko',
  'Subdistrict' 										=> 'Kecamatan',
  'Submit' 												=> 'Submit',
  'Supplier' 											=> 'Supplier',
  'Supplier Added Successfully' 						=> 'Supplier Berhasil Ditambahkan',
  'Supplier Menu' 										=> 'Menu Supplier',
  'Suppliers' 											=> 'Supplier',
  'Suppliers List' 										=> 'Daftar Supplier',
  'Target' 												=> 'Target',
  'Target Commisions This Month' 						=> 'Target Komisi Bulan Ini',
  'Target Commisions Today' 							=> 'Target Komisi Hari Ini',
  'Target Omzet This Month' 							=> 'Target Omzet Bulan Ini',
  'Target Omzet Today' 									=> 'Target Omzet Hari Ini',
  'The bank added to your account' 						=> 'Bank berhasil ditambahkan ke akun Anda',
  'The bank deleted successfully' 						=> 'Bank berhasil dihapus',
  'The bank updated successfully' 						=> 'Bank berhasil diupdate',
  'The total amount you have to pay:' 					=> 'Jumlah total yang harus Anda bayar:',
  'This Action is Unauthorized' 						=> 'Anda tidak diperbolehkan mengakses fitur ini',
  'This Month Achievement' 								=> 'Pencapaian Bulan Ini',
  'Title' 												=> 'Judul',
  'Today Achievement' 									=> 'Pencapaian Hari Ini',
  'Top Product' 										=> 'Top Produk',
  'Top Reseller' 										=> 'Top Reseller',
  'Top Supplier' 										=> 'Top Supplier',
  'Total Commisions' 									=> 'Total Komisi',
  'Total Commisions This Month' 						=> 'Total Komisi Bulan Ini',
  'Total Commisions Today' 								=> 'Total Komisi Hari Ini',
  'Total Count' 										=> 'Total Jumlah Barang',
  'Total Delivered Orders' 								=> 'Total Order Sampai',
  'Total New Orders' 									=> 'Total Order Baru',
  'Total Omzet' 										=> 'Total Omzet',
  'Total Omzet This Month' 								=> 'Total Omzet Bulan Ini',
  'Total Omzet Today' 									=> 'Total Omzet Hari Ini',
  'Total On Process Orders' 							=> 'Total Order dalam proses',
  'Total Orders' 										=> 'Total Order',
  'Total Orders In' 									=> 'Total Order Masuk',
  'Total Orders Out' 									=> 'Total Order Keluar',
  'Total Packed Orders' 								=> 'Total Order Dikemas',
  'Total Price' 										=> 'Total Harga',
  'Total Products' 										=> 'Total Produk',
  'Total Purchased Products' 							=> 'Total Produk Dibeli',
  'Total Sales' 										=> 'Total Sales',
  'Total Shipping Cost' 								=> 'Total Ongkos Kirim',
  'Total Users' 										=> 'Total User',
  'Total Weight' 										=> 'Total Berat',
  'Total incoming order' 								=> 'Total order masuk',
  'Total products sold' 								=> 'Total produk terjual',
  'Type' 												=> 'Tipe',
  'Type here to search' 								=> 'Ketik disini untuk mencari',
  'Type here to search product..' 						=> 'Ketik disini untuk mencari produk..',
  'Update Status' 										=> 'Update Status',
  'User Details' 										=> 'Detail User',
  'User deleted successfully!' 							=> 'User berhasil dihapus!',
  'User Not Found' 										=> 'User Tidak Ditemukan.',
  'User\'s profile updated successfully' 				=> 'Profil User berhasil diupdate',
  'Users Manager' 										=> 'Manajemen User',
  'Weight' 												=> 'Berat',
  'Weight (Gram)' 										=> 'Berat (Gram)',
  'You are not allowed to do this action' 				=> 'Anda tidak diijinkan melakukan hal ini',
  'You are not allowed to order this product!' 			=> 'Anda tidak diijinkan untuk mengorder produk ini!',
  'You can upload multiple images and drag them to re-order the position'
  														=> 'Anda dapat mengupload lebih dari satu gambar dan menggesernya untuk merubah urutan gambar',
  'You have no related users, click here to add!' 		=> 'Anda tidak memiliki user yang terhubung dengan akun Anda, klik disini untuk menambahkan!',
  'You must select subdistrict first!' 					=> 'Anda harus memilih kecamatan dulu!',
  'Your Order Submitted Successfully' 					=> 'Order Anda Berhasil Dibuat',
  'Your profile updated successfully' 					=> 'Profil Anda Berhasil Diupdate',
  'Your total orders' 									=> 'Total Order Anda',
  'Your total shopping' 								=> 'Total belanjaan Anda',
  'on' 													=> 'on',
);
