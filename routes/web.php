<?php

/*
 |--------------------------------------------------------------------------
 | Web Routes
 |--------------------------------------------------------------------------
 |
 | Here is where you can register web routes for your application. These
 | routes are loaded by the RouteServiceProvider within a group which
 | contains the "web" middleware group. Now create something great!
 |
 */

// Auth
Auth::routes();

// Homepage
Route::get('/', 'Auth\LoginController@showLoginForm');

// Dashboard
Route::resource('dashboard', 'DashboardController');

// Users Manager
Route::get('/users_manager/{id}/change_status', 'UsersManagerController@changeStatus') -> where('id', '[0-9]+');
Route::resource('users_manager', 'UsersManagerController');

// Products Manager
Route::resource('products_manager', 'ProductsManagerController');

// Orders Manager
Route::resource('orders_manager', 'OrdersManagerController');

// Shipping Agents Manager
Route::resource('shipping_agents', 'ShippingAgentsController');

// Products
Route::get('products/new_products', 'ProductsController@newProducts');
Route::get('products/favorites', 'ProductsController@favorites');
Route::post('products/update_favorites', 'ProductsController@updateFavorites');
Route::post('products/upload_image', 'ProductsController@uploadImage');
Route::post('products/delete_image', 'ProductsController@deleteImage');
Route::get('products/category/{slug?}', 'ProductsController@index')->name('category');
Route::get('products/search', 'ProductsController@search');
Route::resource('products', 'ProductsController');

// Relations
Route::get('relations/supllier_list', 'RelationsController@supplierList');
Route::get('relations/reseller_list', 'RelationsController@resellerList');
Route::get('relations/search', 'RelationsController@search');
Route::post('relations/add_relations', 'RelationsController@add');
Route::post('relations/accept_relations', 'RelationsController@accept');
Route::post('relations/reject_relations', 'RelationsController@reject');
Route::get('relations/{id}/category/{slug?}', 'RelationsController@show')->name('relations_category');
Route::resource('relations', 'RelationsController');

// Orders
Route::get('orders/{id}/create_order', 'OrdersController@createOrder') -> where('id', '[0-9]+');
Route::get('orders/payment', 'OrdersController@paymentList');
Route::get('orders/payment/{id}', 'OrdersController@paymentDetails') -> where('id', '[0-9]+');
Route::post('orders/payment', 'OrdersController@payment');
Route::post('orders/pack', 'OrdersController@packed');
Route::get('orders/shipment', 'OrdersController@shipment');
Route::get('orders/shipping/{id}', 'OrdersController@shippingDetails') -> where('id', '[0-9]+');
Route::post('orders/shipping_code', 'OrdersController@shippingCode');
Route::post('orders/update_shipping_status', 'OrdersController@updateShippingStatus');
Route::post('orders/get_cities', 'OrdersController@getCities');
Route::post('orders/get_subdistrict', 'OrdersController@getSubdistrict');
Route::post('orders/get_shipping_services', 'OrdersController@getShippingServices');
Route::get('orders/success', 'OrdersController@success');
Route::resource('orders', 'OrdersController');

// Chats
Route::post('chats/web_api/{id}', 'ChatsController@getChatRoom') -> where('id', '[0-9]+');
Route::post('chats/web_api/{id}/store', 'ChatsController@storeChat') -> where('id', '[0-9]+');
Route::resource('chats', 'ChatsController');

// Notifications
Route::resource('notifications', 'NotificationsController');

// Notes
Route::resource('notes', 'NotesController');

// API Users Management
Route::post('api/register', 'Api\ApiUsersController@store');
Route::post('api/login', 'Api\ApiUsersController@login');

// Settings
Route::get('profile', 'SettingsController@profile');
Route::put('profile', 'SettingsController@updateProfile');
Route::get('banks', 'SettingsController@banks');
Route::get('banks/add', 'SettingsController@addBank');
Route::post('banks/store', 'SettingsController@storeBank');
Route::get('banks/{id}/edit', 'SettingsController@editBank') -> where('id', '[0-9]+');
Route::put('banks/{id}/update', 'SettingsController@updateBank') -> where('id', '[0-9]+');
Route::delete('banks/{id}/delete', 'SettingsController@destroyBank') -> where('id', '[0-9]+');
Route::resource('settings', 'SettingsController');

// Terms
Route::get('terms/showcase_categories', 'TermsController@indexShowcaseCategories');
Route::get('terms/add_showcase_categories', 'TermsController@addShowcaseCategories');
Route::post('terms/store_showcase_categories', 'TermsController@storeShowcaseCategories');
Route::get('terms/{id}/edit_showcase_categories', 'TermsController@editShowcaseCategories') -> where('id', '[0-9]+');
Route::put('terms/{id}/update_showcase_categories', 'TermsController@updateShowcaseCategories') -> where('id', '[0-9]+');
Route::delete('terms/{id}/destroy_showcase_categories', 'TermsController@destroyShowcaseCategories') -> where('id', '[0-9]+');
Route::resource('terms', 'TermsController');

// Get and show image from GDrive
Route::get('show_image/{id}', 'Api\ApiProductsController@showImage') -> where('id', '[a-z0-9_0-9.A-Za-z]+');
Route::get('show_temp/{id}/{path}', 'Api\ApiProductsController@showTemp') -> where('id', '[0-9]+');
