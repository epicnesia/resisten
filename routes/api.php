<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
	
	// Users Resource
	Route::post('users/update', 'Api\ApiUsersController@update');
	Route::get('users/load_target', 'Api\ApiUsersController@loadTarget');
	Route::post('users/add_bank', 'Api\ApiUsersController@addBank');
	Route::get('users/get_bank', 'Api\ApiUsersController@getBank');
	Route::get('users/get_seller_bank', 'Api\ApiUsersController@getSellerBank');
	Route::delete('users/del_bank', 'Api\ApiUsersController@deleteBank');
	
	// Products Resource
	Route::resource('products', 'Api\ApiProductsController');
	
	// Relations Resource
	Route::get('relations/supllier_list', 'Api\ApiRelationsController@supplierList');
	Route::get('relations/reseller_list', 'Api\ApiRelationsController@resellerList');
	Route::get('relations/search', 'Api\ApiRelationsController@search');
	Route::post('relations/add_relations', 'Api\ApiRelationsController@add');
	Route::post('relations/accept_relations', 'Api\ApiRelationsController@accept');
	Route::post('relations/reject_relations', 'Api\ApiRelationsController@reject');
	Route::get('relations/get_coop_request', 'Api\ApiRelationsController@getCoopRequest');
	Route::resource('relations', 'Api\ApiRelationsController');
	
	// Orders Resource
	Route::post('orders/payment', 'Api\ApiOrdersController@payment');
	Route::post('orders/pack', 'Api\ApiOrdersController@packed');
	Route::post('orders/shipping_code', 'Api\ApiOrdersController@shippingCode');
	Route::post('orders/update_shipping_status', 'Api\ApiOrdersController@UpdateShippingStatus');
	Route::resource('orders', 'Api\ApiOrdersController');
	
	// Shipping Agents Resource
	Route::resource('shipping_agents', 'Api\ApiShippingAgentsController');
	
});
